---
title: |
  Censure et consensus dans le cinéma de l'après-guerre
subtitle: |
  La genèse parallèle d'Au royaume des cieux (Julien Duvivier, 1949) et La Cage aux filles (Maurice Cloche, 1950)
# summary: Here we describe how to add a page to your site.
date: "2022-11-01T00:00:00Z"
#toc: true
type: book
# reading_time: false  # Show estimated reading time?
# share: false  # Show social sharing links?
# profile: false  # Show author profile?
# comments: false  # Show comments?

# Optional header image (relative to `assets/media/` folder).
header:
  caption: ""
  image: ""
---

Article publié en novembre 2022 dans la revue _French Cultural Studies_.  
{{< cta cta_text="DOI: 10.1177/09571558221120881" cta_link="https://dx.doi.org/10.1177/09571558221120881" cta_new_tab="false" cta_alt_text="Télécharger PDF" cta_alt_link="censure-consensus-final.pdf" cta_alt_new_tab="false" >}}

{{< figure src="royaume.jpg" title="Maria (Suzanne Cloutier) et Mlle Chamblas (Suzy Prim) dans _Au royaume des cieux_" >}}

## Résumé

*Au royaume des cieux* (Julien Duvivier, 1949) et *La Cage aux filles* (Maurice Cloche, 1950), deux fictions situées dans des centres de rééducation pour jeunes femmes, ont une histoire liée. Fondées sur le même fait divers, produites en même temps, elles font l'objet de tractations complexes lorsqu'elles sont soumises à la censure. Le film de Duvivier, dont le rapport au réel est ambigu, finit par échapper à une réécriture, alors que Maurice Cloche accepte de travailler de près avec le ministère de la Justice et finit par réaliser un film réaliste reproduisant le discours officiel des autorités. Pourtant, les deux œuvres présentent la rééducation en internat d'une manière trompeuse et contraire aux reportages sur lesquels elles sont fondés. En examinant la genèse parallèle des deux films, cet article réexamine le rôle des négociations et de la propagande dans la construction du cinéma « consensuel » français de l'après-guerre.

## Abstract

**Censorship and consensus in post-war French cinema: the making of *Au royaume des cieux* (Julien Duvivier, 1949) and *La Cage aux filles* (Maurice Cloche, 1950)**

*Au royaume des cieux* (*The Sinners*, Julien Duvivier, 1949) and *La Cage aux filles* (*Cage of Girls*, Maurice Cloche, 1950), two fiction features set in reform schools for young women, have a shared history. Based on the same real events, written and produced concurrently, they took diverging paths when undergoing government censorship. Duvivier's film, which lets the relationship between its story and the real world remain ambiguous, was left largely untouched; Maurice Cloche, on the other hand, agreed to work closely with the French Ministry of Justice and ended up directing an explicitly realist film that reproduces the authorities' official discourse. Both films, however, present reformatories in a deceptive way that contradicts the journalism they were based on. Examining the parallel trajectories of both films as they were made, this article reexamines the role of negotiations and propaganda in the "consensual" nature of French cinema from the immediate post-war period.

## La timidité

Dans son *Histoire du cinéma français* – une entrée en matière souvent
donnée à lire aux étudiants, parue dans sa quatrième édition en 2019 –
Jean-Pierre Jeancolas présente le cinéma de la Quatrième République
comme un « cinéma de pouvoir, consensuel, centriste et timoré ». Il
souligne l’importance de la censure et de l’autocensure, décrivant ces
phénomènes comme un trait distinctif du cinéma de l’époque.

> Le cinéma français de la Quatrième République est frileux. Il est
> corseté par une censure qui s’échauffe dès qu’un film aborde
> l’actualité : les difficultés économiques, les séquelles de la guerre,
> la guerre froide, la guerre d’Indochine, puis celle qui commence en
> Algérie, la décolonisation en général, la décomposition du système
> politique. \[…\] Hormis quelques courts ou moyens métrages produits
> par le parti communiste ou par les syndicats et contraints à une
> circulation clandestine ou « parallèle », et excepté quelques éclats
> dans des films de Clouzot, de Becker, d’Autant-Lara ou de Duvivier, le
> cinéma se tient prudemment à bonne distance de la réalité française,
> ce à quoi la profession s’accommode assez facilement, par
> pusillanimité ou par corporatisme. (Jeancolas, 2019: 61–63)

Jeancolas est loin d’être le seul historien du cinéma français à mettre
en exergue le caractère consensuel de la production commerciale de ces
années. Pour Geneviève Sellier, le cinéma français, par sa « relecture
rassurante » de l’histoire de la guerre et son recours au patrimoine
littéraire en tant que « ciment culturel », participe à la
reconstruction d’un « imaginaire national consensuel, après une période
de quasi-guerre civile » (Sellier, 2015: 2). Catherine Gaston-Mathé
(1996: 13–14) affirme que « le cinéma de l’après-guerre incite plus à
l’oubli et à l’évasion qu’au devoir de mémoire \[…\] ». Quant à Raymond
Borde, il va jusqu’à qualifier ce cinéma de « post-vichyssois ». Avec un
parti pris contre la critique communiste de l’époque, qui soutenait
instinctivement le cinéma français contre la concurrence hollywoodienne,
il juge globalement médiocre et insipide la production entre 1945 et le
début des années 1950 : « Dans cette démagogie du populisme souriant,
pleurnichard et patriotique, les notes discordantes furent extrêmement
rares \[…\]. » (Borde, 1978: 16–17)

Ces appréciations du cinéma d’après-guerre sont à distinguer des
critiques les plus familières adressées à son encontre, à l’image de
celles formulées par les plumes des *Cahiers du cinéma*. Elles ne
portent pas sur l’esthétique, mais sur l’absence de questionnement
politique et social dans le cinéma commercial. On peut les ranger dans
la catégorie plus large des observations soulignant l’éloignement du
cinéma de la réalité quotidienne des Français (par exemple Williams,
1992: 280; Chirat, 1985: 15) ou l’absence d’un véritable courant
néoréaliste en France alors que ce mouvement s’épanouit en Italie
(Martin, 1984: 24–25). Certes, un cinéma militant de gauche fleurit en
France à cette époque, mais il reste aux marges de la culture
cinématographique, circonscrit aux séances non commerciales, moins
soumises à la censure, lors de meetings politiques ou syndicaux ou au
sein des ciné-clubs (Gallinari, 2015: 63–65).

Si le cinéma de l’après-guerre est si consensuel – voire convenu, pour
ses détracteurs –, c’est surtout l’autocensure qui aurait empêché les
cinéastes de traiter avec audace de l’actualité de leur époque. Au-delà
des questions spécifiques citées par Jeancolas, qui constituaient de
véritables tabous, plusieurs observateurs ont émis l’hypothèse que les
producteurs, voire aussi les réalisateurs et scénaristes, ont évité de
leur propre gré des thèmes politiques, économiques ou sociaux, liés au
quotidien mais potentiellement sujets à controverse, sans avoir été
directement entravés par l’action coercitive de l’État. Dès 1953, le
critique et fondateur de *L’Écran français* Jean-Pierre Barrot
constate : « Depuis la Libération, la censure s’est montrée
progressivement de moins en moins libérale ; et la crainte d’une
interdiction pure et simple ou de coupures trop importantes encourage
nos producteurs à écarter délibérément trop de sujets. » (Barrot, 1953:
28) Selon Marcel Martin, après la censure de quelques œuvres de
réalisateurs communistes (Louis Daquin, Robert Menegoz, René Vautier),
« les scénarios portant sur des sujets progressistes sont
systématiquement écartés par les producteurs. » (Martin, 1984: 22)
Raymond Borde aussi sous-entend que des cas exemplaires de censure ont
entraîné une autocensure importante mais, comme Martin et Barrot, il
fait cette observation sans en dire davantage sur la supposée timidité
des réalisateurs face aux pressions exercées par les producteurs, ou
celle des producteurs vis-à-vis de l’État. En dépouillant un large
échantillon d’archives de la censure du CNC, Fréderic Hervé estime que,
dans l’immédiat après-guerre, peu de films sont frappés de restriction,
car « les censeurs sont craints », c’est-à-dire que la censure et
l’autocensure sont consenties et répandues au sein de l’industrie
(Hervé, 2015: 31–32). Quel était le fonctionnement précis de cette
incitation à l’autocensure dans la période de l’après-guerre ? De quelle
manière les autorités publiques s’inséraient-elles dans le traitement
cinématographique d’un thème d’actualité ? À quel point et dans quelles
circonstances les cinéastes et scénaristes acceptaient-ils de modifier
leurs œuvres ? Si les travaux de Sébastien Denis (2009) ou Sylvie
Lindeperg (2000, 2014) abordent aussi ces questions, celles-ci restent
peu explorées, probablement en raison du peu d’intérêt que la critique
cinéphile a traditionnellement porté à cette période.

L’objectif de cette contribution est d’apporter de nouveaux éléments de
réponse à la question du consensus dans le cinéma français de
l’après-guerre, en examinant la genèse de deux longs métrages traitant
du même sujet, tournés en même temps et censurés en parallèle : *Au
royaume des cieux* de Julien Duvivier et *La Cage aux filles* de Maurice
Cloche. Le premier, sorti le 30 septembre 1949, est vu de nos jours
comme une œuvre mineure d’un réalisateur célèbre pour ses contributions
au réalisme poétique des années 1930 ; le deuxième, sorti le 13 janvier
1950, est le film d’un cinéaste populaire prolifique, aujourd’hui
oublié, si ce n’est pour son intérêt pour les sujets sociaux et la
religion catholique.

*Au royaume des cieux* raconte l’histoire de Maria (Suzanne Cloutier),
une jeune orpheline qui, au début du film, arrive dans une maison de
redressement en bord de Loire. Les méthodes tyranniques de la
directrice, M<sup>lle</sup> Chamblas (Suzy Prim), poussent bientôt une
détenue au suicide et les autres à entamer une grève de la faim. À
l’extérieur, l’amant de Maria, Pierre (Serge Reggiani), sérieux et
idéaliste, échange avec une détenue évadée et essaie de concevoir une
ruse pour permettre à Maria de s’évader. Finalement une révolte gagne
l’établissement. Cependant, Maria a réussi à s’évader au beau milieu de
la mutinerie et s’efforce de rejoindre Pierre alors que la région est
touchée par une inondation. Les derniers plans, où Pierre tient dans ses
bras le corps inerte de Maria, restent ambigus, laissant le public se
demander si elle a payé sa liberté de sa vie.

Tombé dans l’oubli aujourd’hui – il n’a jamais fait l’objet d’une
édition en VHS, DVD, BluRay ou VOD –, *La Cage aux filles* rencontre un
grand succès commercial lors de sa sortie en salles, attirant un peu
plus de trois millions de spectateurs sur la France entière, contre 2,6
millions d’entrées pour *Au royaume des cieux* (Simsi, 2012: 15, 130).
Au début du film, Micheline (Danièle Delorme) échoue à son examen de
sténographe. Face à la timidité de sa mère et la cruauté de son
beau-père (Noël Roquevert), un ouvrier alcoolique qui la bat et semble
ne s’intéresser qu’à son syndicat, elle quitte la maison. Repoussant
l’intérêt que lui porte un jeune ingénieur, elle tombe amoureuse du
truand Freddy, qui l’emmène à Paris, puis l’abandonne. Micheline est
alors arrêtée pour vagabondage, mais son beau-père refuse de la
reprendre, en dépit des supplications d’un juge des enfants et d’une
mère supérieure. Son placement dans un internat de rééducation ne fait
qu’aggraver sa situation, car elle y rencontre des filles de mauvaise
vie, dont la cynique et chaotique Rita (Jacky Flynt). Les deux filles
s’évadent ensemble, vivant de larcins commis avec Loulou (Jacques
Vérières), le futur amant de Micheline. De retour à Lyon, celle-ci est
enfermée dans un autre établissement qui est vite gagné par une
mutinerie. Édith (Suzanne Flon), jeune éducatrice affectueuse, réussit à
arracher Micheline à cet environnement nocif en obtenant son placement
dans un internat modèle qui pratique de nouvelles méthodes. Lors d’une
dernière évasion, Micheline retrouve Rita dans les bras de Loulou ; elle
retourne donc à cette nouvelle maison de redressement, commence à faire
confiance à Édith, apprend un métier et se fiance avec l’ingénieur
qu’elle avait repoussé au début du film.

La mise en images de la problématique sociale de la délinquance juvénile
– un thème portant une charge politique inévitable, sans être un tabou
absolu – est un exercice délicat. Maurice Cloche, Julien Duvivier, leurs
scénaristes et producteurs connaissent sans doute le destin de *L’Île
des enfants perdus*, le projet de Marcel Carné et Jacques Prévert, sur
le même thème, qui a été étouffé par la censure avant la guerre ; Carné
le reprend en 1947 sous le titre *La Fleur de l’âge*, mais l’abandonne
au cours d’un tournage désastreux (Aurouet, 2005a, 2005b). Les enjeux
politiques sont d’autant plus importants pour *La Cage aux filles* et
*Au royaume des cieux* que les deux récits s’inspirent librement des
mêmes faits réels. Les faits et les films surviennent à un moment clé de
l’histoire où, à la suite de la Seconde Guerre mondiale, la France
réexamine le phénomène de la délinquance juvénile, les problèmes sociaux
qui l’engendrent et les institutions, souvent sous-équipées, censées la
combattre. C’est une période où de nouvelles lois sont adoptées pour
réorganiser la justice pour mineurs, des faits divers impliquant des
jeunes connaissent un retentissement médiatique et un large débat public
s’instaure autour de la jeunesse (Jobs, 2007: 141–184). Après un résumé
de l’incident réel ayant inspiré les films et sa couverture dans la
presse, notre analyse des films prendra en compte la manière dont ils
représentent ce fait divers en particulier, et plus largement la
jeunesse, la délinquance et les institutions dans ce contexte historique
particulier. Ensuite, nous procéderons à l’examen des archives liées à
la censure des deux films : ces documents en disent long sur les
rapports qu’entretiennent cinéma et pouvoir politique dans la France de
la fin des années 1940. Nous verrons que la création de ces films
« consensuels » découle de nombreux compromis, négociés en coulisses
entre plusieurs interlocuteurs.

Ces arbitrages ne se limitent pas à la suppression de contenu des films,
mais s’étendent à des pressions incitant les réalisateurs à dresser un
portrait complaisant des institutions. C’est pourquoi nous abordons la
manière dont la création du cinéma « consensuel » repose non seulement
sur la censure, mais aussi sur la propagande. Selon la compréhension
large de ce terme proposée par Jacques Ellul, la propagande peut être ou
bien verticale, commandée et planifiée par des personnes dans des
positions de pouvoir, ou bien horizontale, diffusée de façon volontaire
entre individus (Ellul, 1990: 94–99). Nous verrons que les cinéastes
français de l’après-guerre peuvent occuper les deux positions dans ce
schéma : bien qu’ils soient parfois incités à ou obligés de répéter les
propos du gouvernement, parfois ils expriment de leur plein gré des
formes similaires de propagande dans leurs films.

## La révolte

Le matin du 6 mai 1947, une révolte éclate dans une maison d’éducation
surveillée pour jeunes femmes installée au sein de la prison de Fresnes
(voir Thomazeau, 2007b). Après des dégradations des lieux, le saccage du
magasin et la prise d’une partie du toit par un groupe de filles qui
refusent de descendre, la direction demande des renforts de police. Une
quarantaine de détenues sont arrêtées et emmenées au dépôt, où elles
poursuivent leur rébellion en détruisant des lits et en brisant des
fenêtres. L’événement suscite de nombreux articles dans la presse
quotidienne, certains approchant le sujet d’une angle sociale (« Les
mutinées de Fresnes continuent leur sabbat au dépôt, mais le problème de
la jeunesse délinquante est d’une acuité toujours croissante »,
*L’Aube*, 9 mai 1947), d’autres écrits sur un ton cocasse (« Mutinerie à
Fresnes. Les révoltées boivent un tonneau de vin et font rougir les
agents de police ! », *Combat*, 8 mai 1947).

L’incident intervient à un moment charnière dans l’histoire de la
justice en France. La Libération – effectivement le passage d’un État
policier vers un État de droit (voir, par exemple, Shklar, 1998)  –
occasionne des réformes qui bouleversent l’organisation de la justice
pénale et en particulier la justice pour mineurs. Une ordonnance du
2 février 1945 relative à l’enfance délinquante sépare le service de
l’Éducation surveillée de l’administration pénitentiaire dont il
dépendait auparavant ; elle crée la fonction de juge des enfants, met en
place des régimes de semi-liberté et, en général, donne la priorité aux
mesures éducatives plutôt que répressives (Bantigny, 2007: 148–149;
Chauvière, 1998). Cependant, les principes de cette réforme ambitieuse
sont plus facilement promus dans la presse que mis en œuvre sur le
terrain. Les centres d’observation et internats de rééducation ne
s’éloignent que lentement et avec hésitation de leurs procédés
historiques de répression et de réclusion (Bantigny, 2007: 157–181). La
mutinerie de Fresnes est un exemple de ces difficultés, car elle a lieu
dans un établissement implanté au sein d’une prison pour adultes, une
situation censée disparaître selon la nouvelle législation.

Avant les adaptations cinématographiques, la rébellion des détenues de
Fresnes trouve un premier écho médiatique dans la presse quotidienne. Le
journal populaire *France-soir* édite deux longues séries d’articles
autour de l’incident écrits par l’écrivain reporter Henri Danjou, un
spécialiste en la matière qui dès la fin des années 1920 a participé aux
campagnes de presse contre les « bagnes d’enfants ». En préparant son
long métrage, Julien Duvivier se documente à partir d’articles de
presse, dont ceux de Danjou et, en août 1948, invite ce dernier à
travailler avec lui et son scénariste déjà choisi, Henri Jeanson[^1].
Danjou rompt rapidement cette collaboration et porte plainte contre
Duvivier pour plagiat ; la plainte est classée sans suite (Bonnefille,
2002: vol. 2, 63). Peu après, Maurice Cloche l’engage comme scénariste
pour son projet concurrent. La genèse des deux films commence donc par
les exposés de Danjou dans *France-soir*.

La première série de reportages relève autant du feuilleton que du
journalisme : il présente les événements à Fresnes de façon dramatique
et sensationnelle, à tel point qu’il est difficile de deviner les
sources du journaliste, ou même de distinguer réalité, fiction, rumeur
et effet de style. La révolte elle-même, élément central du récit, et la
description de la directrice de l’établissement, antagoniste principale,
préfigurent *Au royaume des cieux* : cette dernière est partisane des
« bonnes vieilles méthodes anciennes », y compris gifles et coups de
poings. D’autres aspects de cette série d’articles se retrouvent dans le
film de Maurice Cloche. Notamment, Danjou décrit des filles qui, comme
l’héroïne de *La Cage aux filles*, sont arrêtées pour vagabondage ou
éventuellement pour délation ou liaisons avec l’ennemi pendant
l’Occupation, plutôt que pour vols, crimes ou prostitution (Danjou,
1947) ; effectivement, la majorité des jeunes femmes placées en internat
à cette époque le sont par mesure de protection et non par condamnation
pour crime ou délit (Thomazeau, 2007a). D’autres détails ne se
retrouvent pas dans les deux films : par exemple, le journaliste accuse
la directrice d’avoir provoqué exprès l’incident afin de régler un
compte avec deux éducatrices aux méthodes plus modernes.

{{< figure src="francesoir.jpg" title="Le premier volet de la seconde série d’articles paraît sur la une du quotidien France-soir le 15 juin 1949, vers le début du tournage du film de Maurice Cloche." >}}

Parue deux ans plus tard – lorsque les deux films sont déjà en
préparation –, la seconde série de reportages fait entendre la voix de
l’éducatrice Gilberte Sollacaro, dont le nom figure déjà dans quelques
articles de la première série. Le personnage de M<sup>lle</sup> Édith,
l’éducatrice dans *La Cage aux filles* qui prend Micheline sous son
aile, serait vraisemblablement fondé sur cette personne réelle. Sur un
ton plus sobre que la première série, Danjou et Sollacaro racontent en
épisodes le quotidien des détenues de Fresnes. Une première, rencontrée
au cachot, hurlait des injures jusqu’à ce que l’éducatrice lui montre
des signes de confiance (Danjou et Sollacaro, 1949c). Une autre, placée
sous le tutorat de son frère après la mort de leur père, a été
abandonnée après avoir attrapé une infection sexuellement transmissible
d’un amant ; d’après Sollacaro, elle nécessitait seulement un traitement
médical et un peu de tendresse (Danjou et Sollacaro, 1949b). Ces
histoires soulignent souvent l’indifférence, l’arrogance, voire la
cruauté de la part des surveillants et dirigeants de la maison de
redressement et ensuite l’éventuel apaisement d’une jeune femme grâce
aux méthodes « compréhensives » pratiquées par Sollacaro et quelques
collègues. D’autres épisodes retracent le trajectoire de filles
difficiles qui, comme la protagoniste du film de Maurice Cloche, sont
rendues encore plus ingérables par l’institution corrompue.

L’article final aborde l’Institution publique d’Éducation surveillée de
Brécourt, un nouvel internat mis en avant comme « modèle » par l’État,
ouvert en 1947 dans le département de Seine et Oise. Mutée dans cet
établissement à la suite de la révolte à Fresnes, Gilberte Sollacaro
décrit sa « modernité » comme une simple façade (Danjou et Sollacaro,
1949a). Brécourt bénéficie d’un cadre sain, d’excellentes conditions
matérielles et une direction plus scrupuleuse que celle de Fresnes. En
revanche, le taux d’évasion ne serait pas moins important que celui des
autres établissements d’éducation surveillée. Les filles choisies pour
intégrer Brécourt, seulement une trentaine au total, ont toutes été
sélectionnées comme « méritantes » par d’autres institutions. Selon
l’expérience de Sollacaro, il s’agit des prisonnières les plus
« sournoises » et « hypocrites » qui n’hésitaient pas à jouer le jeu des
dirigeants en dénonçant leurs camarades. Après dix mois au nouvel
internat, l’éducatrice doute que ces conditions favorables suffisent
pour « soigner \[le\] caractère et \[les\] âmes malades » des détenues.
Elle affirme qu’après avoir fait part de ses doutes à la directrice,
elle a été immédiatement limogée à cause de sa présence lors de la
révolte de Fresnes, et pour avoir apporté des témoignages dans les
articles publiés par Danjou en 1947.

Ces critiques reflètent certainement la déception personnelle d’une
ancienne employée démise de ses fonctions, mais plusieurs de ces
observations se retrouvent dans les travaux de notre époque portant sur
l’histoire de la rééducation de filles en internat. La capacité
d’accueil de Brécourt, institution unique en son genre en 1949, est en
effet très modeste, et son modèle fondé sur de petits effectifs peut
difficilement être étendu à des établissements moins bien lotis.
L’enfermement en cellule reste une punition courante et un élément clé
de la stratégie de discipline employée à Brécourt, de son ouverture
jusque dans les années 1960, même si la réforme de 1945 a mis l’accent
sur l’éducation plutôt que la répression (Bantigny, 2007: 177–179).
Malgré le développement d’une véritable politique d’éducation, le
personnel est confronté par des impératifs contradictoires d’éduquer,
d’une part, et d’autre part de surveiller et enfermer (Thomazeau,
2005) ; l’empathie et les bonnes conditions matérielles ne sont pas une
panacée.

Notons finalement le titre et le sous-titre trompeurs du dernier texte
de Gilberte Sollacaro et Henri Danjou, qui changent complètement le ton
de la série : « Après l’enfer de Fresnes, voici le paradis de Brécourt :
dans ce pénitencier de l’espoir, les filles sont prisonnières sur
parole… ». On ne saisit le sens ironique du titre qu’après avoir lu le
texte de l’article. Nous n’avons aucun moyen de savoir si ce titre est
choisi par les auteurs ou imposé par *France-soir*, mais un lecteur
inattentif peut toujours croire que l’heure est au progrès dans les
maisons de rééducation. Comme nous le verrons, *Au royaume des cieux* et
surtout *La Cage aux filles* peuvent amener leurs spectateurs à cette
même conclusion.

## L’ambiguïté

*Au royaume des cieux* ne rappelle pas de manière évidente la mutinerie
de Fresnes. Son double récit concerne à la fois l’amour empêché entre
Maria et Pierre et le conflit entre la directrice et les détenues. Par
ailleurs, la réception critique du film se caractérise par deux lectures
adverses : d’une part comme fiction sans lien particulier avec la
réalité contemporaine, d’autre part comme reportage en forme de
mélodrame.

Selon la première interprétation, le film n’évoquerait pas l’actualité
de l’Éducation surveillée. La violence morale et physique subie par les
détenues ne serait pas institutionnelle ; elle émanerait uniquement de
la directrice. Éprise de pouvoir, jalouse de la jeunesse et du charme
des jeunes femmes sous sa tutelle, M<sup>lle</sup> Chamblas est un
personnage haineux et malveillant. Dès la mort de son prédécesseur, elle
se met à l’aise devant son bureau et s’amuse à tamponner des documents
avec le sceau de la maison, symbole de son nouveau pouvoir. Elle se
montre particulièrement brutale envers Maria, l’envoyant dans un cachot
obscur inutilisé depuis cinq ans – c’est-à-dire depuis la fin de
l’Occupation. Dans une séquence exemplaire, après avoir découvert que
Pierre a clandestinement rendu visite à Maria, la directrice insulte et
menace la jeune détenue pendant que celle-ci garde le silence face aux
interrogations hostiles. M<sup>lle</sup> Chamblas finit par arracher la
chemise de la jeune femme et la saisit dans une étreinte perverse, en
criant : « C’est pour ça, c’est pour cette peau-là qu’ils te suivent
comme une chienne ! », provoquant finalement une réponse : « Vous
étouffez de jalousie parce que vous êtes ignoble, parce qu’un homme n’a
jamais voulu de vous ! ». À l’origine de tous les troubles de l’histoire
ne se trouve ni la pauvreté, ni la guerre, ni des traitements
inéquitables, seule une « vieille fille » jalouse[^2].

Selon cette lecture, en mettant en scène un personnage aussi
antipathique, Duvivier et Jeanson inventent un cas particulier d’abus et
n’émettent aucun critique de la gestion des maisons de redressement dans
la France de l’époque. Maria raconte, dès son arrivée à la Haute-Mère,
les aléas qui l’y ont conduite : sa mère décédée, son père inconnu, elle
s’est enfuie après avoir été victime d’abus au sein de plusieurs
familles dans lesquelles elle a été placée par l’Assistance publique.
Pourtant, ce passé, évoqué dans les dialogues, n’est jamais mis en
images. Parmi les autres pensionnaires de la maison, l’une a été arrêtée
pour vol, une autre pour prostitution, une troisième pour homicide ;
toutes, sauf Maria, parlent un langage argotique issu des quartiers
populaires. Cependant, il ne peut y avoir de discours social au premier
plan d’un film qui ne montre aucune image de ces milieux populaires et
qui ne fait que brièvement allusion dans les dialogues à la vie des
détenues avant leur arrestation.

De plus, le règne brutal de M<sup>lle</sup> Chamblas, bien qu’il
remplisse la quasi-totalité du récit, ne serait qu’une parenthèse dans
l’histoire de la fictive maison de Haute-Mère. Au début du film, une
directrice sympathique, M<sup>me</sup> Bardin (Paule Andral), explique à
Maria que la maison sert non pas à punir ses pensionnaires, mais à leur
apprendre un métier et à les aider à trouver leur place dans le monde.
Elle est tellement ravie d’apprendre que l’administration lui accorde de
nouveaux locaux, plus modernes, qu’elle meurt sur le coup d’un
infarctus. À la fin du film, la rébellion des filles s’éteint.
M<sup>lle</sup> Chamblas est suspendue de ses fonctions et remplacée par
une surveillante, la bienveillante mais toujours assez ferme
M<sup>lle</sup> Guérande (Monique Mélinand). Cette nouvelle directrice,
qui critique l’intransigeance et l’inhumanité de son ancienne patronne,
est partisane de « nouvelles méthodes » fondées sur la confiance, sans
que celles-ci soient détaillées.

Bien que Duvivier évite tout rapprochement avec l’actualité, son film a
malgré tout une dimension politique dans la présentation orientée des
institutions. Si, selon la première lecture du film, les sévices
infligés par M<sup>lle</sup> Chamblas relèvent du fictionnel et de
l’exceptionnel, le récit comporte de nombreux détails qui esquissent une
vision fort optimiste de l’actualité. Les promesses imprécises
concernant de nouvelles méthodes d’éducation, l’accueil attentionné de
M<sup>me</sup> Bardin, la ferme empathie de M<sup>lle</sup> Guérande et
l’appel téléphonique de l’administration promettant de nouveaux locaux
composent un arrière-plan entièrement en phase avec les discours
officiels de l’État, et qui traduit mal les réalités complexes de
l’Éducation surveillée en 1949. De nombreux signaux tout au long du film
laissent supposer que l’injustice vue à l’écran a pour seule origine la
perversité d’une femme, et non l’iniquité du système, qu’il suffit d’un
cadre sain et un peu de bienveillance pour rééduquer les délinquants
juvéniles, que les pratiques purement répressives sont alors en voie de
disparition, remplacées par des méthodes à la fois douces et efficaces.
C’est au travers de tels poncifs dans la narration et le montage que,
selon Noël Carroll (1996), le cinéma devient un vecteur de transmission
idéologique favorisant des logiques de domination sociale. Certains de
ces clichés et les images qui les portent – les dortoirs, les ateliers,
les filles faisant la ronde dans la cour au rythme imposé par une
surveillante autoritaire, la brève euphorie de la mutinerie – sont
désormais conventionnels, à l’image de deux films, sortis quelques
années plus tôt, aux objectifs propagandistes transparents. *Prison sans
barreaux* (Léonide Moguy, 1938) et *Le Carrefour des enfants perdus*
(Léo Joannon, 1944) promeuvent des méthodes « humanistes » et
« compréhensives » de rééducation tout en appuyant la politique
officielle de deux gouvernements aux antipodes : le premier est soutenu
par le ministère de l’Éducation de Jean Zay sous le Front populaire,
tandis que le deuxième intègre l’idéologie du régime de Vichy (Houbre,
2000). Plusieurs critiques publiés au moment de la sortie d’*Au royaume
des cieux* font allusion à ces prédécesseurs.

Dans des entretiens publiés pendant le tournage du film, Julien Duvivier
met en avant le réalisme d’*Au royaume des cieux*, évoquant sa visite
d’une maison de correction et les reportages de presse qu’il a utilisés
comme documentation (Berger, 1949). Pourtant, un carton affiché avant le
générique, niant toute exemplarité du récit, laisse planer une certaine
ambiguïté. « Le film que vous allez voir est une œuvre d’imagination.
C’est dire que les personnages, l’action et le milieu sont purement
fictifs. Il ne s’agit pas d’un reportage romancé mais d’un roman
cinématographique. » Ce carton fait hésiter : pourquoi préciser aux
spectateurs que le film qu’ils verront n’est pas un reportage ? Devoir
l’expliciter signifie qu’au contraire, le film ressemble justement à un
reportage et que, sans le carton, des spectateurs risqueraient de
prendre le film pour une représentation de la réalité des maisons de
redressement à l’heure du tournage. En effet, le thème social du film
soutient cette interprétation (voir, par exemple Sorlin, 2015: 42–55).
Le simple fait de situer ce mélodrame dans une maison de redressement,
de montrer des détenues à l’écran, pourrait faire du film une
représentation du réel aux yeux de nombreux spectateurs, malgré la
précision liminaire. Le remplacement de M<sup>lle</sup> Chamblas par
M<sup>lle</sup> Guérande pourrait amener des spectateurs à voir un
reportage sur l’instauration de méthodes plus humaines dans l’Éducation
surveillée. D’autres, qui ne prêtent pas beaucoup d’attention à ces
subtilités de la narration, pourraient même conclure que le film est une
œuvre militante sur le mauvais traitement des délinquantes par l’État ;
nous verrons que c’est le cas d’au moins une spectatrice, membre de la
commission chargée de censurer *Au royaume des cieux*, même si cette
lecture n’est pas partagée par les critiques professionnels.

La réception critique du film montre qu’il est effectivement sujet à une
double lecture[^3]. *Paris-presse* affirme qu’« à aucun moment les
auteurs n’ont tenté une critique sociale. Ce n’est pas la société qui
est mauvaise, mais Suzy Prim, qui est une folle sadique »
(*Paris-presse*, 1949). Dans *Le Parisien libéré*, Jacqueline Michel
(1949) écrit : « *Au royaume des cieux* n’est pas un film “sur” les
maisons de redressement comme l’était *Prison sans barreaux*. » Elle
déplore le fait que « ses filles perdues ne \[soient\] guère vraies et
\[aient\] l’air de sortir plus directement des clubs de
Saint-Germain-des-Prés que des tribunaux pour enfants. » Pour sa part,
Jean Fayard (1949) intitule son billet « *Au royaume des cieux* est un
reportage où il y a du bon et du moins bon », faisant l’éloge de ce
qu’il voit comme une reconstitution véridique. Reprochant à Duvivier des
« effets d’attendrissement un peu faciles », il conclut pourtant que
« les auteurs ont eu le souci visible de rejoindre la vérité et de
reconstituer le climat plausible d’un vrai pénitencier » et de son
quotidien. Cependant, plusieurs critiques relèvent les poncifs et les
clichés employés par Duvivier et Jeanson. Jean Morienval (1949), de
*L’Aube*, affirme que le caractère exceptionnel de
M<sup>lle</sup> Chamblas rend inopérante la thèse du film : « On ne
bâtit pas un film sur un cas de rage. » Jeander (1950), le critique de
*Libération*, ironise : « C’est le film crypto-conventionnel dans toute
son hypocrisie, \[…\] où le “Pater Noster”, l’amour télépathique[^4] et
le ministère de la Justice sont les bons génies qui arrangent tout et
nous consolent de bien des choses. »

## L’optimisme

Alors que subsiste une ambiguïté par rapport au réalisme du film de
Julien Duvivier, *La Cage aux filles* affiche clairement une ambition
documentaire. Dans sa communication autour du film, Maurice Cloche, dont
les deux œuvres précédentes sont des films biographiques (*Monsieur
Vincent*, une énorme réussite populaire en 1947, et *Docteur Laënnec* en
1949), cite la participation d’Henri Danjou et les visites qu’ils ont
effectuées dans les maisons de rééducation pour préparer le tournage.
Dans un interview publié lorsque les deux films concurrents sont en
cours de réalisation, il affirme que *La Cage aux filles* a peu en
commun avec le film de Duvivier :

> Ce que je veux montrer avant tout c’est d’abord comment une fille, une
> fille comme les autres, peut, à la suite de certaines circonstances,
> devenir une délinquante. Et surtout, comment il est possible de la
> rééduquer, de lui réapprendre à vivre, de la sauver… Je ne me contente
> pas d’exposer un problème : je voudrais aussi, surtout, proposer une
> solution. (Vivet, 1949)

La portée sociale mise en exergue de l’œuvre se poursuit avec le début
du film. Comme *Au royaume des cieux*, *La Cage aux filles* commence par
un carton, mais au lieu d’insister sur le statut fictif du récit, il
souligne le rapport entre le film et le réel en identifiant les
pénitenciers où les plans extérieurs ont été tournés.

Des aspects de la représentation des pénitenciers dans *La Cage aux
filles* résultent effectivement d’un travail documentaire sérieux. Les
décors illustrent une véritable problématique de l’époque : les deux
premières maisons de redressement sont situées au sein de prisons pour
adultes et multiplient les références visuelles au milieu carcéral, avec
des contre-plongées impressionnantes sur les rangées de cellules et de
nombreux plans soulignant les décors sombres, les murs tachés et les
barreaux. Un personnage de surveillante désabusée et cruelle, ainsi que
le choix d’une protagoniste innocente, dévoyée par des camarades
incorrigibles rencontrés en détention, rappellent les témoignages de
Gilberte Sollacaro. Par ailleurs, Micheline, la seule fille dont
l’histoire est racontée en détail, ne se retrouve pas emprisonnée à
cause d’actes de délinquance, du moins la première fois. Elle est
arrêtée pour vagabondage, ce qui reflète bien le cas de nombreuses
jeunes femmes placées en maisons de redressement en 1950 (Thomazeau,
2007a: 228–231).

Cependant, ce caractère documentaire est souvent éclipsé par des
représentations schématiques visant à démontrer qu’un internat mal géré
transformera une fille innocente comme Micheline en délinquante hardie,
et que de nouveaux établissements et méthodes auront un effet contraire.
Le film propose une seule hypothèse pour expliquer l’origine des
difficultés de Micheline, ainsi que celles de toutes les jeunes femmes
détenues par l’Éducation surveillée : un manque d’amour au sein de la
famille, et surtout un manque d’amour paternel. L’origine sociale de la
plupart des détenues est invisible, et la famille du personnage
principal vit dans des circonstances matérielles tout à fait correctes.
C’est la cruauté têtue du beau-père interprété par Noël Roquevert – un
spécialiste de rôles secondaires présentant une masculinité rigide – qui
pousse le personnage principal vers la déchéance. L’oncle de Micheline,
artiste peintre plutôt tendre envers elle, n’a lui pas non plus le
courage d’apporter une aide réelle à la jeune femme en détresse. Comme
Julien Duvivier, Maurice Cloche profite de son sujet pour filmer
quelques scènes racoleuses avec des filles adolescentes en
sous-vêtements et des dialogues évoquant des liaisons homosexuelles
entre certaines détenues ; ces répliques font également partie d’une
stratégie pour montrer l’atmosphère malsaine des internats qui nuit à
l’héroïne. Micheline, innocente au début du récit, rencontre Rita dans
le premier établissement où elle est placée et, dans la séquence
suivante, elles s’évadent ensemble. Après plusieurs vols à main armée
avec le groupe de Loulou, l’influence des autres détenues est encore
plus néfaste dans le deuxième internat : des lesbiennes rivalisent pour
être la « copine » de Micheline, l’une explosant de jalousie quand
l’autre lui fait une bise sur la joue. Édith, figure à la fois de
l’autorité et de la compassion, exprime l’hypothèse que l’institution
aggrave encore le comportement des délinquantes.

L’institution modèle est présentée comme le contraire absolu des deux
premières. Les images et les dialogues soulignent ses atouts modernes et
humains : une chambre privée pour chaque pensionnaire, des douches
chaudes, un médecin sur place. Pendant un montage d’images de filles
jouant dans des champs ouverts, suivant des cours de natation et
travaillant en atelier de couture, Édith annonce en *off* sa vision de
l’avenir de l’Éducation surveillée :

> On exigera beaucoup de vous. Par la culture physique obligatoire, par
> le sport, par la natation, on vous fera acquérir le bout de la forme,
> la maîtrise de vous-même, l’amour des distractions saines. Dans ce
> beau cadre champêtre on vous fera oublier les bals-musettes, les fêtes
> foraines, les hôtels borgnes. \[…\] On suit l’exemple de ceux qui vous
> entourent. En peu de temps, vous aurez pris confiance en autrui et en
> vous.

Ce langage et les images qui l’accompagnent construisent un discours
idéaliste qui prétend qu’il suffit d’un environnement sain et un
encadrement compréhensif pour transformer les délinquantes en
travailleuses et épouses honorables. Jusqu’ici, le film exprimait une
vision simplifiée du discours de Gilberte Sollacaro. Notamment, la
représentation d’une seule protagoniste innocente dévoyée par des
camarades délinquantes – dont la rééducation serait apparemment une
cause perdue – est une schématisation de la parole de l’éducatrice
réelle. La conclusion du film va encore plus loin : elle transforme et
aseptise complètement sa perspective sceptique sur l’institution de
Brécourt. Au plan large, Maurice Cloche dépeint les maisons de
rééducation de façon à promouvoir la politique officielle de l’État, qui
consiste à reconstruire l’Éducation surveillée autour de mesures de
protection plutôt que de punition. La presse, d’ailleurs, remarque et
critique largement cette représentation comme naïve ; Alexis Danan
(1950), un journaliste ayant une expérience similaire à celle d’Henri
Danjou, qui a écrit sur les maisons de redressement dans les années 1930
et participé à l’écriture de *Prison sans barreaux*, n’hésite pas à
employer le mot de « propagande » pour évoquer la représentation de
Brécourt. Si *La Cage aux filles* a effectivement une certaine valeur
documentaire, c’est aussi un exercice de propagande qui refuse toute
inquiétude ou nuance sur la réforme de l’Éducation surveillée.

## La négociation

Comme tous les longs métrages projetés en salles à l’époque, les deux
films, l’un ambigu par rapport à son aspect documentaire, l’autre qui
prétend représenter le réel mais qui en offre une version aseptisée,
sont examinés par la commission de contrôle du CNC avant de sortir en
salles. Pourtant, la censure d’*Au royaume des cieux* et *La Cage aux
filles* a comme particularité l’implication personnelle d’un ministre.
Pour cette raison, les archives brossent un portrait riche et détaillé
d’un processus habituellement confidentiel.

En février 1949, lorsque le scénario d’*Au royaume des cieux* est soumis
à une « pré-censure » (voir Garreau, 2009: 45–55), la commission de
contrôle accorde un avis favorable au tournage à l’unanimité moins une
voix, celle de la représentante du ministère de la Justice. Celle-ci,
identifiée seulement comme M<sup>me</sup> Léwy, fait non seulement une
lecture documentarisante du découpage, mais y reconnaît un esprit tout à
fait subversif. Citant le traitement indigne subi par les personnages,
elle conclut : « Si le but de ce film était d’aider la justice \[à\]
adoucir le sort des détenues, \[leur\] permettant de se relever, il
serait tout à fait manqué : un fort sentiment de révolte contre pareil
personnel et contre un régime qui en est responsable est la première
imprécision qu’on en ressort. » (Léwy, 1949)

Bien que minoritaire, cet avis défavorable attire l’attention de Robert
Lecourt, député MRP[^5] qui vient d’être nommé ministre de la Justice.
Malgré un véritable acharnement contre *Au royaume des cieux* de la part
du ministre, la commission de contrôle finit par permettre le tournage
du film tel quel. Une première lettre du ministre, écrite seulement
trois jours après l’avis de M<sup>me</sup> Léwy, résume ses objections à
la réalisation du film de Duvivier ainsi que *La Cage aux filles* de
Maurice Cloche :

> Ces films mettent en cause les méthodes et le personnel de
> l’administration de l’Éducation Surveillée en les présentant de telle
> sorte qu’aucune précaution oratoire ne permettrait d’éviter que le
> public ne fût trompé et très défavorablement influencé à leur égard.
> \[…\] Au demeurant, et ceci est tout particulièrement vrai pour le
> projet de M. Duvivier, c’est le principe même de telles réalisations
> que je crois devoir mettre en cause. (Lecourt, 1949a)

Pourtant, la pièce suivante dans le dossier de censure est une note de
trois lignes, datée du 10 mars 1949 et signée par Robert Mitterrand,
frère et directeur de cabinet du secrétaire d’État chargé de
l’Information, un jeune politique nommé François Mitterrand. Citant le
vote précédent de la commission de contrôle, il confirme simplement
l’autorisation de tournage (Mitterrand, 1949). Ensuite, Lecourt adresse
des lettres aux deux frères Mitterrand se plaignant d’un manque de
réactivité face à ses objections par rapport à *Au royaume des cieux*.

Le dossier ne contient aucune autre trace de ce désaccord. Quatre mois
plus tard, après le tournage du film, la commission de contrôle vote à
l’unanimité une autorisation pour tous publics (Commission de contrôle,
1949a). On peut donc supposer qu’une négociation verbale a eu lieu entre
Lecourt et Mitterrand permettant la levée des réserves avant le vote
final de la commission. Quelles mesures précises ont été prises ? C’est
difficile à dire en l’absence d’une version préliminaire du scénario ;
dans ses lettres, Lecourt refuse notamment de pointer des extraits qui
le gênent ou de proposer des modifications précises. Il semble probable
qu’aucune modification – sauf éventuellement l’ajout du carton au
début – ne soit finalement apportée au film, déjà en cours de tournage,
attaqué par un seul membre de la commission. Il est également possible
que les producteurs soient arrivés à amadouer le ministre grâce à des
négociations en parallèle avec d’autres hauts fonctionnaires du
ministère de la Justice. En effet, le compte rendu de la commission de
pré-censure demande que le CNC invite Julien Duvivier à prendre contact
avec un certain « M. Costa » afin de régler les différends avec le
ministère de la Justice, tout en accordant « définitivement » le
principe de l’autorisation (Commission de contrôle, 1949b). Il s’agit
certainement de Jean-Louis Costa, premier directeur (1945-1951) de
l’Éducation surveillée. Selon Yves Desrichard (2001: 65), forts de leur
renommée liée au cinéma du Front populaire, Duvivier et Jeanson auraient
sollicité directement l’aide de François Mitterrand pour éviter une
censure de leur film. De toute façon, d’un point de vue légal, c’est
Mitterrand qui a le dernier mot pour tout visa d’exploitation en tant
que secrétaire d’État chargé de l’Information[^6].

Pendant ce temps, les négociations entre Maurice Cloche et le ministère
de la Justice suivent un chemin différent. Malheureusement, le dossier
de censure de *La Cage aux filles* est inaccessible[^7], mais plusieurs
documents signalent l’intervention de l’État pendant la production du
film dans le but de susciter une représentation favorable de l’Éducation
surveillée. Tandis que le ministère de la Justice ne parvient pas à
imposer une réécriture d’*Au royaume des cieux*, il dispose d’un levier
supplémentaire pour orienter le contenu du film de Cloche. Une part
significative du financement de *La Cage aux filles* provient du Crédit
national, un organisme public qui prête des fonds à l’industrie
cinématographique par le biais d’un système d’avance sur recettes
(Creton, 2004: 217). Dans la première lettre présentant ses objections à
la réalisation des deux longs métrages, Robert Lecourt préconise déjà la
possibilité de se servir de ce dispositif de financement comme moyen de
pression (Lecourt, 1949a). Le dossier de financement fait état d’une
série d’allers-retours assez typiques entre la maison de production,
Les Films Maurice Cloche, et le Crédit national, discutant du montant de
l’avance et des changements dans le devis du film, avec un élément
supplémentaire : l’avance est conditionnée à l’accord du ministère de la
Justice. Toutefois, ni le Crédit national, ni la maison de production
n’évoque cette condition comme étant particulièrement contraignant ou
indésirable. Dès sa première demande de financement, envoyée le
16 décembre 1948 – soit bien avant la présentation du scénario à la
pré-censure –, Cloche (1948) mentionne sa coopération avec la direction
de l’Éducation surveillée et le ministère de la Justice. D’autres
documents datés de février 1949, pendant la préparation du tournage,
mentionnent un accord verbal de la direction du CNC, sous réserve de
l’avis du ministère de la Justice. Le premier rapport de la commission
d’attribution des avances du Crédit national ajoute deux nouveaux
détails : le secrétariat d’État à l’Information, qui a une tutelle sur
le CNC, « a verbalement donné un avis défavorable sur le scénario » et
« M. Maurice Cloche doit lire ce jour (7 mars 1949) son découpage aux
membres de la Commission compétente » constituée par le ministère de la
Justice (Crédit national, 1949). Effectivement, le lendemain, Robert
Lecourt (1949b) écrit à la direction du CNC pour donner des nouvelles de
ce rendez-vous : le nouveau scénario, « très sensiblement différent du
texte primitif, ne se heurtera vraisemblablement pas aux mêmes
objections de ma part que le précédent ».

Sans disposer d’une copie de cette version antérieure, il est impossible
de savoir quels changements précis le réalisateur a apportés[^8].
Pourtant, la lettre du ministre de la Justice, qui fait partie du
dossier de censure d’*Au royaume des cieux*, fournit quelques
informations intéressantes. Avant tout, elle montre que Maurice Cloche
est obligé de remanier de son scénario afin de satisfaire aux objections
du ministère de la Justice. Elle confirme aussi ce qu’on peut deviner au
ton des communications dans le dossier de financement de *La Cage aux
filles* : que le rapport entre Cloche et les représentants de l’État qui
censurent son projet est un arrangement collégial plutôt qu’un bras de
fer. Ce rapport continue par la suite, lorsque la maison de production,
afin d’obtenir l’approbation finale du ministre, nécessaire pour
percevoir l’avance du Crédit national, invite le ministre à envoyer un
« conseiller technique qui suivrait nos prises de vues et pourrait, le
cas échéant, sur le plateau même, donner à Maurice Cloche certaines
indications de détail » (Les Films Maurice Cloche, 1949). Le scénario
reçoit finalement l’approbation écrite du garde des Sceaux le
1<sup>er</sup> juin 1949, ce qui permet le versement de l’avance par le
Crédit national et le début des prises de vue (Mahias, 1949).

Les lettres de Robert Lecourt montrent que celui-ci surveille en
parallèle les projets qui deviendront *La Cage aux filles* et *Au
royaume des cieux*. Au fur et à mesure de leur avancée, il s’inquiète de
moins en moins du contenu de *La Cage aux filles*, modifié par son
réalisateur pour se conformer aux indications du ministère, en même
temps qu’il devient de plus en plus exaspéré par *Au royaume des cieux*,
dont le réalisateur négocie avec les frères Mitterrand au secrétariat
d’État à l’Information. En raison des différentes sensibilités
politiques présentes au sein du gouvernement de coalition, chaque
cinéaste peut traiter avec un interlocuteur différent. Duvivier,
cinéaste associé au Front populaire (quoique l’orientation politique de
son cinéma soit une question bien plus complexe, et que ses
positionnements idéologiques soient loin de correspondre à ceux du Front
populaire), obtient l’aide d’un secrétaire d’État à l’Information d’un
parti de centre-gauche[^9], contre les tentatives de censure d’un garde
des Sceaux chrétien-démocrate. En revanche, Cloche, qui saupoudre son
film de clins d’œil anti-laïques et anti-syndicaux, reçoit un avis
défavorable de la part du secrétariat à l’Information à la suite de
réserves similaires du garde des Sceaux ; il choisit de modifier son
film en coopération étroite avec le ministère de la Justice.

## Le consensus

À première vue, *Au royaume des cieux* et *La Cage aux filles* peuvent
donner l’impression d’une vision consensuelle de la France de l’époque.
Ils appartiennent à une catégorie de films de l’après-guerre qui
évoquent des questions sociales avec un certain esprit engagé, mais en
évitant de faire de vague : citons, à titre d’exemple, un film sur la
délinquance juvénile comme *Chiens perdus sans collier* (Jean Delannoy,
1955), d’autres œuvres de Maurice Cloche évoquant la prostitution ou la
prison, la filmographie de Ralph Habib ou encore des films de Jean-Paul
Le Chanois comme *L’École buissonnière* (1949) ou *Papa, Maman, la Bonne
et moi* (1954). Il serait facile de croire que ces œuvres découlent d’un
accord de fond réunissant la classe politique et la corporation
cinématographique, voire la France entière. Pourtant, le résultat final
projeté sur les écrans cache une réalité plus complexe. À cette époque,
les arbitrages autour des films politiquement sensibles peuvent
impliquer de nombreuses parties intéressées, au pouvoir inégal et aux
intérêts divers : les cinéastes, les producteurs, les censeurs du CNC
(la moitié représentant les ministères de l’État, l’autre moitié les
professions cinématographiques), des administrations dépeintes dans un
film, des organismes de financement et, dans le cas exceptionnel que
nous venons d’exposer, deux membres du gouvernement qui interviennent
personnellement. Il s’agit alors d’un cinéma fondé sur de nombreux
compromis plutôt que sur un véritable consensus.

D’après Frédéric Hervé (2015: 499), « entre 1945 et 1975, les censeurs
sont les coproducteurs de chacun des films réalisés en France, sans
exception aucune ». Les archives autour d’*Au royaume des cieux* et de
*La Cage aux filles* sont un exemple de la façon dont cette
« coproduction » fonctionne dans la période de l’après-guerre : il n’est
pas impossible pour les cinéastes de traiter d’un sujet politiquement
sensible, mais toute tentative déclenche une série de tractations à la
fois cordiales et coercitives. Ces cas confirment que la censure est
consentie au sein de l’industrie à cette époque, mais révèlent aussi ce
qui a déjà été démontré par des travaux sur la censure cinématographique
dans d’autres contextes historiques : il s’agit souvent d’un processus
collaboratif, voire producteur de sens, plutôt qu’une ingérence
verticale par des institutions répressives et éloignées de la production
des films (voir, par exemple, Kuhn, 1989: 2–6; Staiger, 1995: 13–15).

L’idée d’un cinéma fondé sur le compromis reste cohérente avec
l’hypothèse d’un système poussant les cinéastes à l’autocensure. Pour la
plupart des projets de films, les négociations consistent surtout en des
échanges entre le producteur et la commission de contrôle avant le
tournage du film, un cas de figure prévu par le service de
« pré-censure » proposé par le CNC. Ce dispositif incite les sociétés de
production à remanier les scénarios, en suivant les consignes de la
commission, afin d’éviter le tournage coûteux de scènes qui risqueraient
*in fine* la censure. Laurent Garreau (2009: 46) rappelle que la
pré-censure conduit souvent à « des négociations qui s’inscrivaient à la
fois dans la poursuite d’une meilleure qualité des productions
françaises, dans l’écoute la plus attentive possible au respect des
droits des scénaristes et dans le souci de ne pas trop compromettre
l’exercice de la liberté d’opinion. » Cependant, de tous les moyens dont
dispose la commission de contrôle, c’est la pré-censure « qui peut jouer
le rôle le plus directement normatif et avoir l’action la plus
coercitive et dissuasive ». Il est raisonnable de supposer, en suivant
la logique de Marcel Martin, que dans plus de cas encore les seules
négociations à avoir lieu sont celles entre cinéastes et producteurs
– sans parler des œuvres que les cinéastes imaginent mais n’osent même
pas proposer. Ces dernières formes d’autocensure laisseraient peu ou pas
d’archives écrites.

Cependant, la censure n’est pas le seul objet des arbitrages. L’histoire
d’*Au royaume des cieux* et *La Cage aux filles* montre comment les
cinéastes, en subissant des pressions mais aussi de leur propre gré,
pouvaient représenter avec indulgence les institutions et évoquer
l’actualité de manière faussée. Une fois épinglé par le ministre de la
Justice, Maurice Cloche donne son entière coopération à l’entreprise
promotionnelle en faveur de la direction de l’Éducation surveillée.
Julien Duvivier échappe aux pressions visant à l’obliger à faire de
même, mais il accepte de discuter avec un second groupe de politiques et
de hauts fonctionnaires, et son film finit par propager de nombreuses
idées reçues appuyant une vision partielle et partiale des politiques de
l’État. D’où un film aussi « consensuel » que celui de Cloche, qui
dresse un portrait tout aussi déformé de la justice pour mineurs.

Dans son ouvrage sur les drames en costumes français des années 1950,
Susan Hayward (2010: 87–88) constate que ces films, loin d’être
apolitiques ou de soutenir une vision de la société teintée d’optimisme,
traduisent souvent la gêne et l’angoisse ambiantes devant les
bouleversements économiques et sociaux de l’époque. De manière
similaire, Thomas Pillard (2014: 321–322) observe comment les films
noirs de l’après-guerre véhiculent les angoisses provoquées notamment
par l’Occupation et les débuts d’une société de consommation encore
inaccessible à de nombreux Français. À la différence de la plupart des
drames en costumes et des films policiers, *Au royaume des cieux* et
*La Cage aux filles* sont des œuvres inspirées de faits réels qui
traitent directement un thème d’actualité. La gêne y est d’autant plus
transparente : aujourd’hui encore, comme pour de nombreux spectateurs
d’époque, à l’image de Jeander ou Alexis Danan, on perçoit facilement le
malaise avec lequel les cinéastes et les autorités ont abordé ensemble
des questions sociales. Cependant, l’existence même de ces films remet
en question l’affirmation de Jean-Pierre Jeancolas que le cinéma de
l’après-guerre « se tient prudemment à bonne distance de la réalité
française ». Au contraire, il était tout à fait possible pour les
cinéastes d’aborder de nombreuses questions sociales et politiques, à
condition de se prêter au jeu de censure négociée qui, certes, pouvait
dénaturer l’image de la réalité qui allait finalement être projeté sur
les écrans. Le résultat ici est deux films qui traitent du monde
contemporain avec gêne, qui répètent des idées reçues et des vœux pieux
plutôt que de se plonger dans les véritables difficultés de la
rééducation des jeunes femmes délinquantes. Cloche et Duvivier engagent
un dialogue autour de ces sujets tout de même – des sujets d’une
actualité brûlante, à en juger par la popularité des films auprès des
spectateurs ou par l’inquiétude du ministre de la Justice.

## Remerciements

Un grand merci à François Albera, Dunja Jelenković et Audrey Orillard
pour leurs relectures et retours sur ce texte.

## Références

Aurouet C (2005a) De *l’Île des enfants perdus* à *la Fleur de l’âge* :
le projet chaotique et mythique de Marcel Carné et Jacques Prévert.
*1895. Mille huit cent quatre-vingt-quinze* (47): 96‑133. DOI:
[10.4000/1895.330](https://doi.org/10.4000/1895.330).

Aurouet C (2005b) Prévert et Carné : *La Fleur de l’âge*. *Positif*
(535): 68‑72.

Bantigny L (2007) *Le Plus Bel Âge ? Jeunes et jeunesse en France de
l’aube des « Trente Glorieuses » à la guerre d’Algérie*. Paris: Fayard.

Barrot J-P (1953) Une tradition de la qualité. In: *Sept ans de cinéma
français : 1945-1952*. 7e Art. Paris: Le Cerf, p. 25‑37.

Berger P (1949) Julien Duvivier va installer très bientôt *Le Royaume
des cieux* derrière une prison sans barreaux. *Paris-presse*, 14
février.

Bonnefille É (2002) *Julien Duvivier : le mal aimant du cinéma français,
1940-1967*. Paris: L’Harmattan.

Borde R (1978) Préface. In: *Les Malédictions du cinéma français : une
histoire du cinéma français parlant, 1928-1978*. Paris: Alain Moreau, p.
7‑21.

Burch N (2000) *Double speak*. De l’ambiguïté tendancielle du cinéma
hollywoodien. *Réseaux. Communication - Technologie - Société* 18(99).
Hermès Science Publications: 99‑130. DOI:
[10.3406/reso.2000.2197](https://doi.org/10.3406/reso.2000.2197).

Burch N et Sellier G (2005) *La Drôle de guerre des sexes du cinéma
français, 1930-1956*. Paris: Armand Colin.

Carroll N (1996) Film, Rhetoric, and Ideology. In: *Theorizing the
Moving Image*. Cambridge studies in film. Cambridge (Angleterre):
Cambridge University Press, p. 275‑289.

Chauvière M (1998) Question pour un non-évenement : quelles alternatives
à l’Éducation surveillée en 1945 ? *Revue d’histoire de l’enfance
« irrégulière ». Le Temps de l’histoire* (1): 41‑54. DOI:
[10.4000/rhei.8](https://doi.org/10.4000/rhei.8).

Chirat R (1985) *La <span class="smallcaps">iv</span><sup>e</sup>
République et ses films*. Bibliothèque du cinéma. Paris: Hatier.
Available at: <http://www.sudoc.fr/005165350> (consulté le 16 septembre
2012).

Cloche M (1948) *Lettre à Xavier Flûry*. Dossier CN368-B255, Fonds du
Crédit national, 16 décembre. Paris: Cinémathèque française/Bibliothèque
du film.

Commission de contrôle (1949a) *Avis de censure*. Dossier de censure
d’*Au royaume des cieux*, visa n<sup>o</sup> 8710, 8 juillet. Paris:
Centre national de la cinématographie.

Commission de contrôle (1949b) *Compte rendu de pré-censure*. Dossier de
censure d’*Au royaume des cieux*, visa n<sup>o</sup> 8710, 23 février.
Paris: Centre national de la cinématographie.

Crédit national (1949) *Résumé du rapport sur *La Cage aux Filles*,
premier examen*. Dossier CN368-B255, Fonds du Crédit national, 7 mars.
Paris: Cinémathèque française/Bibliothèque du film.

Creton L (2004) *Histoire économique du cinéma français: production et
financement, 1940-1959*. Paris: CNRS éditions.

Danan A (1950) Cage dorée vaut mieux que cage noire… mais pas de cage du
tout serait bien préférable. *Le Franc-tireur*, 18 janvier.

Danjou H (1947) De haute lutte on délivre les « mitardes » qui
croupissent dans les cachots. *France-soir*, 22 mai.

Danjou H et Sollacaro G (1949a) Après l’enfer de Fresnes, voici le
paradis de Brécourt. *France-soir*, 3 juillet.

Danjou H et Sollacaro G (1949b) « Je veux mourir… je recommencerai ! »
gémissait Claudine après avoir bu un litre de crésyl. *France-soir*, 17
juin.

Danjou H et Sollacaro G (1949c) « Ouvrez la grille… Je vous écrase la
tête ! » hurlait Matelote transportée de force au cachot. *France-soir*,
16 juin.

Denis S (2009) *Le Cinéma et la guerre d’Algérie. La propagande à
l’écran, 1945-1962*. Histoire et cinéma. Paris: Nouveau Monde éditions.

Desrichard Y (2001) *Julien Duvivier : cinquante ans de noirs destins*.
Courbevoie/Paris: Durante/BiFi.

Ellul J (1990) *Propagandes*. Paris: Economica.

Fayard J (1949) *Au royaume des cieux* est un reportage où il y a du bon
et du moins bon. *Opéra*, 5 octobre.

Gallinari P (2015) *Les Communistes et le cinéma : France, de la
Libération aux années 60*. Rennes: Presses universitaires de Rennes.

Garreau L (2009) *Archives secrètes du cinéma français, 1945-1975*.
Paris: Presses universitaires de France.

Gaston-Mathé C (1996) *La Société française au miroir de son cinéma : de
la débâcle à la décolonisation*. Condé-sur-Noireau: Arléa-Corlet.

Hayward S (2010) *French Costume Drama of the 1950s: Fashioning Politics
in Film*. Bristol: Intellect.

Hervé F (2015) *Censure et cinéma dans la France des trente glorieuses*.
Paris: Nouveau Monde éditions. Available at:
<http://catalogue.bnf.fr/ark:/12148/cb44311640w> (consulté le 1 avril
2017).

Houbre G (2000) Rééduquer la jeunesse délinquante sous Vichy : l’exemple
du « Carrefour des enfants perdus » de Léo Joannon. *Revue d’histoire de
l’enfance « irrégulière ». Le Temps de l’histoire* (3). Numéro 3.
Presses universitaires de Rennes: 159‑177. DOI:
[10.4000/rhei.75](https://doi.org/10.4000/rhei.75).

Jeancolas J-P (2019) *Histoire du cinéma français* (éd. M Marie).
4<sup>e</sup> éd. augmentée. Focus cinéma. Malakoff: Armand Colin.

Jeander \[Jean Derobe\] (1950) *La Cage aux filles*. *Libération*, 20
janvier.

Jobs RI (2007) *Riding the New Wave: Youth and the Rejuvenation of
France after the Second World War*. Stanford: Stanford University Press.

Kuhn A (1989) *Cinema, Censorship and Sexuality, 1909-1925*. London/New
York: Routledge.

Lecourt R (1949a) *Lettre à Michel Fourré-Cormeray*. Dossier de censure
d’*Au royaume des cieux*, visa n<sup>o</sup> 8710, 26 février. Paris:
Centre national de la cinématographie.

Lecourt R (1949b) *Lettre à Michel Fourré-Cormeray*. Dossier de censure
d’*Au royaume des cieux*, visa n<sup>o</sup> 8710, 8 mars. Paris: Centre
national de la cinématographie.

Les Films Maurice Cloche (1949) *Lettre à Robert Lecourt*. Dossier
CN368-B255, Fonds du Crédit national, 13 mai. Paris: Cinémathèque
française/Bibliothèque du film.

Léwy (1949) *Avis de pré-censure*. Dossier de censure d’*Au royaume des
cieux*, visa n<sup>o</sup> 8710, 23 février. Paris: Centre national de
la cinématographie.

Lindeperg S (2000) Engagement politique et création cinématographique,
les trajectoires de Louis Daquin, Jean Grémillon et Jean-Paul Le
Chanois. In: Bertin-Maghit J-P (éd.) *Les Cinémas européennes des années
cinquante*. Paris: AFRHC, p. 55‑98.

Lindeperg S (2014) *Les Écrans de l’ombre. La Seconde Guerre mondiale
dans le cinéma français, 1944-1969*. nouvelle éd. augmentée. Paris:
Éditions du Seuil.

Mahias P (1949) *Lettre à Marcel Bertrou*. Dossier CN368-B255, Fonds du
Crédit national, 1 juin. Paris: Cinémathèque française/Bibliothèque du
film.

Martin M (1984) *Le Cinéma français depuis la guerre*. Paris: Edilig.

Michel J (1949) *Au royaume des cieux* : cette fois le septième art ne
mène pas au septième ciel. *Le Parisien libéré*, 5 octobre.

Mitterrand R (1949) *Lettre à Michel Fourré-Cormeray*. Dossier de
censure d’*Au royaume des cieux*, visa n<sup>o</sup> 8710, 10 mars.
Paris: Centre national de la cinématographie.

Morienval J (1949) Un scénario discutable, mais un film de grande
classe : *Au royaume des cieux*. *L’Aube*, 13 octobre.

*Paris-presse* (1949) Une complainte populaire qui fera pleurer Margot.
30 septembre.

Pillard T (2014) *Le Film noir français face aux bouleversements de la
France d’après-guerre (1946-1960)*. Nantes: Joseph K.

Sellier G (2015) Le cinéma populaire et ses usages dans la France
d’après-guerre. *Studies in French Cinema* 15(1): 1‑10. DOI:
[10.1080/14715880.2014.996453](https://doi.org/10.1080/14715880.2014.996453).

Shklar JN (1998) Political Theory and the Rule of Law. In: *Political
Thought and Political Thinkers*. Chicago: University of Chicago Press,
p. 21‑37.

Simsi S (2012) *Ciné-passions : le guide chiffré du cinéma en France*.
Paris: Dixit.

Sorlin P (2015) *Introduction à une sociologie du cinéma*. Paris:
Klincksieck.

Staiger J (1995) *Bad Women: Regulating Sexuality in Early American
Cinema*. Minneapolis: University of Minnesota Press.

Thomazeau A (2005) Entre éducation et enfermement : le rôle de
l’éducatrice en internat de rééducation pour filles, de la Libération au
début des années 1960. *Revue d’histoire de l’enfance « irrégulière ».
Le Temps de l’histoire* (7): 147‑171. DOI:
[10.4000/rhei.1108](https://doi.org/10.4000/rhei.1108).

Thomazeau A (2007a) La rééducation des filles en internat (1945-1965).
*Histoire de l’éducation* (115-116): 225‑246. DOI:
[10.4000/histoire-education.1427](https://doi.org/10.4000/histoire-education.1427).

Thomazeau A (2007b) Violence et internat : les centres de rééducation
pour filles, en France, de la Libération au début des années 1960.
*Revue d’histoire de l’enfance « irrégulière ». Le Temps de l’histoire*
(9): 107‑125. DOI:
[10.4000/rhei.2163](https://doi.org/10.4000/rhei.2163).

Vivet J-P (1949) Le problème de la jeunesse délinquante porté à l’écran
: « La Cage aux filles », une réalisation de Maurice Cloche. *Combat*, 9
juillet.

Williams A (1992) *Republic of Images: A History of French Filmmaking*.
Cambridge (Massachusetts): Harvard University Press.

[^1]: Notons que Jeanson, scénariste prolifique qui a aussi mené une
activité de journaliste, est un collaborateur de Duvivier de longue
date : il a participé à l’écriture de *Pépé le Moko* (1937) et *Un
carnet de bal* (1937). Par ailleurs, il a déjà une expérience liée aux
maisons de rééducation pour jeunes femmes : c’est le dialoguiste de
*Prison sans barreaux* (Léonide Moguy, 1938), un film auquel plusieurs
critiques comparent *Au royaume des cieux* et *La Cage aux filles*.

[^2]: Bien qu’il ne figure pas dans leur corpus, *Au royaume des cieux*
illustre clairement la thèse de Noël Burch et Geneviève Sellier, qui
voient dans le cinéma de l’après-guerre une présence accrue de poncifs
misogynes et de figures féminines (auto)destructrices. (Voir Burch et
Sellier, 2005: 224–237)

[^3]: Ces lectures opposées rappellent le phénomène que discerne Noël
Burch dans le cinéma hollywoodien, certains films pouvant susciter à la
fois des lectures de gauche et de droite. (Voir Burch, 2000)

[^4]: Le critique fait référence à une séquence montée en parallèle ou
Maria et Pierre rêvent l’un de l’autre.

[^5]: Mouvement républicain populaire. Parti chrétien-démocrate fondé à la
Libération, il fait partie de la « troisième force » politique face aux
Communistes et aux Gaullistes dans la Quatrième République.

[^6]: Curieusement, François Mitterrand est également impliqué dans
l’histoire de *La Fleur de l’âge* de Marcel Carné : celui-ci affirme
avoir projeté les rushes de son film incomplet à Mitterrand et sa
belle-sœur Christine Gouze-Rénal au début des années 1950. Les bobines
auraient disparu avant que Carné revienne les chercher le lendemain.
(Voir Aurouet, 2005a)

[^7]: En raison de la fermeture du site de Fontainebleau des Archives
nationales depuis 2013.

[^8]: D’après le dossier Crédit national, la seconde version du scénario
suscite une révision à la baisse du budget du film, de 65,8 millions à
47,7 millions de francs. Il est donc probable que les changements du
scénario visent aussi à faire des économies.

[^9]: De 1946 à 1965, François Mitterrand est membre, puis dirigeant, de
l’Union démocratique et socialiste de la résistance (UDSR), parti qui
participe à plusieurs gouvernements de la « troisième force » aux côtés
du MRP, des Socialistes et des Radicaux.
