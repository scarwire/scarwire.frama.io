---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "La rhétorique judiciaire dans le cinéma français des années 1950"
event: "La rhétorique filmée, 1926-1960 : France, Allemagne, États-Unis"
event_url: http://www-artweb.univ-paris8.fr/?Colloque-Rhetorique-filmee-1927
location: Université Paris 8 Vincennes-Saint-Denis, Maison de la recherche
address:
  street: 2 rue de la Liberté
  city: Saint Denis
  region:
  postcode: 93200
  country:
summary:
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2019-06-06T15:30:00+01:00
#date_end: 2019-11-20T14:54:42+01:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2019-11-20T14:54:42+01:00

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: research_resources/rhetorique-diapos.pdf

url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Dans les années 1950, une poignée de longs métrages français proposent des représentations de la justice pénale et des tribunaux : au sein d'un cinéma populaire, réputé apolitique, ces séquences ont de quoi surprendre. Le plus souvent, ce sont les accusés eux-mêmes, et non pas leurs avocats, qui occupent la place centrale dans la rhétorique filmique. Dans cette communication, j'analyse deux stratégies rhétoriques opposées qui véhiculent un discours politique autour de la justice, celles de Sacha Guitry et d'André Cayatte.

Tandis que le premier construit sa rhétorique autour du _logos_ (la parole) et de l'_êthos_ (la personnalité de l'orateur), le second privilégie une rhétorique fondée avant tout sur le _pathos_ (compassion pour des personnages qui ont souffert) et le _kairos_ (intervention du discours à un moment oppportun). Deux comédies de Sacha Guitry, _La Poison_ (1951) et _Assassins et voleurs_ (1957), construisent l'héroïsme de leurs protagonistes à partir de la maîtrise de la parole. Dans ces films, souvent critiqués comme étant du théâtre filmé, la rhétorique est clairement énoncée par le protagoniste. Dans _La Poison_, le héros, incarné par Michel Simon, convainc un jury d'assises du fait qu'il avait raison de tuer sa femme. La morale qui en résulte, drôle, anticonformiste et misanthrope, a déjà été épinglé pour son aspect misogyne. Elle relève aussi de l'anarchisme de droite : la loi s'appliquerait au commun des mortels, mais pas à quelques êtres exceptionnels dotés d'un pouvoir de persuasion hors norme -- comme le personnage de Michel Simon et son alter ego, Guitry.

Plusieurs drames sociaux datant de la même époque développent une stratégie aux antipodes de celle-ci, parmi lesquels les films d'André Cayatte, lui-même ancien avocat. Par exemple, _Nous sommes tous des assassins_ (Cayatte, 1952) incite les spectateurs à compatir avec un protagoniste pauvre et illettré, incapable de se défendre devant ses juges, accusé de méfaits dont il n'est pas pleinement responsable. La rhétorique du film ne découle pas de la parole du héros, ni de celle de ses défenseurs, mais du montage et de l'agencement du récit. De même, c'est l'incapacité du héros à prendre la parole qui construit la rhétorique de _Crainquebille_ (Ralph Habib, 1954, adapté du roman d'Anatole France de 1901). Le résultat est une remise en cause de l'accusation qui pousse le public à questionner le bien-fondé de la justice répressive.

Entre ces deux types de film aux stratégies discursives opposées, les critiques des _Cahiers du cinéma_, les plus influents de l'époque, choisissent leur camp. François Truffaut fait l'éloge du cinéma de Guitry, malgré sa théâtralité, parce qu'il le trouve réaliste. Quant au cinéma de Cayatte, il le trouve « grotesque de fausseté » ; selon le futur fer de lance de la Nouvelle Vague, Cayatte « n'est pas un artiste ». André Bazin publie un article plus subtil sur Cayatte, mais tire les mêmes conclusions. Il fustige l'utilisation de personnages unidimensionnels dans le but de construire un argument persuasif : pour le doyen des _Cahiers_, il s'agirait d'un faux réalisme, un mauvais emploi du médium cinématographique. En analysant les représentations du tribunal dans ces films, je souhaiterais cerner le rapport qu'ils créent entre réalisme et stratégie rhétorique, et interroger les jugements des critiques des _Cahiers_ : la méthode de Guitry est-elle vraiment plus proche du réel que celui de Cayatte ?
