---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Critical censors, censorious critics, and notions of quality in French cinema, 1945-1958"
event: "Screening Censorship: New Histories, Perspectives, and Theories on Film and Screen Censorship, Ghent/Brussels"
event_url: http://www.censorship-symposium.org/
location: University of Ghent, online conference
address:
  street: 
  city: 
  region:
  postcode: 
  country:
summary:
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2020-10-16T10:30:00+01:00
#date_end: 2019-11-20T14:54:42+01:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2020-06-01T14:54:42+01:00

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: research_resources/censorship+criticism.pdf
url_code:
url_pdf:
url_video: https://audrey2.freemyip.com/nextcloud/s/5pzYcrsnbTGgpo7

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

<video controls="" src="https://audrey2.freemyip.com/nextcloud/s/5pzYcrsnbTGgpo7/download">
  <source src="https://audrey2.freemyip.com/nextcloud/s/5pzYcrsnbTGgpo7/download" type="video/mp4">
</video>

In 1946, the critics Jeander (the pen name of Jean Derobe) and Georges Sadoul pitched a proposal to the CNC, the newly formed state agency overseeing the French film industry: film censorship, they argued, should be based on the sole criterion of quality. Although the proposal was ultimately scrapped, it did initially attract interest from Georges Huisman, the first president of the CNC's censorship board. Critics continued to play a role in film censorship throughout the late 1940s and 1950s: one seat on the censorship board was always filled by a critic, and the board occasionally voted to censor films based on considerations of quality. The first part of this paper will be devoted to an analysis of archival documents presenting the critics' project, as well as several examples of films which were restricted for reasons related to quality. How did Jeander and Sadoul define quality and was was their rationale for using it as a basis for censorship? What was at stake in censoring based on quality during this era, before the distinction between art cinema and popular cinema was fully developed (and before quality became a common benchmark used by censors to distinguish art cinema from pornography)?

Several years later, in 1954, François Truffaut famously maligned the “quality” French films that dominated screens at the time, attacking them as overly academic and turning the very notion of quality on its head. In the very same issue of Cahiers du cinéma, Truffaut also expressed his admiration for Hollywood’s Production Code, drawing a distinction between moral censorship, which he supported, and censorship aiming to protect the stature of public institutions, which he claimed to oppose. The influential Cahiers critics often took moralistic stances against films they disapproved of, and in his famous article “A certain tendency of French cinema,” Truffaut focused his objections on films that he found “profane” or “blasphemous,” or which took anti-clerical or anti-militarist stances. Not only did the Cahiers critics’ promotion of the directors they identified as auteurs constitute a manifesto for a new kind of cinema, but their dismissal of films and filmmakers they disliked marginalized this “quality” cinema, in the long term removing it from classrooms, retrospectives, and film canons.

This contribution will thus examine the sometimes unclear boundaries between censorship and criticism in France from the end of World War Two to the beginning of the New Wave. How and why did critics participate in the censorship process? Were the CNC’s censors, who rarely banned films and most often acted through negotiations with producers, ultimately acting like just another pressure group? Were the Cahiers critics actually engaging in a form of censorship? Above all, when censors and critics claimed to be evaluating the quality of films, were they actually trying to reshape cinema according to their political preferences?

# Principal sources

Association française de critique de cinéma. Proposed law on film censorship, May 4, 1946. French National Archives, Pierrefitte-sur-Seine, series F/41/2375.

Bæcque, Antoine de. _Les Cahiers du cinéma : histoire d’une revue._ Vol. 1. Paris: Cahiers du cinéma, 1991.

—. _La cinéphilie : invention d'un regard, histoire d'une culture, 1944-1968_. Paris: Fayard, 2003

Bunn, Matthew. “Reimigining repression: new censorship theory and after.” _History and Theory_ no. 54 (February 2015): 25--44, [doi:10.1111/hith.10739](http://dx.doi.org/10.1111/hith.10739).

Douin, Jean-Luc. _Dictionnaire de la censure au cinéma: images interdites._ Paris: Presses universitaires de France, 2001.

Hervé, Frédéric. _Censure et cinéma dans la France des trente glorieuses._ Paris: Nouveau Monde éditions, 2015.

—. _La censure du cinéma en France à la Libération_. Paris: ADHE, 2001.

Hess, John. “La politique des auteurs, 1: world view as aesthetics.” _Jump Cut_, no. 1 (May-June 1974): 19--22, <http://www.ejumpcut.org/archive/onlinessays/JC01folder/auturism1.html>.

—. “La politique des auteurs, 2: Truffaut’s manifesto.” _Jump Cut_, no. 2 (July--August 1974): 20--22, <http://www.ejumpcut.org/archive/onlinessays/JC02folder/auteur2.html>.

Jeander \[Jean Derobe\], “Petite histoire de la censure.” _Image et son_, no. 140-141 (April--May 1961): 3.

Staiger, Janet. _Bad Women: Regulating Sexuality in Early American Cinema_. Minneapolis: University of Minnesota Press, 1995.

Truffaut, François. “Une certaine tendance du cinéma français.” _Cahiers du cinéma_, no. 31 (January 1954): 15--29.

—. “Aimer Fritz Lang.” _Cahiers du cinéma_, no. 31 (January 1954): 52--54.
