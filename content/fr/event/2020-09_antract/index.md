---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Censure officieuse et propagande officielle dans les actualités filmées françaises, 1946-1958"
event: "Journée d'études du projet ANR ANTRACT _Histoire mondiale des actualités cinématographiques pendant la seconde moitié du XX<sup>e</sup> siècle_"
event_url: https://antract.hypotheses.org/167
location: Université Paris 1 - Panthéon-Sorbonne, Centre de colloques
address:
  street: 2 rue des Fillettes
  city: Aubervilliers
  region:
  postcode: 93300
  country:
summary:
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2020-09-18T11:00:00+01:00
#date_end: 2019-11-20T14:54:42+01:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2020-06-01T14:54:42+01:00

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: research_resources/antract-slides.pdf
url_code:
url_pdf:
url_video: 

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Cette intervention aborde les images de la justice et de la police dans
les actualités filmées françaises entre 1946 – date de la fin du
monopôle de l'état – et 1958 – le début de la Cinquième République
mais aussi d'une époque où la télévision commence à sérieusement
concurrencer le cinéma en France. Ces sujets, suffisamment politiques
pour être perçus comme sensibles par les représentants du gouvernement,
sans pour autant relever d'un tabou absolu, permettent l'analyse de deux
traits caractéristiques des actualités de l'époque. D'une part, une
censure stricte mais officieuse : en l'absence d'un cadre légal, les
producteurs des actualités projettent leurs bandes devant un comité de
représentants de plusieurs ministères chaque semaine avant leur sortie
en salles. Il s'agit d'une forte incitation à l'autocensure et une
dernière chance pour que les représentants du gouvernement expriment des
réserves quant au contenu des reportages. D'autre part, les actualités
filmées constituent un véritable vecteur de propagande. Les reportages
sur la justice et la police ont tendance à dépolitiser leur sujets et à
promouvoir les politiques menées par les gouvernements successifs. Au
fur et à mesure l'épuration et les procès de crimes de guerre
disparaissent des écrans, des reportages sur des faits divers prennent
leur place. Au sein de ces reportages, la vision globale de la justice,
aseptisée et quasi-officielle, est nettement différente de celle qui est
distillée dans la presse écrite ou encore dans le cinéma de fiction.
