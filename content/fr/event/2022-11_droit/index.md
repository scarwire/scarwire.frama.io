---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Image, parole et témoignage dans les films contre la peine de mort. France, États-Unis, 1945-1981"
event: "Colloque _Filmer le droit, le droit filmé_"
event_url: https://antract.hypotheses.org/167
location: Université Paris 1 - Panthéon-Sorbonne, centre Saint-Charles
address:
  street: 47 rue des Bergers
  city: Paris
  region:
  postcode: 75015
  country:
summary:
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2022-11-19T16:20:00+01:00
#date_end: 2019-11-20T14:54:42+01:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2022-11-18T14:54:42+01:00

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: research_resources/pdm-diapos.pdf
url_code:
url_pdf:
url_video: 

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Dans cette communication, je propose d’étudier un corpus restreint de films de fiction qui ont déployé un argumentation contre la peine de mort – ou qui ont été perçus comme ayant un tel parti pris – dans deux pays avec une expérience très différente de la peine capitale : la France et les États-Unis.

Jadis un spectacle publique, l’exécution est ramenée à l’intérieur des murs des prisons dans les deux pays pendant la première moitié du XXe siècle. Aux États-Unis, des journalistes assistent couramment aux exécutions afin de pouvoir témoigner au public de cette fonction de l’État, mais ils n’ont pas le droit filmer ou de prendre des images photographiques. En France, seuls le défenseur du condamné et quelques magistrats sont admis et -- différence clé par rapport aux États-Unis -- la loi interdit à la presse de publier toute description de l’exécution à part les informations figurant sur le procès-verbal. Ces règles tranchent notamment avec celles de la plupart des états américains, où un certain nombre de représentants de la presse sont toujours convoqués aux exécutions en tant que témoins. La mise en scène de la peine capitale par le cinéma de fiction est donc le principal moyen par lequel le public a accès à des images et à un imaginaire de cette « peine ultime ». De quelle manière les cinéastes ont-ils réussi à informer le public à travers ces images ?

Les stratégies rhétoriques employées par le cinéma contre la peine de mort sont variables. Certains films remettent en cause la culpabilité du condamné afin d’attaquer l’irréversibilité de la peine, alors que d’autres privilégient un personnage de condamné pleinement coupable pour mieux démontrer que même les pires criminels ne méritent pas la peine de mort. De la même manière, certains réalisateurs choisissent de montrer toute la violence de l’exécution pour que les spectateurs comprennent exactement ce que fait l’État en leur nom, tandis que d’autres préfèrent souligner la vie des condamnés pour qu’on voie le contexte de leur crime et de leur condamnation. Nous nous demandons à quel point la manière d’exposer et d’argumenter à l’écran contre la peine de mort dépend du contexte culturel et historique : existe-t-il des stratégies ou des configurations rhétoriques privilégiées qui correspondent à l’un des deux pays, ou à une époque ou une autre ?

En analysant des films tels que _Nous sommes tous des assassins_ (André Cayatte, 1952), _Je veux vivre !_ (_I Want to Live!_, Robert Wise, 1958), _De sang froid_ (_In Cold Blood_, Richard Brooks, 1967), _La vie, l’amour, la mort_ (Claude Lelouch, 1968) et _Le Pull-over rouge_ (Michel Drach, 1979), plusieurs points communs apparaissent dans l'argumentation des films français et américains. D'abord, les films des deux pays ont pour ambition d'éclaircir ce coin obscur du système judiciaire qu'est la salle d'exécution, de reconstituer des images d'une peine dont il est impossible de fournir des images documentaires. En parallèle, ces films opposent la puissance des images à l'éloquence de la parole : dans les scènes de prétoire, les protagonistes qui seront condamnés peinent à se défendre face aux discours des procureurs et des magistrats, mais les films prennent le parti des accusés en racontent leur histoire en images. En revanche, une différence clé se trouve dans la représentation de la presse et des médias dans les films américains : on accède aux exécutions à travers à travers des personnages de journalistes, et ceux-ci, professionnels de la parole, peuvent déployer leurs talents pour le meilleur ou pour le pire.
