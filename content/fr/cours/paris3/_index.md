---
# Page title
title: Université Sorbonne Nouvelle

# Title for the menu link if you wish to use a shorter link title, otherwise remove this option.
linktitle: Sorbonne Nouvelle

# Page summary for search engines.
#summary: Blah, blah, blah...

# Date page published
date: 2023-01-30

# Position of this page in the menu. Remove this option to sort alphabetically.
weight: 10
---
