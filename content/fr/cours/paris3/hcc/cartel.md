---
# Page title
title: Ce que les studios de cinéma refusent de comprendre au streaming
author: Katherine Trendacosta

# Date page published
date: 2021-04-15

# Academic page type (do not modify).
type: book
toc: false

# Position of this page in the menu. Remove this option to sort alphabetically.
weight: 14

#copyright_license:
#- enable: true
#- allow_derivatives: true
#- share_alike: false
#- allow_commercial: true
#- notice: 'Ce billet est publié sous la licence Creative Commons Attribution 2.0 (CC BY 2.0 FR)'
---

par Katherine Trendacosta

*[Texte original en
anglais](https://www.eff.org/deeplinks/2021/04/what-movie-studios-refuse-understand-about-streaming)
publié le 7 avril 2021 sur le blog*
[Deeplinks](https://www.eff.org/deeplinks) *de la [Electronic Frontier
Foundation](https://www.eff.org/), une association basée aux États-Unis
qui promeut la défense de la vie privée et la liberté d'expression en
ligne. Traduction par Daniel Morgan.*

{{< figure src="../robocop.jpg" title="Une grosse société maléfique. _Robocop_ (Paul Verhoeven, 1987)" >}}

Plus nous nous habituons à vivre dans le monde numérique, plus nous nous
apercevons que ce monde reproduit des problématiques et des luttes que
nous connaissons depuis des décennies. Dans le cas des studios de
cinéma, ce que nous observons depuis quelques années fait écho à ce qui
s'est produit dans les années 1940 et 1950, lorsqu'un petit groupe de
studios dirigeaient aussi les salles qui projetaient leurs films.
En 1948, la Cour suprême a déclaré que cette pratique des studios était
en violation des lois *antitrust* sur la libre concurrence et ils ont
été contraints de signer un accord avec le ministère de la Justice
américaine. Le ministère a mis fin à cette décision en 2019 en
[affirmant](https://www.hollywoodreporter.com/thr-esq/justice-department-moves-terminate-paramount-consent-decrees-1255858)
que les studios actuels n'étaient pas en mesure de « rétablir leur
cartel ». Certes, pas dans des salles de cinéma physiques. Mais en
ligne, c'est une autre histoire.

Dans les années 1930 et 1940, le problème était que les principaux
studios -- y compris la Warner Brothers et Universal, qui existent
encore aujourd'hui -- étaient propriétaires de *tout* ce qui était lié
aux films qu'ils produisaient. Tous leurs employés travaillaient sous
des contrats exclusifs et restrictifs. Ils détenaient la propriété
intellectuelle de leurs films. Ils contrôlaient même les laboratoires
qui développaient la pellicule. Et, bien sûr, ils étaient propriétaires
des salles de cinéma.

En 1948, ayant perdu à la Cour suprême, les studios ont été contraints
de vendre leurs parts dans les salles individuelles et les réseaux de
salles.

Cela a entraîné des avantages évidents pour les spectateurs. Avant 1948,
les séances étaient programmées de façon à ne jamais se chevaucher ; il
était impossible de regarder un film à la salle la plus proche à l'heure
qui *vous* convenait. Les studios obligeaient les gérants de salles à
acheter la liste entière de leurs productions sans les avoir vus (une
pratique qu'on appelait « l'achat à l'aveugle »), plutôt que de leur
permettre de choisir les meilleurs films ou les plus intéressants
-- ceux qui pouvaient attirer les spectateurs. Bien entendu, les plus
grandes sociétés d'exploitation de salles de cinéma et les salles qui
appartenaient aux studios bénéficiaient d'un traitement préférentiel.

Les tribunaux ont mis fin à ces pratiques pour une bonne raison. La
séparation entre les salles de cinéma et les studios a permis aux salles
locales de proposer une plus grande variété de films au public, des
œuvres que les spectateurs étaient plus susceptibles de vouloir
regarder, à des horaires plus commodes. Les spectateurs n'étaient plus
dans l'obligation de chercher une obscure combinaison d'horaire,
d'endroit et de film.

À l'heure actuelle, en 2021, si vous consommez des médias numériques,
vous auriez pu remarquer quelque chose de similaire.

La première vague des services de streaming -- Hulu, Netflix,
iTunes, etc. -- proposaient un éventail de contenus provenant de
nombreux studios différents. Quant au catalogue de contenus déjà
diffusés à la télévision, ils étaient proposés au complet dans les
catalogues de chacun de ces services, qui offraient aux spectateurs tous
les épisodes à la fois. C'était la grande ascension du *binge watching*.

Comme pour la salle de cinéma locale, la valeur ajoutée de ces services
était la commodité pour le spectateur. Pour un prix fixe, on pouvait
choisir au sein d'un large catalogue et regarder ce qu'on voulait, quand
on le voulait, sans quitter la maison. Comme ils l'ont fait il y a
presque 100 ans, les studios se sont vite rendu compte des débouchés
commerciaux présentés par le fait de gérer toutes les étapes de la
production des divertissements. La seule différence est que, au premier
abord, ces étapes ne semblent pas les mêmes qu'autrefois.

Au lieu d'être propriétaires des laboratoires de développement, les
producteurs détiennent aujourd'hui l'infrastructure, sous la forme des
fournisseurs d'accès à internet (FAI). AT&T détient Warner Brothers et
HBO ; Comcast détient Universal et NBC[^1] ; et ainsi de suite.

Au lieu de signer des contrats restrictifs avec les employés impliqués
dans la création des contenus, ils... enfin, cela ils le font toujours.
Netflix [apparaît
souvent](https://www.businessinsider.com/biggest-deals-netflix-hbo-jj-abrams-shonda-rhimes-ryan-murphy-2019-11)
[à la une des
journaux](https://www.theverge.com/2020/11/12/21562109/netflix-david-fincher-multiyear-deal-mindhunter-house-of-cards-mank)
[pour avoir fait
signer](https://www.forbes.com/sites/tonifitzgerald/2018/07/20/inside-shonda-rhimes-150-million-netflix-deal-and-how-it-could-change-tv/)
[des contrats
exclusifs](https://variety.com/2018/digital/news/netflix-accounting-deals-shonda-rhimes-kenya-barris-ryan-murphy-1202908653/)
[à des artistes et des personnalités
célèbres](https://variety.com/2018/digital/news/barack-michelle-obama-netflix-deal-1202817723/).
Et certains studios rachètent d'autres studios et d'autres médias afin
de contrôler les droits de franchises populaires. Par exemple, Disney a
récemment racheté Star Wars et Marvel dans le but de garder la main sur
autant de « propriété intellectuelle » que possible, avec la possibilité
d'exploiter non seulement les droits des films mais aussi chaque flux de
revenu que les histoires de ces franchises sont capables de générer.
Comme on dit, personne ne fait à Disney ce que Disney a fait aux frères
Grimm.

Plutôt qu'être propriétaires des réseaux de salles de cinéma, les
studios ont tous lancé leur propre service de streaming. Comme avec les
salles d'autrefois, le caractère commode du service offert a rapidement
disparu. Les studios ont remarqué combien rapportait le streaming et ne
voulaient pas laisser les concurrents faire des bénéfices avec leurs
propres produits ; depuis peu, les studios reprennent la main sur leurs
propres œuvres et les réservent exclusivement à leur propre service de
streaming.

Au lieu de proposer un immense catalogue de productions très diverses
-- ce qui a rendu Netflix populaire au départ --, les services de
streaming ont remplacé la *commodité* qui les caractérisait à l'origine
par l'*exclusivité*. Bien sûr, comme jadis, le problème est que les
spectateurs ne s'intéressent pas à toutes les œuvres que propose un seul
studio ; ils en veulent certaines seulement. En revanche, les frais
d'abonnement à ces services ne couvrent pas seulement ce que veulent les
clients, ces derniers « achètent » tout. Comme les anciens réseaux de
salles de cinéma, nous achetons à l'aveugle la liste entière de tout ce
que propose Disney, HBO ou Amazon.

Bien entendu, ils ne peuvent pas risquer le fait qu'on paiera une seule
fois l'abonnement mensuel, pour regarder ce que l'on veut regarder puis
se désabonner. En conséquence, beaucoup de ces productions exclusives ne
sont plus mises en ligne selon un mode propice au *binge watching* ;
leur sortie est échelonnée afin de nous faire payer chaque mois pour
voir l'épisode suivant. Ainsi, le nouveau monde du streaming devient-il
un hybride du monde ancien de la télévision câblée et des salles de
cinéma.

Regarder un film ou une série en ligne de façon légale implique
aujourd'hui une recherche étendue sur de très nombreux services. On
espère souvent que le contenu que l'on veut se trouve sur l'un des
services auxquels on est déjà abonné plutôt que sur un autre. Parfois,
on recherche quelque chose qui *était* sur une plateforme à laquelle on
est abonné, mais se trouve désormais bloqué sur un autre service. Plutôt
que de concevoir des services qui fournissent la commodité recherchée
par le public -- avec la concurrence qui pousse les plateformes à créer
de meilleurs produits pour les spectateurs --, les plateformes trouvent
désormais de la valeur dans la *rareté* de leurs contenus : quelque
chose que l'on ne trouve que sur une seule plateforme. Même s'il n'est
pas intéressant, il est au moins lié à une franchise populaire ou
quelque tendance dont les spectateurs ne veulent pas rester à l'écart.

Plutôt que favoriser la conception de meilleurs services -- un accès à
internet plus rapide, de meilleures interfaces, de meilleurs
contenus --, les plateformes ont choisi un modèle fondé sur le contrôle
exclusif. La plupart des Américains n'ont pas le choix de leur
fournisseur d'accès internet[^2], et les FAI font tout pour protéger ce
monopole plutôt que de construire un service tellement attractif qu'on
le choisirait exprès. Plutôt que choisir le service de streaming avec le
meilleur prix, le meilleur catalogue ou la meilleure interface, il faut
s'abonner à tous les services. Nos anciens films et séries préférés sont
verrouillés et on ne peut plus accéder à tout dans un seul endroit. De
même, les nouveaux récits situés dans nos univers préférés sont attachés
de force à certains services, parfois même à certains appareils. Et les
artistes qu'on apprécie ? Eux aussi sont bloqués dans des contrats
exclusifs avec ces mêmes services.

L'histoire nous enseigne que ceci n'est pas ce que demandent les
consommateurs. L'exemple des années 1930 et 1940 montre que ce type
d'intégration verticale a des effets néfastes sur la créativité et sur
les publics. Nous savons, d'après l'histoire récente, que les
utilisateurs veulent des services commodes, légaux et bon marché, et
qu'ils s'en serviront s'ils leur sont proposés. Nous savons très bien
que ce système est intenable et étouffe la concurrence, qu'il peut
encourager la violation du droit d'auteur, qu'il propulse la croissance
des lois réactionnaires sur le droit d'auteur qui nuisent à l'innovation
et la création indépendante. Cependant, nous connaissons déjà un modèle
qui fonctionne bien.

Les autorités qui appliquaient les lois *antitrust* dans les années 1930
et 1940 ont reconnu qu'un tel système ne devrait pas exister et ils y
ont mis fin. Le démantèlement du cartel des studios dans les années 1940
a créé de la place pour davantage de salles indépendantes, davantage de
maisons de production indépendantes et, en général, plus de créativité
dans le cinéma. Pourquoi, alors, avons-nous permis à ce même système de
se rétablir en ligne ?

[^1]: Note du traducteur : AT&T et Comcast sont des grandes sociétés de
    télécommunications américaines, parmi les premiers fournisseurs
    d'accès à internet outre-Atlantique. NBC est l'une des principales
    chaînes de télévision aux États-Unis.

[^2]: Note du traducteur : il s'agit d'une différence importante entre
    les États-Unis et la France, où il existe une concurrence importante
    entre les quatre principales sociétés de télécommunications.

_Ce billet est publié sous la licence [Creative Commons Attribution 2.0 (CC BY 2.0 FR)](https://creativecommons.org/licenses/by/2.0/fr/)._
