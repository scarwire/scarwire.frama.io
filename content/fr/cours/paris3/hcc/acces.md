---
# Page title
title: Accès aux œuvres en cas d'urgence
subtitle: un petit guide du piratage pour étudiants de cinéma en période de pandémie

# Title for the menu link if you wish to use a shorter link title, otherwise remove this option.
linktitle: Accès aux ressources
draft: false

# Page summary for search engines.
#summary: Blah, blah, blah...

# Date page published
date: 2021-01-26

# Academic page type (do not modify).
type: book
#toc: true

# Position of this page in the menu. Remove this option to sort alphabetically.
weight: 13
---

{{% callout note %}}

Certaines informations dans cette page, publiée début 2021, ne sont plus à jour en 2023 : notamment, le site de liens PrimeWire ne fonctionne plus, et certains sites de vidéos mentionnés ci-dessous ont été fermés. Je mettrai à jour cette page quand j'aurai le temps, mais si les sites ont changé au fil du temps, les mêmes principes s'appliquent. Pour trouver des sites de liens aujourd'hui, un bon point de début est Unblockit, notamment les rubriques _Streaming Sites_ et _Direct Download_.

{{% /callout %}}

{{< figure src="../lone_gunmen.png" title="_The X-Files_, vers 1995. Pas besoin d'être un hacker de ce genre pour trouver des films en ligne." >}}

{{% toc hide_on="xl" %}}

## Introduction

La cinémathèque est fermée ? Votre pote a changé son mot de passe pour Netflix, qui de toute façon n'avait pas assez de films classiques ou art et essai ? Pas envie de donner votre argent durement gagné à une société multinationale qui [abuse de sa position dominante](https://www.bfmtv.com/tech/amazon-vise-par-une-enquete-pour-abus-de-position-dominante-en-autriche_AN-201902140022.html), [maltraite ses employés](https://www.lesnumeriques.com/vie-du-net/amazon-enquete-denonce-conditions-travail-brutales-n84117.html) et [esquive les impôts](https://www.nouvelobs.com/economie/20201210.OBS37313/comment-amazon-echappe-systematiquement-a-l-impot.html) ? La situation actuelle de pandémie rend particulièrement difficile l'accès aux films et aux textes que vos enseignants vous ont pourtant demandé d'étudier. Vous vous demandez peut-être si cette conjoncture exceptionnelle n'exige pas des mesures exceptionnelles. Ce petit guide a donc pour objectif de vous **initier au téléchargement de films et de livres**.

Tout d'abord, un avertissement, mais pas celui qu'on donne d'habitude. Bien sûr, en France, le téléchargement d'œuvres protégées par le droit d'auteur est illégal. Vous savez très bien que la loi et l'éthique ne sont pas toujours raccord, mais soyons clairs : le téléchargement pose des problèmes moraux, car la redistribution sans autorisation d'œuvres artistiques prive les auteurs de compensation. Cependant, il s'agit d'un problème éthique à géométrie variable, avec des différences sensibles entre le téléchargement d'un film d'art et essai d'une micro-société de production indépendante et le piratage d'une copie de _Star Wars_[^1]. 

Le tutoriel ci-dessous n'est évidemment pas cautionné par l'université Sorbonne Nouvelle et, si vous le suivez, vous le faites à vos propres risque et périls. Cela dit, j'essaierai de vous donner une idée des risques que vous courez avec les différentes techniques ; au passage, je vous donnerai quelques suggestions sur la protection de la vie privée en ligne.

[^1]: Dans le premier cas, on prive de vivres une petite maison de production, ce je trouve moralement discutable, même si un seul téléchargement n'est sans doute pas assez pour couler la boîte. Quant au second exemple, je vous laisse arriver à votre propre conclusion : quelle est la préjudice que vous causeriez à Disney en le privant d'une part microscopique de rente sur un film tourné il y a plus de 40 ans, qui a déjà remporté des centaines de millions de dollars ? Selon la loi, il n'y a pas vraiment de différence entre les deux téléchargements, sauf que Disney a des ressources financières et des équipes dédiées pour menacer et poursuivre en justice les internautes ayant mis à disposition ses films. La petite boîte de production indépendante ne les a sûrement pas.

{{% callout note %}}
_Ressources parfaitement légales_

Avant de vous résigner au téléchargement, n'oubliez pas qu'il existe des options parfaitement légales pour regarder des films et lire des livres, même en période de pandémie. À ce jour (début février 2021), la [bibliothèque universitaire de Paris 3](https://www.dbu.univ-paris3.fr/), qui possède une vaste collection de DVDs et de livres sur le cinéma, a des horaires réduits mais **reste ouverte au public**. De même pour la [bibliothèque du cinéma François Truffaut](https://www.paris.fr/equipements/bibliotheque-du-cinema-francois-truffaut-3371). Vous pouvez [réserver un créneau horaire](http://bit.ly/affluence_DBU) pour emprunter des films ou des livres ou pour les consulter sur place. Le site web [archive.org](https://www.archive.org/) met en ligne de nombreux ouvrages en libre accès ou empruntables avec un compte gratuit. Finalement, la plateforme [OpenEdition Books](https://books.openedition.org/catalogue?q=cin%C3%A9ma#results) publie en accès libre un certain nombre d'ouvrages universitaires sur le cinéma.
{{% /callout %}}

## Prérequis

Avant de chercher des films en ligne, il vous faudra un petit minimum d'équipements, et il faudra surtout que ces équipements soient bien configurés.

D'abord, il vous faudra un **ordinateur**. Cela peut avoir l'air évident, mais c'est seulement sur un ordinateur que vous pourrez effectuer toutes les opérations suivantes, **pas sur un téléphone**. Je suppose aussi que vous êtes suffisamment à l'aise avec votre ordinateur pour pouvoir naviguer sur internet et installer des logiciels, mais les techniques résumés dans ce guide n'exigent aucune autre connaissance technique spécifique. Peu importe si votre machine tourne sous Windows, macOS ou Linux.

### Navigateur

Ce tutoriel suppose que vous utilisez une version récente du navigateur [Firefox](https://www.mozilla.org/firefox/), qui vient avec plusieurs fonctions anti-pistage, vous permet d'installer une large variété d'extensions et, à la différence de la plupart des autres navigateurs, est édité par une association à but non lucratif. Si toutefois vous préférez un autre navigateur, les versions récentes de Google Chrome, Chromium, Opera ou Microsoft Edge vous permettront d'installer les extensions ci-dessous. Malheureusement, celles-ci ne sont pas prises en charge par Safari ou Internet Explorer.

- [{{< icon name="external-link-alt" pack="fas" >}}Mozilla Firefox](https://www.mozilla.org/firefox/)

### Extensions anti-pub

{{< figure src="../theylive2.jpeg" title="_Invasion Los Angeles_ (_They Live_, John Carpenter, 1988)" >}}

Les sites de téléchargement de films ont la tendance fâcheuse d'afficher beaucoup de publicités ; encore pire, si vous tombez sur un faux site de téléchargement, les liens pourraient vous afficher de véritables torrents de publicité indésirable. Vous aurez donc besoin d'un bon bloqueur de pub. Installées ensemble, les deux extensions suivantes bloquent la plupart des publicités et, encore mieux, empêchent les annonceurs de pister votre activité en ligne :

- [{{< icon name="external-link-alt" pack="fas" >}}uBlock Origin](https://ublockorigin.com/)
- [{{< icon name="external-link-alt" pack="fas" >}}PrivacyBadger](https://privacybadger.org/)

### Lecteur de vidéos

Il existe de très nombreux lecteurs de fichiers vidéo, et les fichiers que vous téléchargerez fonctionneront avec la plupart. Pour une expérience sans failles, je conseille le [lecteur VLC](https://www.videolan.org/), un logiciel libre qui non seulement lit la quasi-totalité de fichiers vidéo mais [facilite la recherche et le chargement des sous-titres](#VLSub).

- [{{< icon name="external-link-alt" pack="fas" >}}VLC media player](https://www.videolan.org/)

### Paramétrage des serveurs DNS

<div style='max-width: 500px; margin: 0 auto'>

{{< figure src="../switchboard.jpeg" title="_Telephone Operator_ (Scott Pembroke, 1937)" >}}

</div>

L'accès à de nombreux sites de téléchargement est bloqué par défaut par certains fournisseurs d'accès internet. Heureusement, ce blocage -- effectivement une [forme de censure](https://www.numerama.com/tech/303113-comment-surveille-t-on-et-censure-t-on-internet.html) -- est très simple à contourner, car il intervient au niveau des serveurs DNS. Lorsque vous visitez une adresse sur internet, le nom du site (par exemple [eff.org](https://eff.org/)) est traduit en un numéro d'adresse IP (dans ce cas [173.239.79.196](http://173.239.79.196), essayez-le !) que votre ordinateur utilise pour communiquer avec le site. Si vous n'avez jamais modifié vos paramètres DNS, vous utilisez probablement les serveurs DNS de votre fournisseur d'accès qui, lorsque vous tentez de visiter un site comme [Library Genesis](http://libgen.lc/) ou [PrimeWire](https://www.primewire.li/), risquent de vous rediriger vers un autre site ou vers un message d'erreur.

Pour changer vos serveurs DNS, il suffit de suivre [ce tutoriel](https://www.justgeek.fr/changer-serveurs-dns-windows-mac-ou-linux-40817/). Comme les auteurs de ce guide, j'utilise les serveurs DNS de Cloudflare (1.1.1.1 et 1.0.0.1) qui sont très rapides, mais il existe de nombreux autres, y compris [des serveurs avec des garanties assez solides](https://www.opennic.org/) contre la censure et pour la protection de la vie privée.

- [{{< icon name="external-link-alt" pack="fas" >}}Tutoriel : changer de serveurs DNS](https://www.justgeek.fr/changer-serveurs-dns-windows-mac-ou-linux-40817/)

## Téléchargement direct

L'option la plus facile et la moins risquée pour accéder à des films en ligne est sans doute le téléchargement direct.

Il existe un écosystème foisonnant de sites mettant à disposition des films téléchargeables. Notons la différence entre trois types de sites :

1) des **sites de vidéos**, où les films eux-mêmes sont hébergés (exemple : Mixdrop) ;
2) des sites, financés par la publicité, qui fournissent des **liens** vers les sites de vidéo (exemple : Primewire) ;
3) des **forums** où des utilisateurs individuels publient et échangent des liens vers les sites de vidéo (exemple : Kebekmac).

Le premier type de site stocke des contenus vidéo de tous genres : des vidéos mis à disposition avec l'accord de leurs créateurs, aussi bien que des films et des séries piratés. Par contre, ces sites ne gardent pas un index de leurs contenus : il n'y a pas de fonction de recherche comme sur YouTube ou Dailymotion. Il faut donc trouver des liens renvoyant à ces sites sur d'autres sites --  ou se faire envoyer des liens par ses amis.

{{% callout note %}}
<div id="risques-relatifs"></div>

_Risques relatifs du téléchargement direct et du p2p_

Vous courez beaucoup moins de risques en utilisant des sites de téléchargement direct qu'en ayant recours aux réseaux _peer-to-peer_ (p2p) tels que BitTorrent. En effet, la HADOPI -- le [dispositif zombie](https://www.numerama.com/politique/167219-hadopi-ce-sera-fini-le-4-fevrier-2022.html) de [surveillance de masse](https://www.laquadrature.net/2020/02/24/tremble-hadopi/) qui sera supprimé en 2022 mais qui reste [très coûteux pour le contribuable](https://www.developpez.com/actu/144403/Hadopi-d-ici-sa-fin-en-2022-l-autorite-aura-coute-plus-de-100-millions-d-euros-au-contribuable-du-gaspillage-de-ressources/) -- **ne surveille pas les sites de téléchargement direct, [seulement les connexions p2p](https://www.service-public.fr/particuliers/vosdroits/F32108)**. Quant aux ayants droit, ils sont nombreux à essayer de faire supprimer leurs œuvres des sites de vidéos en téléchargement direct, mais il est très rare qu'ils tentent de poursuivre les utilisateurs ordinaires qui téléchargent des fichiers depuis ces sites. C'est pour ces raisons que ce guide se concentre sur le téléchargement direct plutôt que le p2p.

Dans tous les cas, si vous souhaitez naviguer de façon vraiment anonyme, vous pouvez souscrire un service de [VPN](https://www.lesnumeriques.com/appli-logiciel/pourquoi-utiliser-un-vpn-virtual-private-network-et-comment-le-choisir-a139241.html). Ces services payants ajoutent une couche de protection et d'anonymat en faisant passer par leurs serveurs, en version chiffrée, tout le trafic entre votre ordinateur et internet.
{{% /callout %}}


Ces sites, souvent poursuivis et parfois fermés pour violation du droit d'auteur, jouent constamment au chat et la souris avec les lobbys représentant les ayants-droit et les autorités de divers pays. Dès qu'un site est fermé ou bloqué, un ou plusieurs nouveaux prennent sa place. Les informations dans ce guide, surtout les noms et adresses des différents sites, reflètent donc la situation actuelle en février 2021, mais cette situation est susceptible d'évoluer assez rapidement.

### Sites de liens

Un bon exemple de site de pour commencer à chercher des films est [PrimeWire](https://www.primewire.li/), dont l'utilisation est plutôt intuitive.

Une fois que vous êtes sur Primewire, commencez par taper le titre d'un film dans le champ de recherche. **N'hésitez pas à essayer le titre anglophone**, y compris pour des films français, et n'oubliez pas que certains films qui sont sortis en France avec un titre en anglais ont un autre titre dans les pays anglophones. Par exemple, _Very Bad Trip_ (Todd Phillips, 2009) s'appelle _The Hangover_ en version originale.

{{% callout note %}}
<!-- <img src="../wplang.png" width="200px" style='float: right;; margin-top: 0;'> -->
<div style="float: right; margin-top: -2rem;">
{{< figure src="../wplang.png" title="" width="200px" style="margin-top: 0;" >}}
</div>

<div style="min-height: 230px;">

_Titres anglophones_

Vous pouvez très facilement repérer le titre anglophone d'un film sur Wikipédia : une fois que vous avez ouvert l'article francophone correspondant au film, cliquez sur le lien pour l'article équivalent en anglais dans la colonne de gauche.
</div>
{{% /callout %}}

{{% callout warning %}}
_Attention à l'adresse !_

Comme les sites de liens existent presque uniquement pour faciliter le téléchargement d'œuvres protégées par le droit d'auteur, il existe de nombreuses copies, souvent avec de faux liens. Quand vous visitez ces sites, tapez **l'adresse entière**: par exemple `primewire.li` plutôt que simplement `primewire`. La deuxième option vous amènera vers une page de recherche avec tous les sites nommés "primewire" : `primewire.ag`, `primewire.li`, `primewire.gr`, mais aussi `primewire.show`, qui a l'air d'être un site bidon plein de publicités.
{{% /callout %}}


Après avoir repéré le film que vous cherchez, le site vous affichera un ou plusieurs liens vers des sites de vidéos qui hébergent des copies du film. Il faut essayer ces liens un par un. Il se peut que certains ne fonctionnent pas : ils vous amèneront vers des sites qui sont hors service, ou vers des fichiers qui ont été enlevés des sites de vidéo suite à une réclamation de la part des ayants droit.

{{< video src="download_pw_mxd.mp4" controls="yes" >}}

Certains sites de vidéos sont plus faciles d'utilisation que d'autres. La plupart permettent le visionnage immédiat en streaming, ce qui est génial tant que vous n'avez pas besoin de sous-titres. En revanche, s'il vous faut des sous-titres, il vaut mieux télécharger le film sur votre ordinateur d'abord, puis suivre la démarche indiqué ci-dessous pour [ajouter des sous-titres au film](#téléchargement-de-sous-titres). Pour certains sites qui permettent le visionnage en streaming sans proposer une option pour télécharger le film, il existe une astuce pour enregistrer le fichier sur votre disque dur tout de même :

{{< video src="devtools.mp4" controls="yes" >}}

Actuellement, les trois sites de vidéos les plus faciles à utiliser, qui permettent et le visionnage en streaming et le téléchargement, sont **mixdrop**, **dood.watch** et **streamtape**. Favorisez ces liens, surtout si vous devez télécharger le film pour ensuite ajouter des sous-titres.

**Primewire** est, à mon avis, le site de liens le plus souple et intuitif à l'heure actuelle :

- [{{< icon name="external-link-alt" pack="fas" >}}PrimeWire](https://www.primewire.li/)

Le site **Unblockit** héberge des copies de très nombreux sites de liens streaming, certains fiables, d'autres moins :

- [{{< icon name="external-link-alt" pack="fas" >}}Unblockit](https://unblockit.ltd/)

### Les forums

Il existe aussi des forums en ligne permettant aux utilisateurs de partager des liens vers des films. Probablement le meilleur forum francophone à l'heure actuelle s'appelle **kebekmac**. Vous pouvez trouver des liens pour des films sur ce forum mais aussi sur le blog créé par  :

- [{{< icon name="external-link-alt" pack="fas" >}}Kebekmac (forum)](https://kebekmac.forum-canada.com/)
- [{{< icon name="external-link-alt" pack="fas" >}}Kebekmac (blog)](https://kebekmac.blogspot.com/)

Pour chercher des films sur le forum, il faut d'abord créer un compte gratuit, puis utiliser la fonction de recherche interne du forum. N'hésitez pas à essayer des termes différents. Si vous voulez obtenir une copie de _Casque d'or_ (Jacques Becker, 1952), par exemple, essayez d'abord avec le titre, mais une copie pourrait également apparaître sur une page consacrée à son réalisateur ou à l'un de ses comédiens principaux (Simone Signoret, Serge Reggiani).

Pour chercher sur le blog, vous pouvez faire une recherche Google ou [DuckDuckGo](https://duckduckgo.com/) en limitant les résultats au blog. Par exemple, pour trouver des films de Sacha Guitry, on peut essayer la recherche suivante :

[`site:kebekmac.blogspot.com Sacha Guitry`](https://duckduckgo.com/?q=site%3Akebekmac.blogspot.com+sacha+guitry&t=h_&ia=web)

La plupart des liens sur kebekmac renvoient ou bien vers le serveur **Uptobox**, ou bien vers le service de dissimulation des liens **Multiup**, qui à son tour fournit des liens vers Uptobox et quelques autres sites où les vidéos sont hébergées.

## Le _peer-to-peer_

Comme noté ci-dessus, les réseaux _peer-to-peer_ posent [un peu plus de risque](#risques-relatifs) par rapport aux sites de téléchargement direct. Si tout de même vous êtes intéressé·e·s par ce moyen de partage de films -- dès lors qu'on reçoit un fichier, on le met forcément à disposition pour d'autres utilisateurs -- voici quelques liens pour commencer :

- [{{< icon name="external-link-alt" pack="fas" >}}Comment télécharger un film en Torrent](https://syskb.com/comment-telecharger-un-film-en-torrent/)
- [{{< icon name="external-link-alt" pack="fas" >}}Transmission (client de BitTorrent libre et multiplateforme)](https://transmissionbt.com/)

## Téléchargement de sous-titres

Une fois que vous disposez d'une copie du film que vous voulez regarder, il faudra éventuellement ajouter des sous-titres. Il existe plusieurs sites stockant des fichiers de sous-titres pour un vaste nombre de films en plusieurs langues. Le plus grand et plus connu est [OpenSubtitles.org](https://www.opensubtitles.org/).

Les sous-titres qu'on télécharge sur ce type de site peuvent être bien ou mal traduits, mais le problème le plus répandu est qu'ils ne sont pas bien synchronisés avec la copie du film qu'on vient de télécharger.

Plutôt que faire une recherche sur OpenSubtitles, nous pouvons automatiser et accélérer le processus de téléchargement de sous-titres jusqu'à ce qu'on trouve une traduction bien synchronisée. On le fera grâce à l'extension VLSub qui est inclus avec le lecteur de médias [VLC](#lecteur-de-vidéos).

{{< video src="vlsub.mp4" controls="yes" >}}

## Téléchargement de livres et d'articles scientifiques

Finalement, le téléchargement ne se limite pas aux films et séries. Si vous n'avez pas accès à un livre à la bibliothèque, vous pouvez essayer de télécharger une copie sur le réseau **Library Genesis**, qui propose de plus en plus de titres en français, même si la collection en anglais est beaucoup plus large. Bien entendu, le téléchargement de livres pose des problèmes éthiques similaires au téléchargement de films, et les éditeurs et les libraires indépendants ont besoin de votre soutien. Cela dit, pour chercher un renseignement précis ou une petite citation, ou pour avoir un aperçu d'un livre, Library Genesis est un outil précieux :

- [{{< icon name="external-link-alt" pack="fas" >}}Library Genesis](https://libgen.is/)

Un projet similaire, mais pour des articles scientifiques :

- [{{< icon name="external-link-alt" pack="fas" >}}Sci-hub](https://sci-hub.st/)
