---
# Page title
title: Histoire du cinéma classique

# Title for the menu link if you wish to use a shorter link title, otherwise remove this option.
# linktitle: Cinéma classique

# Page summary for search engines.
#summary: Blah, blah, blah...

# Date page published
date: 2023-01-30
draft: true
# Academic page type (do not modify).
type: book
toc: true

# Position of this page in the menu. Remove this option to sort alphabetically
weight: 11
---

Université Sorbonne Nouvelle  
Groupe TD 6, mercredi 8h00-10h00, salle C203  
Enseignant : Daniel Morgan  
E-mail : [daniel.morgan@sorbonne-nouvelle.fr](mailto:daniel.morgan@sorbonne-nouvelle.fr)

{{< figure src="denham.jpeg" title="_King Kong_ (M. Cooper/E. Shoedsack, 1933)" >}}

Bienvenue dans mon groupe du cours d'**Histoire du cinéma classique** en Licence 1 cinéma et audiovisuel à l'université Sorbonne Nouvelle ! Ce cours abordera la période dite "classique", de l'introduction du cinéma parlant à la fin des années 1920 jusqu'au début des Nouvelles Vagues aux environs de 1960. Nous aborderons le cinéma de sept pays qui ont été des grand producteurs et consommateurs de films à cette époque: la France, l'Italie, l'Allemagne, le Royaume-Uni, les États-Unis, le Japon et l'Union Soviétique.

{{% toc hide_on="xl" %}}

## Films au programme

### Films obligatoires

Les six films suivants sont **obligatoires**. Vous devez impérativement les voir pour les dates indiquées :

- _Chercheuses d'or de 1933_ (_Gold Diggers of 1933_, Mervyn LeRoy, 1933)  
:arrow_right: à voir pour le 15 février  
{{< icon name="film" pack="fas" >}} **Séances à la [Cinémathèque universitaire](http://www.univ-paris3.fr/cinematheque-universitaire--27069.kjsp)** (salle de projection au rez de jardin, bâtiment B) : lundi 13 février à 16h et le mercredi 15 février à midi.  
{{< icon name="film" pack="fas" >}} [téléchargement](https://filesender.renater.fr/?s=download&token=ef69da7b-5140-4d0b-8fd8-d71d5f0860c0) possible jusqu'au 1er mars<!--, [lien alternatif](https://uptobox.com/9lq6zc1cdts2/1933%20King%20Kong%20MULTi.1080p.HDLight.x264.AC3.mkv)--><!-- {{< icon name="language" pack="fas" >}} <a href="Gold%20Diggers%20Of%201933%201933%201080p%20WEBRip%20x264-%5bYTS%20AM%5d.srt" download>sous-titres</a> -->
- _Le jour se lève_ (Marcel Carné, 1939)  
:arrow_right: à voir pour le 22 février  
{{< icon name="film" pack="fas" >}} **Séances à la Cinémathèque universitaire** : lundi 20/02 à 16h et mercredi 22/02 à 12h  
{{< icon name="film" pack="fas" >}} [téléchargement](https://filesender.renater.fr/?s=download&token=ba7667b3-0b15-4b94-b18f-75200d34ffac) possible jusqu'au 18 mars
- _M le maudit_ (_M_, Fritz Lang, 1931)  
:arrow_right: à voir pour le 8 mars  
{{< icon name="film" pack="fas" >}} **Séances à la Cinémathèque universitaire** : lundi 06/03 à 16h et mercredi 08/03 à 12h  
{{< icon name="film" pack="fas" >}} [téléchargement](https://filesender.renater.fr/?s=download&token=1d50a322-48bd-4a54-896c-091b46e57e97) possible jusqu'au 31 mars  
{{< icon name="film" pack="fas" >}} [lien alternatif](https://uptobox.com/rggecwn8xsov/meinestadt31.rar)  
{{< icon name="language" pack="fas" >}} <a href="M.1931.srt" download>sous-titres</a>
- _Le Retour de Jean_ (Henri-Georges Clouzot, 1949)  
:arrow_right: à voir pour le 22 mars <!-- {{< icon name="film" pack="fas" >}} [téléchargement](https://filesender.renater.fr/?s=download&token=e8035455-6922-475b-9a40-d54ac5f08b13), [lien alternatif](https://uptobox.com/62u1mr7qtke8) -->
- _Riz amer_ (Giuseppe de Santis, 1949)  
:arrow_right: à voir pour le 12 avril  
{{< icon name="film" pack="fas" >}} [téléchargement](https://filesender.renater.fr/?s=download&token=60a3b340-0a35-4503-ad6f-3f1df299f643), [lien alternatif](https://uptobox.com/jc4spp3h5z82)
- _Quand passent les cigognes_ (_Letiat jouravli_, Mikhaïl Kalatozov, 1957)  
:arrow_right: à voir pour le 26 avril.
{{< icon name="film" pack="fas" >}} [téléchargement](https://filesender.renater.fr/?s=download&token=4439d45f-d4c9-4ecf-b453-591d997d72e1), [lien alternatif](https://uptobox.com/6uin4c3c0pxu/Kalatozov-1957-Quand%20passent%20les%20cigognes_multi_720.mp4)

### Pour aller plus loin

- La **Cinémathèque universitaire** propose une [programmation spécifique](#) pour ce cours. Assistez à autant de séances que vous pouvez, vous ne serez pas déçus !
- [Une liste complémentaire]({{< relref "filmo_extra.md" >}}) de films classiques à voir juste pour le kif
- La [bibliothèque universitaire](https://www.dbu.univ-paris3.fr/) dispose d'une excellente collection de DVDs, du matériel pour les visionner sur place, ainsi que de très nombreux livres et revues sur le cinéma
- Le site archive.org met à disposition [une collection de longs métrages gratuits](https://archive.org/details/feature_films), y compris certains films classiques. Ce site propose également des livres sur le cinéma [complètement gratuits](https://openlibrary.org/subjects/film) et d'autres [empruntables](https://archive.org/details/inlibrary?and[]=cinema&sin=&and[]=subject%3A%22Films%2C+cinema%22&and[]=subject%3A%22Film%22&and[]=subject%3A%22Motion+pictures%22) si vous avez créé un compte.
- Des [conseils sur l'accès aux films]({{< relref "acces.md" >}}), dont quelques astuces sur le téléchargement. Plus parfaitement à jour (cela date de 2021), et à utiliser avec précaution.

## Bibliographie indicative

- AUGROS Joël et KITSOPANIDOU  Kira, [_Une  Histoire du cinéma  américain.  Stratégies, révolutions et mutations au XXe siècle_](https://ipfs.io/ipfs/bafykbzaceasewqdakcevtqhzwyefdwhsavgtuw3ykmsqlgmilwijwyqrxbqhk?filename=Jo%C3%ABl%20Augros_%20Kira%20Kistopanidou%20-%20L%27%C3%A9conomie%20du%20cin%C3%A9ma%20am%C3%A9ricain_%20Histoire%20d%27une%20industrie%20culturelle%20et%20de%20ses%20strat%C3%A9gies-Armand%20Colin%20%282009%29.epub), Paris, Armand Colin, 2016
- BILLARD Pierre, _L’Âge  classique  du  cinéma  français.  Du  cinéma  parlant à la  Nouvelle Vague_, Paris, Flammarion, 1995
- BOURGET Jean-Loup, [_Hollywood.  La  norme  et  la  marge_](https://ipfs.io/ipfs/bafykbzaceahgo3l45exnitvb7qfigxv5ewcrqd3rdkuht2b6pbnbguhcedwnq?filename=%28Armand%20Colin%20cine%CC%81ma%29%20Bourget%2C%20Jean-Loup%20-%20Hollywood%2C%20la%20norme%20et%20la%20marge-Armand%20Colin%20%282005%29.epub) \[1998\],  Paris, Armand  Colin, 2e édition, 2016
- EISENSCHITZ Bernard, _Le Cinéma  allemand_ \[1999\], Paris,  Armand  Colin, 2e édition, 2008
- JEANCOLAS Jean-Pierre, _Le Cinéma français_ \[1993\], Paris, Armand Colin, 4e édition augmentée, 2019. (L'édition de 1999 est accessible [ici](https://ipfs.io/ipfs/bafykbzacea3kewi3detqw2cf6jn2prmgxk3q7ic7arwwdvxul4vmuh2ug34vi?filename=Jean-Pierre%20Jeancolas%20-%20Histoire%20du%20cinema%20francais%20%281999%29.djvu).)
- NACACHE Jacqueline, _Le Film hollywoodien classique_, Paris, Armand Colin, 2005
- PILLARD Thomas, _Le Film noir français face aux bouleversements de la France d’après-guerre (1946-1960)_, Nantes, Éditions Joseph K., 2014
- SCHIFANO Laurence, [_Le Cinéma italien de 1945 à nos jours. Crise et création_](https://ipfs.io/ipfs/bafykbzacedkcovulidgymbtxmr4c4goccqfuyksu3en34eadft2zxe43zl3zw?filename=Schifano%2C%20Laurence%20-%20Le%20cin%C3%A9ma%20italien%20de%201945%20%C3%A0%20nos%20jours%20-%204e%20%C3%A9d-Armand%20Colin%20%282016%29.epub) \[1995], Paris, Armand Colin, 4e édition revue et augmentée, 2016
- SUMPF Alexandre, _Révolutions  russes  au  cinéma. Naissance d’une  nation: URSS, 1917-1985_, Paris, Armand Colin, 2015
- TESSIER Max et MONVOISIN Frédéric, _Le Cinéma japonais. Une introduction_ \[1997\], Paris, Armand Colin, 3e édition actualisée et augmentée, 2018
- (Si vous lisez bien l'anglais) THOMPSON Kristin et BORDWELL David, [_Film History: An Introduction_](https://ipfs.io/ipfs/bafykbzaced4srbyef5lw25ketn2ytlgfqnbbuyckezu26yoqbe75scf66mtxi?filename=David%20Bordwell_%20Kristin%20Thompson%20-%20Film%20history%20_%20an%20introduction-University%20of%20Wisconsin%20%282019%29.pdf) \[1993\], New York, McGraw-Hill, 4e édition, 2019

## Validation

Vous serez évalué·es à partir de deux épreuves :

- Un devoir sur table le **22 mars 2022** Vous aurez **1h15** pour répondre à des questions concernant les contenus des cinq premiers cours et notamment sur l'un des films obligatoires.
- Un [devoir à la maison]({{< relref devoir.md >}}) à rendre **lundi 10 mai**. Vous écrirez une dissertation portant sur une question transversale du cinéma classique. Vous devrez répondre en faisant référence aux films classiques que vous aurez vu pendant le semestre. (Le sujet sera bientôt précisé.)

Votre présence en cours ne sera pas contrôlée mais est **très fortement conseillée**.

## Calendrier

### 1er février 2023

Introduction

- Qu'est-ce que l'histoire du cinéma ?
- La transition vers le parlant

#### Ressources

- Extraits :
  - [_Le Chanteur de jazz_, 1 - dialogues/chanson](https://www.youtube.com/watch?v=8SzltpkGz0M)
  - [_Le Chanteur de jazz_, 2 - modernité/tradition](https://mediaserver.lecnam.net/permalink/v1261898383cb0af6ac8/iframe/)
- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos1.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Plus d'informations sur le passage au parlant](1/poly_parlant.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Entretien avec l'historien Kevin Brownlow sur la généralisation du parlant](1/brownlow.pdf)

### 8 février 2023

Le cinéma américain des années 1930 (1)

- Censure (le code de production)
- Stars
- Genres

#### Ressources

- Films :
  - {{< icon name="film" pack="fas" >}} [_L'Ennemi public_](https://filesender.renater.fr/?s=download&token=172dfdbe-3b42-48e7-8897-153a7944242f) (_The Public Enemy_, William Wellman, 1931)
  - {{< icon name="film" pack="fas" >}} [_Les Anges aux figures sales_](https://filesender.renater.fr/?s=download&token=3d580c06-fd30-4a1a-9c07-1f79305e8615) (_Angels with Dirty Faces_, Michael Curtiz, 1938)
- Extraits :
  - [_Les Anges aux figures sales_ : exécution/rédemption](https://mediaserver.lecnam.net/permalink/v12618e81df8f1itxuwb/)<!-- - [_I’ll Be Glad When You’re Dead You Rascal You_](https://www.youtube.com/watch?v=gPpOJvm6998) (Dave Fleischer, 1932)-->
- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos2.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le code de production](V2MA01_Poly3.pdf)
- Lectures pour la prochaine séance :
  - {{< icon name="file-pdf" pack="fas" >}} [Edgar Morin, _Les Stars_, 1957 (extraits)](morin.pdf)
  - {{< icon name="external-link-alt" pack="fas" >}} Un [résumé du code de production](https://fr.wikipedia.org/wiki/Code_Hays) plutôt bien écrit sur Wikipédia
  - {{< icon name="external-link-alt" pack="fas" >}} Quelques informations complémentaires sur l'[époque pré-code](http://cinemaclassic.free.fr/code/code.htm)

### 15 février 2023

Le cinéma américain des années 1930 (2)

- Le système des studios
- De nouveaux genres
- Analyse de _Chercheuses d'or de 1933_

#### Ressources

- Extraits :
  - [_Chercheuses d'or de 1933_ : "We're in the money"](https://mediaserver.lecnam.net/permalink/v12618e75d52aja0tk5v/iframe/)
  - [_Chercheuses d'or de 1933_ : "In the shadows"](https://mediaserver.lecnam.net/permalink/v12618e75eb78t2j24no/iframe/)
  - [_Chercheuses d'or de 1933_ : "Forgotten man"](https://mediaserver.lecnam.net/permalink/v12618e75ee34w5gzeaf/iframe/)
- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos3.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : les studios et le cinéma hollywoodien des années 1930](V2MA01_Poly2.pdf)

### 22 février 2023

Le cinéma français des années 1930

- Studios, stars et spectateurs en France
- Le cinéma et le Front populaire
- Le réalisme poétique
- Analyse de _Le jour se lève_

#### Ressources

- Films entiers : voir la [filmographie complémentaire]({{< relref "filmo_extra.md" >}}) pour des liens vers _Taris_, _Zéro de conduite_, _Le Crime de Monsieur Lange_ et _Le Quai des brumes_
- Extraits :
  - [_Zéro de conduite_, la révolte](https://mediaserver.lecnam.net/permalink/v1261943ce17febrtohm/iframe/)
  - [_Le Crime de Monsieur Lange_, conclusion](https://mediaserver.lecnam.net/permalink/v1261943cdd48ad08qhj/iframe/)
  - [_Le Quai de brumes_, Gabin/Morgan](https://mediaserver.lecnam.net/permalink/v1261943cf3d3yw22orq/iframe/)
- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos4.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le cinéma français des années 1930](V2MA01_Poly4.pdf)

{{% callout note %}}
**Quelques pistes de réflexion sur _Le jour se lève_**

1) Comment se construit le temps dans ce film ?
2) Comment se construit l’espace du film ?
3) Quels sont les liens entre (1) et (2) ? De quelle manière la présentation du temps et de l'espace est-elle liée au récit ?
4) Quel portrait le film dresse-t-il de différents milieux sociaux ? Du crime ? De l’exclusion ? Le film comporte-t-il un message politique ?
5) Quelle image le film donne-t-il des relations entre les sexes ? Comment décrire les rapports entre les quatre personnages principaux ? Quelles sont les préconceptions du protagoniste sur les deux personnages féminins, et comment ces deux femmes sont-elles différentes par rapport à ces idées reçues ? De quelle manière ces idées reçues façonnent le rapport entre François et Valentin, ainsi que la chute de ces deux hommes ?
6) _Le jour se lève_ fait partie du réalisme poétique. De quelle façon ? Quels sont ses points communs avec d'autres films de cette tendance ? Qu'est-ce qu'il y a de réaliste dans le film ? De poétique ?

{{% /callout %}}

**_Vacances la semaine du 27 février._**

### 8 mars 2023

**Cours annulé.** 

### 15 mars 2023

**Cours annulé.** En raison des actions de grève et de blocage, le premier partiel est repoussé de deux semaines, jusqu'au 29 mars.

En attendant la reprise des cours, n'hésitez pas à regarder cette [présentation du cinéma français des années 1940 et 1950](https://drive.google.com/file/d/1xJfHX3YWtD8-MeMk4tp-GhuESzIxcCa3/view?usp=sharing) (connectez-vous à l'aide de vos identifiants Sorbonne Nouvelle). Ce contenu ne figurera pas sur le premier partiel, mais vous pourrez vous en servir pour le partiel final en fin de semestre.

### 22 mars 2023

**Cours annulé.**

### 29 mars 2023

**Premier partiel.** Temps de composition : 1h15. Voir ["validation"](#validation) ci-dessus.

Le partiel sera suivi d'un petit retour sur le module portant sur le cinéma français des années 1940 et 1950, ainsi que la projection d'un court métrage.

### 5 avril 2023

{{% callout warning %}}

De façon exceptionnelle, **ce cours se tiendra à distance** sur la plateforme Google Meet. Cliquez sur le lien suivant pour vous connecter : https://meet.google.com/xrk-gjva-ejq

{{% /callout %}}

Le cinéma allemand de 1929 à 1945

- La transition vers le parlant
- Le cinéma de la République de Weimar
- Le cinéma du IIIe Reich
- Analyse de _M le maudit_

#### Ressources

- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos5.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le cinéma allemand, 1930-1944](V2MA01_Poly5.pdf)
- Extraits :
  - [_L'Ange bleu_ : le professeur arrive au cabaret](https://drive.google.com/file/d/1cuNUi8d3LXwTjdMloOGUndtM5RWayqjh/view?usp=share_link)
  - [_Les Chemins du paradis_ : version allemande](https://drive.google.com/file/d/1XMBh2rgf0I84s2iyuRVZLUxUswQuK86y/view?usp=share_link)
  - [_Les Chemins du paradis_ : version française](https://drive.google.com/file/d/1YQQo1PXPbs0fuM5ZwDB6eJg6ebxNVgCs/view?usp=share_link)
  - [_Le Testament du Docteur Mabuse_ : Lohmann interroge le directeur de l'hôpital psychiatrique](https://drive.google.com/file/d/1ss_sSGQoxLwfDf-r3Yi_x7iChwSPwMOy/view?usp=share_link) (maintenant avec sous-titres !)
  - [_M le maudit_ : une atmosphère de délation](https://drive.google.com/file/d/1lYs83HHmaVAoOu7I5P0s27aGY50qYE4J/view?usp=sharing)
  - [_M le maudit_ : police et pègre en montage parallèle](https://drive.google.com/file/d/1POLv9LX9RDPxh_B082zFL_xNdQ2y3Tpe/view?usp=sharing)
  - [_M le maudit_ : utilisation du son hors-champ](https://drive.google.com/file/d/1Mc0Z9Pqsp-iTUYacNREKxS9-GlfIkLTs/view?usp=sharing)
  - [_M le maudit_ : concurrence entre mafieux et police](https://drive.google.com/file/d/1Uxf20nC762P8a_i_MCC__i2wavq4q43N/view?usp=share_link)
  - [_M le maudit_ : une conlclusion ambigüe](https://drive.google.com/file/d/1RV4Q-Zhq8D-JgHtqZmoW7lmG6f_FuU-S/view?usp=share_link)

<!-- #### Ressources

- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos6.pdf) 
- {{< icon name="file-pdf" pack="fas" >}} Lecture pour accompagner le film : [André Bazin, "La cybernétique d'André Cayatte"](4/bazin_cybernetique.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma français sous l'occupation + de 1945 à 1960](V2MA01_Poly6.pdf) -->
<!-- - Extraits :
  - [_Avant le déluge_ : panique autour de la guerre froide](https://mediaserver.lecnam.net/permalink/v1261954f5f59hx0xpx1/iframe/)
  - [_Avant le déluge_ : le meurtre de Daniel](https://mediaserver.lecnam.net/permalink/v1261954f5ef2ixhg513/iframe/)
  - [_Avant le déluge_ : le verdict, les adolescents et les parents](https://mediaserver.lecnam.net/permalink/v1261954f5f80qgg2cz3/iframe/) -->

<!-- ### 5 avril 2023

Le cinéma américain des années 1940 et 1950

- Développements techniques : couleurs et écran large
- Genres et sous-genres
- La fin d'une époque à Hollywood

#### Ressources
-->
<!-- - {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos7.pdf) -->
<!-- - {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma américain des années 1940](V2MA01_Poly7a.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma américain des années 1950](V2MA01_Poly7b.pdf) -->
<!-- - Extraits :
  - Le début de la fin du Code de production à Hollywood :
    - [_Psychose_ (_Psycho_, Alfred Hitchcock, 1960) : ouverture](https://mediaserver.lecnam.net/permalink/v1261954f6042nbvp31j/iframe/)
    - [_Certains l'aiment chaud_ (_Some Like It Hot_, Billy Wilder, 1959) : fiançailles](https://mediaserver.lecnam.net/permalink/v1261954f765b7757w0g/iframe/) -->

### 12 avril 2023

En raison d'une nouvelle fermeture de l'université, cette séance est annulée. **Merci d'étudier cette [présentation écrite]({{< relref italie.md >}}) avec notes, diapos et extraits.**

- L'époque fasciste
- L'àprès-guerre et le néoréalisme
- Analyse de _Riz Amer_

#### Ressources

<!-- - {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos8.pdf) -->
- {{< icon name="file" pack="fas" >}} [Notes, diapos, et extraits]({{< relref italie.md >}})
- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma italien, 1929-1959](V2MA01_Poly8.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Jean A. Gili, "_Riz amer_ : Entre Dovjenko et Matarazzo"](5/gili_riz_amer.pdf)
<!-- - {{< icon name="file-pdf" pack="fas" >}} [André Bazin, "Une grande œuvre : _Umberto D._"](5/bazin_umberto_d.pdf) -->
<!-- - Extraits :
  - [_Riz amer_ : début](https://mediaserver.lecnam.net/permalink/v12619a1564fbxyq928h/iframe/)
  - [_Umberto D._ : le réveil de la bonne](https://mediaserver.lecnam.net/videos/hcc05x02/)
  - [_La Strada_ : début](https://mediaserver.lecnam.net/videos/hcc05x03/)
  - [_Dommage que tu sois une canaille_ : la plage](https://mediaserver.lecnam.net/videos/hcc05x04/) -->
<!--  - [_Chien enragé_ : début](https://mediaserver.lecnam.net/videos/hcc05x05)-->
### 19 avril 2023

Retour sur les cinémas américain et français des années 1940 et 1950.

Avant ce cours, merci de regarder cette [présentation du cinéma français de 1940 à 1960](https://drive.google.com/file/d/1xJfHX3YWtD8-MeMk4tp-GhuESzIxcCa3/view?usp=sharing) (56 minutes, extraits de films compris).

#### Ressources :

- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos7.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma français sous l'occupation + de 1945 à 1960](V2MA01_Poly6.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma américain des années 1940](V2MA01_Poly7a.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma américain des années 1950](V2MA01_Poly7b.pdf)

### 26 avril 2023

Le cinéma japonais de 1929 à 1959

Le cinéma soviétique de 1929 à 1959

#### Ressources

- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos9.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le cinéma japonais, 1930-1960](V2MA01_Poly9.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le cinéma soviétique, 1930-1960](V2MA01_Poly10.pdf)
 - Extraits :
  - [_Voyage à Tokyo_ : l'arrivée des parents](https://mediaserver.lecnam.net/videos/hcc09x01/)
  - [_L'Intendant Sansho_ : frère et sœur séparés](https://mediaserver.lecnam.net/videos/hcc09x02/)
  - [_Chien enragé_ : début](https://mediaserver.lecnam.net/videos/hcc09x03/)
  - [_Godzilla_ : quel est le vrai sujet de ce film ?](https://mediaserver.lecnam.net/videos/hcc09x04/)
  - [_La Chute de Berlin_ : conclusion](https://mediaserver.lecnam.net/videos/hcc10x01/)
  - [_Quand passent les cigognes_ : la caméra et Veronika](https://mediaserver.lecnam.net/videos/hcc10x02/)
  - [_Quand passent les cigognes_ : viol](https://mediaserver.lecnam.net/videos/hcc10x03/)
  - [_Quand passent les cigognes_ : conclusion](https://mediaserver.lecnam.net/videos/hcc10x04/)

<!-- - {{< icon name="file-alt" pack="fas" >}} [Katherine Trendacosta, "Ce que les studios de cinéma refusent de comprendre au streaming"](cartel/) -->

<!-- ### 26 avril 2022

**[Deuxième partiel]( relref devoir.md reconstruct with \{\{\< \}\}\> ) à rendre au plus tard mardi 26 avril à midi.**
**Deuxième partiel.** Voir ["validation"](#validation) ci-dessus. -->

### Devoir final

À rendre le 10 mai 2023. **[Voir le détail du sujet ici.]({{< relref devoir.md >}})**
<!-- ### Modules asynchrones

1) [Cinéma hollywoodien des années 1930](https://icampus.univ-paris3.fr/mod/url/view.php?id=218624), module de Nachiketas Wignesan. À regarder avant le cours du 12 février, disponible jusqu'au 14 mars.
2) [Les identités visuelles des films hollywoodiens des années 1930-1940](https://icampus.univ-paris3.fr/mod/url/view.php?id=218183), module de Maureen Lepers. À regarder avant le cours du 12 février, disponible jusqu'au 14 mars.
3) [Le Cinéma français des années 1930](https://icampus.univ-paris3.fr/mod/url/view.php?id=219726), module de Chloé Stephant. À regarder avant le cours du 19 février, disponible jusqu'au 14 mars.
4) [Le Cinéma allemand de 1929 à 1945](https://icampus.univ-paris3.fr/mod/url/view.php?id=219529), module de Pierre-Antoine Bourquin. À regarder avant le cours du 19 février, disponible jusqu'au 14 mars.
5) [Cinéma français des années 1940-50](https://filesender.renater.fr/?s=download&token=31a1058c-3e90-40e0-9ddc-072073e8cc37), module de Daniel Morgan. À regarder avant les cours du 19/26 mars, disponible jusqu'au 31 mars.
6) [Cinéma hollywoodien des années 1940-50](https://icampus.univ-paris3.fr/mod/url/view.php?id=233020), module de Flavia Soubiran. À regarder avant les cours du 19/26 mars, disponible jusqu'au 31 mars.
7) [Cinéma italien de 1929 à 1959](https://icampus.univ-paris3.fr/mod/url/view.php?id=247395), module de Guglielmo Scarfimuto. À regarder avant le cours du 9 avril, disponible jusqu'au 30 avril.
8) [Cinéma japonais de 1929 à 1959](https://icampus.univ-paris3.fr/mod/url/view.php?id=247284), module de Jérôme Bloch. À regarder avant le cours du 9 avril, disponible jusqu'au 30 avril.
9) Cinéma soviétique de 1929 à 1959, module de Thomas Pillard : [diapos](https://icampus.univ-paris3.fr/mod/url/view.php?id=216135), [audio](https://icampus.univ-paris3.fr/mod/url/view.php?id=216138), [extraits](https://icampus.univ-paris3.fr/mod/url/view.php?id=216141). À regarder avant les cours du 16/30 avril, disponible jusqu'au 30 avril.
10) [Cinéma indien de 1929 à 1959](https://icampus.univ-paris3.fr/mod/url/view.php?id=246435), module d'Ophélie Wiel. À regarder avant les cours du 16/30 avril, disponible jusqu'au 30 avril. -->
