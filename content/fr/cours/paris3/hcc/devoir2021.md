---
# Page title
title: Devoir final

# Title for the menu link if you wish to use a shorter link title, otherwise remove this option.
# linktitle: 

# Page summary for search engines.
#summary: Blah, blah, blah...

# Date page published
date: 2021-03-20

# Academic page type (do not modify).
type: book
toc: false

# Position of this page in the menu. Remove this option to sort alphabetically.
weight: 14
draft: true
---

{{% callout note %}}

Ce **travail individuel** est à rendre par e-mail à <daniel.morgan@sorbonne-nouvelle.fr> pour le 7 mai 2021 à 12h, dernier délai. Chaque jour de retard entraînera une pénalité d'un point.

{{% /callout %}}

{{< figure src="../sentimental-prise.jpg" caption="Eddie Constantine dans _Je suis un sentimental_ (John Berry, 1955)" >}}

Vous rédigerez, sous la forme d'un **article** ou un **clip vidéo**, un texte de présentation et d'analyse d'un des films au programme : ou bien l'un des cinq films obligatoires (_Chercheuses d'or de 1933_, _M le maudit_, _Avant le déluge_, _Riz amer_, _Quand passent les cigognes_), ou bien l'un des longs métrages figurant sur la [liste complémentaire]({{< relref "filmo_extra.md" >}}).

Votre article ou vidéo doit comprendre :

- Une **courte présentation** du cinéaste, scénaristes et/ou interprètes.
- Au moins **2 images légendées** tirées du film, parmi les plus représentatives.
- Un **court résumé personnel** du film et de sa fiche technique, faisant mention de la date de réalisation ou de sortie en salles. 
- Un **encadré sur le genre, mouvement ou tendance** auquel le film vous semble pouvoir être rattaché (aidez-vous de votre cours).
- Une **analyse détaillée du film d'environ 6 000 signes**, comprenant un **regard personnel** sur le scénario, la mise en scène, la photographie, la musique, la direction et le jeu d'acteurs·rices, les enjeux idéologiques du film, et tout ce qui vous paraîtra pertinent sur le plan cinématographique et socio-historique. Il ne s'agit pas d'énumérer ces différents éléments et de les étudier l’un après l’autre, mais bien de faire une analyse globale en vous concentrant sur ce qui fait selon vous l’intérêt et la spécificité du film, notamment par rapport à son contexte d’émergence et à l’histoire du cinéma où il s’insère. En revanche, on ne vous demande pas votre avis personnel sur le film : ce n’est pas une critique (que vous ayez aimé ou pas n’est pas l’objet de ce travail).
- Une **courte conclusion** portant sur ce qui fait, pour vous, l'importance de cette œuvre dans l'histoire du cinéma.

{{% callout warning %}}

**Le plagiat d'Internet ou d'ouvrages de cinéma ne sera pas toléré.** Il s'agit d'un dossier personnel. Mis à par pour les images, les recherches biographiques et la fiche technique, l'utilisation de ressources autre que votre cerveau est interdite.
{{% /callout %}}

Soignez la présentation, l’organisation et l’orthographe de votre article, qui ne doit pas comporter de fautes ou de coquilles. Imaginez qu’il s’agit d’un texte faisant partie d’un dossier pédagogique sur le film, destiné à des étudiant·e·s en cinéma. La longueur attendue pour le dossier, au format Word ou PDF, est de 2 ou 3 pages maximum. Il vous revient de proposer une mise en page soignée, permettant de rendre la lecture de l’article (toujours en imaginant qu’il se destine à apparaître dans un dossier pédagogique) intelligible, claire et attrayante.

Dans le cas d’un clip vidéo d’une durée conseillée de 5-6 minutes, vous en définirez librement la forme en veillant à adapter les consignes au format et à ne pas dépasser les 7-8 minutes.
