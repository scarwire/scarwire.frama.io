---
# Page title
title: Devoir final

# Title for the menu link if you wish to use a shorter link title, otherwise remove this option.
# linktitle: 

# Page summary for search engines.
#summary: Blah, blah, blah...

# Date page published
date: 2023-04-20

# Academic page type (do not modify).
type: book
toc: false
draft: true

# Position of this page in the menu. Remove this option to sort alphabetically.
weight: 15

---

{{< figure src="../chien-enrage.jpeg" caption="_Chien enragé_ (Akira Kurosawa, 1949).<br /> Franchement, c’est chaud, la situation dans laquelle se trouve l’inspecteur Murakami (Toshirō Mifune). Cette évaluation, par contre, ne doit pas être une source de stress à ce point." >}}

# Sujet

Écrivez une dissertation argumentée de 7 500 à 10 000 signes sur **un** des trois sujets suivants. Votre dissertation doit comprendre une introduction, où vous annoncerez la problématique, ainsi qu'une brève conclusion. 

1) Choisissez **un ou deux** des films suivants, tous sortis en salles la même année, tous ayant été remarqués pour leur esthétique réaliste et leur tournage en décors réels :

   - [_Au-delà des grilles_](#liens-vers-les-films) (René Clément, 1949)
   - [_Les Bas-fonds de Frisco_](#liens-vers-les-films) (_Thieves' Highway_, Jules Dassin, 1949)
   - [_Chien enragé_](#liens-vers-les-films) (_Nora inu_, Akira Kurosawa, 1949)
   - [_Riz amer_](#liens-vers-les-films) (_Riso amaro_, Giuseppe de Santis, 1949)

   En vous appuyant sur des analyses de séquences spécifiques, analysez l'équilibre entre fiction et réalité dans le ou les films en question. Pour chaque film que vous examinez, quel est son rapport à la société, l'époque et le milieu où le récit se déroule ? Quel est son rapport au mode dominant de création cinématographique dans son pays de réalisation ? 

   (Attention : si vous choisissez d'écrire sur _Au-delà des grilles_, vous devrez évoquer deux pays car il s'agit d'une coproduction franco-italienne !)

2) De quelle manière _Quand passent les cigognes_ (Mikhaïl Kalatozov, 1957) aborde-t-il la Seconde Guerre mondiale en particulier et l'expérience de la guerre en général ? Appuyez-vous sur une analyse des personnages, du récit, et/ou des séquences précises du film. Dans un second temps, examinez la façon dont cette perspective sur la guerre de 1939-1945 fait transparaître des enjeux politiques et culturels de l'Union soviétique de 1957.

   Une idée de conclusion, facultative : si vous le souhaitez, vous pouvez terminer par un paragraphe (pas plus) qui discute de la pertinence éventuelle de ce film par rapport à la guerre actuelle en Ukraine.

3) Voici une échange avec le _chatbot_ ChatGPT autour du cinéma classique :

   > **Question.** _En quoi le concept du "cinéma classique" s'applique-t-il de manière similaire ou inégale d'un pays à l'autre ? Prenez le cas des États-Unis, la France et l'Italie dans la période de 1930 à 1960. Considérez le développement de l'industrie cinématographique et l'évolution de différents courants esthétiques dans les trois pays, et appuyez votre réponse sur des exemples de films précis._
   >
   > **Réponse.** Le concept du "cinéma classique" peut varier d'un pays à l'autre en raison de différences culturelles, politiques, économiques et historiques. Cependant, les États-Unis, la France et l'Italie ont tous connu une période de cinéma classique qui a eu lieu principalement entre les années 1930 et 1960.
   > 
   > Aux États-Unis, le cinéma classique est caractérisé par des studios de cinéma puissants et une production de films industrielle, avec un accent sur la narration linéaire, la clarté et la continuité. Des genres tels que le film noir, le western, la comédie musicale et le film de guerre étaient populaires. Les exemples de films classiques américains incluent _Citizen Kane_ (1941) d'Orson Welles, _Casablanca_ (1942) de Michael Curtiz et _The Searchers_ (1956) de John Ford.
   > 
   > En France, le cinéma classique était plus axé sur l'art et la créativité, avec une attention particulière portée à la réalisation et à la mise en scène. Les cinéastes français étaient souvent associés à la Nouvelle Vague, un mouvement qui a remis en question les conventions de la narration cinématographique et a introduit des techniques telles que la caméra à l'épaule et le saut de coupe. Les exemples de films classiques français incluent _Les Enfants du paradis_ (1945) de Marcel Carné, _Le Salaire de la peur_ (1953) d'Henri-Georges Clouzot et _Les Quatre Cents Coups_ (1959) de François Truffaut.
   > 
   > En Italie, le cinéma classique était caractérisé par le néoréalisme italien, qui a mis l'accent sur la représentation de la vie quotidienne et de la pauvreté après la Seconde Guerre mondiale. Les films étaient souvent tournés en extérieur et mettaient en vedette des acteurs non professionnels. Les exemples de films classiques italiens incluent _Rome, ville ouverte_ (1945) de Roberto Rossellini, _Le Voleur de bicyclette_ (1948) de Vittorio De Sica et _La Dolce Vita_ (1960) de Federico Fellini.
   > 
   > En conclusion, bien que les États-Unis, la France et l'Italie aient tous connu une période de cinéma classique, les caractéristiques de ces périodes varient en fonction des influences culturelles et artistiques de chaque pays.

   Commencez par analyser la réponse de ChatGPT dans un bref commentaire d'une page environ. Sur quels points êtes-vous d'accord avec l'ordinateur ? Sur quels points trouvez-vous la réponse insuffisante, voire trompeuse ou erronée ? Ensuite, écrivez votre propre réponse à la même question. Vous pouvez garder la même structure et langage que ChatGPT si vous le souhaitez, mais votre réponse doit être plus détaillée. Sentez-vous libre de choisir d'autres exemples de films et de poursuivre votre propre raisonnement.

# Rendu des copies

Soignez la présentation, l’organisation et l’orthographe de votre dissertation, qui ne doit pas comporter de fautes ou de coquilles. La longueur attendue pour le devoir est de **5 pages maximum**.

{{% callout note %}}

Ce **travail individuel** est à rendre par e-mail à <daniel.morgan@sorbonne-nouvelle.fr> jusqu'au **mercredi 10 mai 2023 à 18h**, dernier délai. Chaque heure de retard entraînera une pénalité d'un point.

{{% /callout %}}

{{% callout warning %}}

**Le plagiat d'Internet ou d'ouvrages de cinéma ne sera pas toléré.** La dissertation que vous rendez doit être entièrement le produit de votre propre travail. Vous êtes libres (et encouragés !) de consulter les films, vos notes, une encyclopédie ou d'autres outils de référence, ainsi que des ouvrages sur le cinéma. Cependant, toute citation ou paraphrase doit obligatoirement être signalé par une note de bas de page. (Voir [ce guide](https://bib.umontreal.ca/citer/styles-bibliographiques/chicago) afin de connaître un bon système pour citer vos sources.) **Tout passage copié ou paraphrasé sans attribution entraînera la note de zéro sur vingt.**

{{% /callout %}}

## Liens vers les films

Vous êtes invités à consulter les films et à appuyer vos conclusions sur de courtes analyses de séquences et d'images. Si souhaitez voir ou revoir les films, des copies sont à votre disposition :

- _Au-delà des grilles_ : [téléchargement](https://filesender.renater.fr/?s=download&token=7368be57-cb9d-43f1-b2a0-8e3035dafff5), [lien alternatif](https://uptobox.com/yymedax5wxv4)
- _Les Bas-fonds de Frisco_ : [téléchargement](https://filesender.renater.fr/?s=download&token=63fb7f6f-7268-4e74-94d7-793b49c2f449)
- _Chien enragé_ : [téléchargement](https://filesender.renater.fr/?s=download&token=e25aefca-691f-4fda-a391-d9c00a21eb61)
- _Quand passent les cigognes_ : [téléchargement](https://filesender.renater.fr/?s=download&token=c79ed2d4-3dd8-41ec-b1a6-b43b86f24c21), [lien alternatif](https://uptobox.com/6uin4c3c0pxu/Kalatozov-1957-Quand%20passent%20les%20cigognes_multi_720.mp4)
- _Riz amer_ : [téléchargement](https://filesender.renater.fr/?s=download&token=60a3b340-0a35-4503-ad6f-3f1df299f643), [lien alternatif](https://uptobox.com/nsm6erxv11q7)
