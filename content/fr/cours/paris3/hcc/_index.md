---
title: Histoire du cinéma classique
date: 2025-01-21
type: book
toc: true
weight: 11
---

Université Sorbonne Nouvelle  
Groupe TD 6, mercredi 8h00-10h00, salle C403  
Enseignant : Daniel Morgan  
E-mail : [daniel.morgan@sorbonne-nouvelle.fr](mailto:daniel.morgan@sorbonne-nouvelle.fr)

{{< figure src="denham.jpeg" title="_King Kong_ (M. Cooper/E. Shoedsack, 1933)" >}}

Bienvenue dans mon groupe du cours d'**Histoire du cinéma classique** en Licence 1 cinéma et audiovisuel à l'université Sorbonne Nouvelle ! Ce cours abordera la période dite « classique », de l'introduction du cinéma parlant à la fin des années 1920 jusqu'au début des Nouvelles Vagues aux environs de 1960. Nous aborderons le cinéma de sept pays qui ont été des grand producteurs et consommateurs de films à cette époque: la France, l'Italie, l'Allemagne, le Royaume-Uni, les États-Unis, le Japon et l'Union Soviétique.

{{% toc hide_on="xl" %}}

## Films au programme

### Films obligatoires

Les films suivants sont **obligatoires**. Vous devez impérativement les voir pour les dates indiquées :

- _Les Chasses du Comte Zaroff_ (*The Most Dangerous Game*, Ernest B. Schoedsack/Irving Pichel, 1932)  
:arrow_right: à voir pour le 29 janvier 2025 (si possible)  
{{< icon name="film" pack="fas" >}} **Séances à la [Cinémathèque universitaire](http://www.univ-paris3.fr/cinematheque-universitaire--27069.kjsp)** (salle BR10, rez de jardin, bâtiment B) : lundi 27/01 à 16h et vendredi 31/01 à midi.  
{{< icon name="film" pack="fas" >}} [téléchargement](https://filesender.renater.fr/?s=download&token=5f15ff78-a740-454c-a0d1-55a0954a266d) possible jusqu'au 1er mars
- _Le Quai des brumes_ (Marcel Carné, 1938)  
:arrow_right: à voir pour le 7 février 2025  
{{< icon name="film" pack="fas" >}} **Séances à la Cinémathèque universitaire** : lundi 10/02 à 16h et vendredi 14/02 à 12h<!--  
{{< icon name="film" pack="fas" >}} [téléchargement](https://filesender.renater.fr/?s=download&token=a8732cbf-3ed7-443c-9ba5-b93d7e3a5e0d) possible jusqu'au 22 février-->
- _Jeunes filles en uniforme_ (_Mädchen in Uniform_, Leontine Sagan/Carl Froelich, 1931)  
:arrow_right: à voir pour le 5 mars 2025  
{{< icon name="film" pack="fas" >}} **Séances à la Cinémathèque universitaire** : lundi 17/02 à 16h et vendredi 21/02 à 12h
- _Justice est faite_ (André Cayatte, 1950)  
:arrow_right: à voir pour le 19 mars 2024 (si possible)  
{{< icon name="film" pack="fas" >}} **Séances à la Cinémathèque universitaire** : lundi 17/03 à 16h et vendredi 21/03 à 12h
- *Umberto D.* (Vittorio De Sica, 1951)  
:arrow_right: à voir pour le 27 mars 2024  
{{< icon name="film" pack="fas" >}} **Séances à la Cinémathèque universitaire** : lundi 31/03 à 16h et vendredi 04/04 à 12h
- _Bonjour_ (*Ohayō*, Yazujirō Ozu, 1959)  
:arrow_right: à voir pour le 17 avril 2024  
{{< icon name="film" pack="fas" >}} **Séances à la Cinémathèque universitaire** : lundi 07/04 à 16h et vendredi 11/04 à 12h

### Pour aller plus loin

- La **Cinémathèque universitaire** propose une [programmation spécifique](cin_u_2024.png) pour ce cours. Assistez à autant de séances que vous pouvez, vous ne serez pas déçus !
- [Une liste complémentaire]({{< relref "filmo_extra.md" >}}) de films classiques à voir juste pour le kif
- La [bibliothèque universitaire](https://www.dbu.univ-paris3.fr/) dispose d'une excellente collection de DVDs, du matériel pour les visionner sur place, ainsi que de très nombreux livres et revues sur le cinéma
- Le site archive.org met à disposition [une collection de longs métrages gratuits](https://archive.org/details/feature_films), y compris certains films classiques. Ce site propose également des livres sur le cinéma [complètement gratuits](https://openlibrary.org/subjects/film) et d'autres [empruntables](https://archive.org/details/inlibrary?and[]=cinema&sin=&and[]=subject%3A%22Films%2C+cinema%22&and[]=subject%3A%22Film%22&and[]=subject%3A%22Motion+pictures%22) si vous avez créé un compte.
- Des [conseils sur l'accès aux films]({{< relref "acces.md" >}}), dont quelques astuces sur le téléchargement. Plus parfaitement à jour (cela date de 2021), et à utiliser avec précaution.

## Bibliographie indicative

- AUGROS Joël et KITSOPANIDOU  Kira, [_Une  Histoire du cinéma  américain.  Stratégies, révolutions et mutations au XXe siècle_](http://libgen.li/ads.php?md5=193B5AC9A671502EED2485903C7B8F1E), Paris, Armand Colin, 2016
- BILLARD Pierre, _L’Âge  classique  du  cinéma  français.  Du  cinéma  parlant à la  Nouvelle Vague_, Paris, Flammarion, 1995
- BOURGET Jean-Loup, [_Hollywood.  La  norme  et  la  marge_](http://libgen.li/ads.php?md5=ACFF4E8F07EBD7A8B6E68DC0246BE648) \[1998\],  Paris, Armand  Colin, 2e édition, 2016
- BURCH Noël et SELLIER Geneviève, [_La Drôle de guerre des sexes du cinéma français, 1930-1956_](https://catalogue-bsn.sorbonne-nouvelle.fr/permalink/33USPC_USN/1lm7vtm/cdi_askewsholts_vlebooks_9782140113796), édition revue et augmentée, Paris, L'Harmattan, 2019
- EISENSCHITZ Bernard, _Le Cinéma  allemand_ \[1999\], Paris,  Armand  Colin, 2e édition, 2008
- JEANCOLAS Jean-Pierre, [_Le Cinéma français_](https://libgen.li/ads.php?md5=C51E6271E30FE1D94FD6984C92F445C1) \[1993\], Paris, Armand Colin, 4e édition augmentée, 2019.
- NACACHE Jacqueline, _Le Film hollywoodien classique_, Paris, Armand Colin, 2005
- PILLARD Thomas, _Le Film noir français face aux bouleversements de la France d’après-guerre (1946-1960)_, Nantes, Éditions Joseph K., 2014
- SCHIFANO Laurence, [_Le Cinéma italien de 1945 à nos jours. Crise et création_](http://libgen.li/ads.php?md5=FA3E5088912F52395D21ADC399C2F958) \[1995], Paris, Armand Colin, 5e édition revue et augmentée, 2022
- SUMPF Alexandre, _Révolutions  russes  au  cinéma. Naissance d’une  nation: URSS, 1917-1985_, Paris, Armand Colin, 2015
- TESSIER Max et MONVOISIN Frédéric, _Le Cinéma japonais. Une introduction_ \[1997\], Paris, Armand Colin, 3e édition actualisée et augmentée, 2018
- (Si vous lisez bien l'anglais) THOMPSON Kristin et BORDWELL David, [_Film History: An Introduction_](http://libgen.li/ads.php?md5=5BCA8F173CF7F8952412A46111C0D398) \[1993\], New York, McGraw-Hill, 5e édition, 2021

## Validation

Vous serez évalué·es à partir de deux épreuves :

- Une épreuve de mi-semestre le **12 mars 2025**. Vous aurez **1h30** pour répondre à des questions concernant les contenus des cinq premiers cours, et notamment sur l'un des films obligatoires.
- Un devoir sur table final le **30 avril 2025**. Vous aurez **deux heures** pour rédiger une dissertation portant sur une question transversale du cinéma classique. Vous devrez répondre en faisant référence aux films classiques que vous aurez vus pendant le semestre.

Votre présence en cours ne sera pas contrôlée mais est **très fortement conseillée**.

## Calendrier

### 22 janvier 2025

Introduction

- Qu'est-ce que l'histoire du cinéma ?
- La transition vers le parlant

#### Ressources

- {{< icon name="file-pdf" pack="fas" >}} [Diapos du premier cours](diapos1.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : du muet au parlant](1/poly_parlant.pdf)
- Extraits :
  - [_Le Chanteur de jazz_, 1 - dialogues/chanson](https://www.youtube.com/watch?v=8SzltpkGz0M)
  - [_Le Chanteur de jazz_, 2 - modernité/tradition](https://mediaserver.lecnam.net/permalink/v1261898383cb0af6ac8/iframe/)
- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos1.pdf)
- Lectures :
  - {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le passage au parlant](1/poly_parlant.pdf)
  - {{< icon name="file-pdf" pack="fas" >}} [Entretien avec l'historien Kevin Brownlow sur la généralisation du parlant](1/brownlow.pdf)

### 29 janvier 2025

Le cinéma américain des années 1930 (1)

- Le système des studios
- De nouveaux genres
- Hollywood face à la crise économique
- Analyse de _Chercheuses d'or de 1933_ (Mervyn LeRoy, 1933)

#### Ressources

- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos2.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : les studios et le cinéma hollywoodien des années 1930](V2MA01_Poly2.pdf)
- Extraits :
  - [_Chercheuses d'or de 1933_ : « We're in the money »](https://mediaserver.lecnam.net/permalink/v12618e75d52aja0tk5v/iframe/)
  - [_Chercheuses d'or de 1933_ : « In the shadows »](https://mediaserver.lecnam.net/permalink/v12618e75eb78t2j24no/iframe/)
  - [_Chercheuses d'or de 1933_ : « Forgotten man »](https://mediaserver.lecnam.net/permalink/v12618e75ee34w5gzeaf/iframe/)
- Films entiers (!) :
  - :film_projector: [_Les Chasses du Comte Zaroff_ (E. Schoedsack/I. Pichel, 1932)](https://filesender.renater.fr/?s=download&token=5f15ff78-a740-454c-a0d1-55a0954a266d)
  - :film_projector: [_King Kong_ (M. Cooper/E. Schoedsack, 1933)](https://filesender.renater.fr/?s=download&token=35637c66-eb08-440c-af36-65587023816b)
  - :film_projector: [_Chercheuses d'or de 1933_ (Mervyn LeRoy, 1933)](https://filesender.renater.fr/?s=download&token=8cab4fe2-b33b-4097-93dd-a947da4f84f5)
- Lectures pour la prochaine séance :
  - {{< icon name="file-pdf" pack="fas" >}} [Edgar Morin, _Les Stars_, 1957 (extraits)](morin.pdf)
  - {{< icon name="external-link-alt" pack="fas" >}} Un [résumé du code de production](https://fr.wikipedia.org/wiki/Code_Hays) plutôt bien écrit sur Wikipédia
  - {{< icon name="external-link-alt" pack="fas" >}} Quelques informations complémentaires sur l'[époque pré-code](http://cinemaclassic.free.fr/code/code.htm)

### 5 février 2025

Le cinéma américain des années 1930 (2)

- Censure (le code de production)
- Stars
- Genres
- Analyse des _Chasses du Comte Zaroff_ ([à regarder obligatoirement avant cette séance](#films-obligatoires))

#### Ressources

<!--- Film entier-->
<!--  - [_Baby Face_](https://archive.org/details/baby-face-1933-barbara-stanwyck-john-wayne-george-brent-theresa-harris-283708426915) (Alfred E. Green, 1933) avec Barbara Stanwyck, exemple du cinéma pré-code-->
- Extraits :
  - [_L'Ennemi public_](https://www.youtube.com/watch?v=cBjXcyZy2eI) (_The Public Enemy_, William Wellman, 1931), exemple de cruauté gratuite dans un film pré-code
  - [_Les Anges aux figures sales_](https://mediaserver.lecnam.net/permalink/v12618e81df8f1itxuwb/) (_Angels with Dirty Faces_, Michael Curtiz, 1938), exécution et rédemption du personnage de gangster de James Cagney à l'époque du code de production
  <!-- - [_I’ll Be Glad When You’re Dead You Rascal You_](https://www.youtube.com/watch?v=gPpOJvm6998) (Dave Fleischer, 1932)-->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos3.pdf)-->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le code de production](V2MA01_Poly3.pdf) -->

### 12 février 2025

Le cinéma français des années 1930

- Studios, stars et spectateurs en France
- Le cinéma et le Front populaire
- Le réalisme poétique
- Analyse du _Quai des brumes_ ([à regarder obligatoirement avant cette séance](#films-obligatoires))

#### Ressources

<!--- Films entiers : voir la [filmographie complémentaire]({{< relref "filmo_extra.md" >}}) pour des liens vers _Taris_, _Zéro de conduite_, _Le Crime de Monsieur Lange_ et _Le Quai des brumes_-->
- Extraits :
  - [_Zéro de conduite_, la révolte](https://mediaserver.lecnam.net/permalink/v1261943ce17febrtohm/iframe/)
  - [_Le Crime de Monsieur Lange_, conclusion](https://mediaserver.lecnam.net/permalink/v1261943cdd48ad08qhj/iframe/)
  <!--- [_Le Quai des brumes_, Gabin/Morgan](https://mediaserver.lecnam.net/permalink/v1261943cf3d3yw22orq/iframe/)-->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos4.pdf)-->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le cinéma français des années 1930](V2MA01_Poly4.pdf)-->
- Pour un bon tour d'horizon du cinéma français des années 1930, n'hésitez pas à consulter le chapitre 2 (« Du parlant à la qualité française. 1929-1958 ») d'[_Histoire du cinéma français_ de Jean-Pierre Jeancolas](https://catalogue-bsn.sorbonne-nouvelle.fr/permalink/33USPC_USN/1lm7vtm/cdi_cairn_booktitles_ARCO_JEANC_2019_01).

<!--{{% callout note %}}-->
<!--**Quelques pistes de réflexion sur _Le jour se lève_**-->
<!---->
<!--1) Comment se construit le temps dans ce film ?-->
<!--2) Comment se construit l’espace du film ?-->
<!--3) Quels sont les liens entre (1) et (2) ? De quelle manière la présentation du temps et de l'espace est-elle liée au récit ?-->
<!--4) Quel portrait le film dresse-t-il de différents milieux sociaux ? Du crime ? De l’exclusion ? Le film comporte-t-il un message politique ?-->
<!--5) Quelle image le film donne-t-il des relations entre les sexes ? Comment décrire les rapports entre les quatre personnages principaux ? Quelles sont les préconceptions du protagoniste sur les deux personnages féminins, et comment ces deux femmes sont-elles différentes par rapport à ces idées reçues ? De quelle manière ces idées reçues façonnent le rapport entre François et Valentin, ainsi que la chute de ces deux hommes ?-->
<!--6) _Le jour se lève_ fait partie du réalisme poétique. De quelle façon ? Quels sont ses points communs avec d'autres films de cette tendance ? Qu'est-ce qu'il y a de réaliste dans le film ? De poétique ?-->
<!---->
<!--{{% /callout %}}-->

### 19 février 2025

Le cinéma allemand de 1929 à 1945  
Séance assurée par Thomas Pillard

- La transition vers le parlant
- Le cinéma de la République de Weimar
- Le cinéma du IIIe Reich

#### Ressources

<!--- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos5.pdf)-->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le cinéma allemand, 1930-1944](V2MA01_Poly5.pdf)-->
- Extraits :
  - [_L'Ange bleu_ : le professeur arrive au cabaret](https://drive.google.com/file/d/1cuNUi8d3LXwTjdMloOGUndtM5RWayqjh/view?usp=share_link)
  - [_Les Chemins du paradis_ : version allemande](https://drive.google.com/file/d/1XMBh2rgf0I84s2iyuRVZLUxUswQuK86y/view?usp=share_link)
  - [_Les Chemins du paradis_ : version française](https://drive.google.com/file/d/1YQQo1PXPbs0fuM5ZwDB6eJg6ebxNVgCs/view?usp=share_link)
  - [_Le Testament du Docteur Mabuse_ : Lohmann interroge le directeur de l'hôpital psychiatrique](https://drive.google.com/file/d/1ss_sSGQoxLwfDf-r3Yi_x7iChwSPwMOy/view?usp=share_link) (maintenant avec sous-titres !)
<!--- Film entier :-->
<!--  - [_Kameradschaft_](https://youtu.be/DeVU0LGoOGk) (_La Tragédie de la mine_, Georg Wilhelm Pabst, 1930)-->

**_Vacances la semaine du 24 février._**

### 5 mars 2025

Le cinéma allemand (continuation)

- Analyse de _Jeunes filles en uniforme_ ([à regarder obligatoirement avant cette séance](#films-obligatoires))

Préparation au premier devoir sur table

#### Ressources

<!--- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos-m+partiel.pdf)-->

<!--{{% callout note %}}-->
<!--**Pistes de réflexion sur _M le maudit_**-->
<!---->
<!--1) Quel portrait Lang brosse-t-il de l’Allemagne à la fin de la république de Weimar ?-->
<!--   - Quelle représentation des **citoyens ordinaires** ?-->
<!--   - Quelle représentation des **autorités publiques** et de la **police** ?-->
<!--   - Quelle représentation du **crime organisé** ?-->
<!--2) Quels outils esthétiques sont employés par Lang afin de créer une atmosphère glauque et étouffante ? (Pensez au son, à l’éclairage, au montage.)-->
<!--3) Quel est le portrait de la ville de Berlin dans ce film ?-->
<!--4) Quelle est la signification de la séquence en montage parallèle entre les autorités et les mafieux ?-->
<!--5) Comment décrire le personnage de Beckert (Peter Lorre) ? Que fait Lang pour que Beckert puisse faire l’objet de l’empathie des spectateurs ?-->
<!--6) Quel est le moral de l’histoire ?-->
<!--7) D’après vous, qu’auraient pensé les Nazis de ce film ?-->
<!---->
<!--{{% /callout %}}-->

### 12 mars 2025

**Premier partiel.** Temps de composition : 1h30. Voir [« validation »](#validation) ci-dessus.

Le partiel sera suivi de la projection d'un court métrage.

<!-- #### Ressources
  - [_M le maudit_ : une atmosphère de délation](https://drive.google.com/file/d/1lYs83HHmaVAoOu7I5P0s27aGY50qYE4J/view?usp=sharing)
  - [_M le maudit_ : police et pègre en montage parallèle](https://drive.google.com/file/d/1POLv9LX9RDPxh_B082zFL_xNdQ2y3Tpe/view?usp=sharing)
  - [_M le maudit_ : utilisation du son hors-champ](https://drive.google.com/file/d/1Mc0Z9Pqsp-iTUYacNREKxS9-GlfIkLTs/view?usp=sharing)
  - [_M le maudit_ : concurrence entre mafieux et police](https://drive.google.com/file/d/1Uxf20nC762P8a_i_MCC__i2wavq4q43N/view?usp=share_link)
  - [_M le maudit_ : une conlclusion ambigüe](https://drive.google.com/file/d/1RV4Q-Zhq8D-JgHtqZmoW7lmG6f_FuU-S/view?usp=share_link)
-->

### 19 mars 2024

Le cinéma français des années 1940 et 1950

- Le cinéma sous l'Occupation
- Le cinéma de l'après-guerre
- Analyse de _Justice est faite_ ([à regarder obligatoirement](#films-obligatoires))

#### Ressources

<!--- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos6.pdf) -->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma français sous l'occupation + de 1945 à 1960](V2MA01_Poly6.pdf)-->
<!--- {{< icon name="external-link-alt" pack="fas" >}} Lecture pour accompagner le film : Ophélie Wiel, [« Ce que voulait Lola, l'a-t-elle obtenu ? »](https://www.critikat.com/actualite-cine/critique/lola-montes/)-->
- {{< icon name="file-pdf" pack="fas" >}} Lecture pour accompagner le film : [André Bazin, "La cybernétique d'André Cayatte"](4/bazin_cybernetique.pdf)
<!--- Extraits :-->
<!--  - [_Avant le déluge_ : panique autour de la guerre froide](https://mediaserver.lecnam.net/permalink/v1261954f5f59hx0xpx1/iframe/)-->
<!--  - [_Avant le déluge_ : le meurtre de Daniel](https://mediaserver.lecnam.net/permalink/v1261954f5ef2ixhg513/iframe/)-->
<!--  - [_Avant le déluge_ : le verdict, les adolescents et les parents](https://mediaserver.lecnam.net/permalink/v1261954f5f80qgg2cz3/iframe/) -->

### 26 mars 2025

Le cinéma américain des années 1940 et 1950

- Développements techniques : couleurs et écran large
- Genres et sous-genres
- La fin d'une époque à Hollywood

#### Ressources

<!--- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos7.pdf)-->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma américain des années 1940](V2MA01_Poly7a.pdf)-->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma américain des années 1950](V2MA01_Poly7b.pdf)-->
- {{< icon name="external-link-alt" pack="fas" >}} Pour une bonne introduction aux travaux sur le cinéma du philosophe américain Stanley Cavell, voir Marc Cerisuelo, [« Stanley Cavell et l'expérience du cinéma »](https://www.cairn.info/revue-francaise-d-etudes-americaines-2001-2-page-53.htm).
- Extraits :
  - Le début de la fin du Code de production à Hollywood :
    - [_Psychose_ (_Psycho_, Alfred Hitchcock, 1960) : ouverture](https://mediaserver.lecnam.net/permalink/v1261954f6042nbvp31j/iframe/)
    - [_Certains l'aiment chaud_ (_Some Like It Hot_, Billy Wilder, 1959) : fiançailles](https://mediaserver.lecnam.net/permalink/v1261954f765b7757w0g/iframe/)

### 2 avril 2025

Le cinéma italien de 1929 à 1959

- L'époque fasciste
- L'àprès-guerre et le néoréalisme
- Analyse d'_Umberto D._ ([à regarder obligatoirement](#films-obligatoires))

#### Ressources

<!--- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos8.pdf) -->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma italien, 1929-1959](V2MA01_Poly8.pdf)-->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Jean A. Gili, "_Riz amer_ : Entre Dovjenko et Matarazzo"](5/gili_riz_amer.pdf)-->
- {{< icon name="file-pdf" pack="fas" >}} [André Bazin, "Une grande œuvre : _Umberto D._"](5/bazin_umberto_d.pdf) 
- Extraits :
  - [_Riz amer_ : début](https://mediaserver.lecnam.net/permalink/v12619a1564fbxyq928h/iframe/)
  <!--- [_Umberto D._ : le réveil de la bonne](https://mediaserver.lecnam.net/videos/hcc05x02/)-->
  - [_La Strada_ : début](https://mediaserver.lecnam.net/videos/hcc05x03/)
  - [_Dommage que tu sois une canaille_ : la plage](https://mediaserver.lecnam.net/videos/hcc05x04/) 
<!--  - [_Chien enragé_ : début](https://mediaserver.lecnam.net/videos/hcc05x05)-->

<!-- #### Ressources :

- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos7.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma français sous l'occupation + de 1945 à 1960](V2MA01_Poly6.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma américain des années 1940](V2MA01_Poly7a.pdf)
- {{< icon name="file-pdf" pack="fas" >}} [Fiches de synthèse : le cinéma américain des années 1950](V2MA01_Poly7b.pdf) -->

### 16 avril 2025

Le cinéma japonais de 1929 à 1959

 - L'époque fasciste
 - L'occupation américaine et la transformation de la société
 - La libération de l'expression post-occupation

Le cinéma soviétique de 1929 à 1959

- Analyse de _Quand passent les cigognes_ ([à regarder obligatoirement avant cette séance](#films-obligatoires))


#### Ressources

<!--- {{< icon name="file-pdf" pack="fas" >}} [Diapos](diapos9.pdf)-->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le cinéma japonais, 1930-1960](V2MA01_Poly9.pdf)-->
<!--- {{< icon name="file-pdf" pack="fas" >}} [Fiche de synthèse : le cinéma soviétique, 1930-1960](V2MA01_Poly10.pdf)-->
- Extraits :
  - [_Voyage à Tokyo_ : l'arrivée des parents](https://mediaserver.lecnam.net/videos/hcc09x01/)
  - [_L'Intendant Sansho_ : frère et sœur séparés](https://mediaserver.lecnam.net/videos/hcc09x02/)
  - [_Chien enragé_ : début](https://mediaserver.lecnam.net/videos/hcc09x03/)
  - [_Godzilla_ : quel est le vrai sujet de ce film ?](https://mediaserver.lecnam.net/videos/hcc09x04/)
  - [_La Chute de Berlin_ : conclusion](https://mediaserver.lecnam.net/videos/hcc10x01/)
  - [_Quand passent les cigognes_ : la caméra et Veronika](https://mediaserver.lecnam.net/videos/hcc10x02/)
  - [_Quand passent les cigognes_ : viol](https://mediaserver.lecnam.net/videos/hcc10x03/)
  - [_Quand passent les cigognes_ : conclusion](https://mediaserver.lecnam.net/videos/hcc10x04/)

<!-- - {{< icon name="file-alt" pack="fas" >}} [Katherine Trendacosta, "Ce que les studios de cinéma refusent de comprendre au streaming"](cartel/) -->

_**Vacances la semaine du 14 avril.**_

### 23 avril 2025

Le cinéma britannique de 1929 à 1959

Conclusion et préparation au devoir sur table final


### 30 avril 2025

<!--**[Deuxième partiel( relref devoir.md reconstruct with \{\{\< \}\}\> ) à rendre au plus tard mardi 26 avril à midi.**-->
**Deuxième partiel.** Voir [« validation »](#validation) ci-dessus.

