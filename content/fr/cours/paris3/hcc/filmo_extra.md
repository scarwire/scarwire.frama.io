---
# Page title
title: Filmographie complémentaire

# Title for the menu link if you wish to use a shorter link title, otherwise remove this option.
# linktitle: Cinéma classique

# Page summary for search engines.
#summary: Blah, blah, blah...

# Date page published
date: 2021-01-26

# Academic page type (do not modify).
type: book
toc: false

# Position of this page in the menu. Remove this option to sort alphabetically.
weight: 12
---

Voici une liste de films classiques dont je parlerai ou montrerai des extraits au cours du semestre. Des liens sont disponibles pour télécharger certains ; mais vous pouvez voir les autres à la [bibliothèque universitaire](https://www.dbu.univ-paris3.fr/) en cherchant par titre. À voir juste pour le kif... ou si vous vous retrouvez  avec rien de meilleur à faire :

- _L'Ennemi public_ (_The Public Enemy_, William Wellman, 1931)
- _Les anges aux figures sales_ (_Angels with Dirty Faces_, Michael Curtiz, 1938)
- [_I'll Be Glad When You're Dead You Rascal You_](https://www.youtube.com/watch?v=gPpOJvm6998) (Dave Fleischer, 1932)
- _King Kong_ (Merian Cooper/Ernest Schoedsack, 1932)
- _Les Voyages de Sullivan_ (_Sullivan's Travels_, Preston Sturges, 1941)
- _Tous en scène_ (_The Band Wagon_, Vincente Minnelli, 1953)
- _Tout ce que le ciel permet_ (_All that Heaven Allows_, Douglas Sirk, 1955)
- _L'Invasion des profanateurs de sépultures_ (_Invasion of the Body Snatchers_, Don Siegel, 1956)
- _La Chienne_ (Jean Renoir, 1931)
- [_Taris_](https://www.youtube.com/watch?v=MDHCsLPepRc) (Jean Vigo, 1931)
- [_Zéro de conduite_](https://archive.org/details/zero_de_conduite) (Jean Vigo, 1933)
- [_Le Crime de Monsieur Lange_](https://1fichier.com/?284n1jm9tuz2effcziq2) (Jean Renoir, 1936)
- [_La vie est à nous_](https://parcours.cinearchives.org/Les-films-565-16-0-0.html) (Jean Renoir, 1936)
- [_Le Quai des brumes_](https://1fichier.com/?ixczo19vhk5nu67mu5hw) (Marcel Carné, 1938)
- _Le Corbeau_ (Henri-Georges Clouzot, 1943)
- _Panique_ (Julien Duvivier, 1946)
- _Casque d'or_ (Jacques Becker, 1951)
- _La Traversée de Paris_ (Claude Autant-Lara, 1956)
- _Voyage à Tokyo_ (_Tōkyō monogatari_, Yazujiro Ozu, 1953)
- _L'Intendant Sansho_ (_Sanshō dayū_, Kenji Mizoguchi, 1954)
- _La Rue de la honte_ (_Akasen chitai_, Kenji Mizoguchi, 1956)
- _Joyeux Garçons_ (_Vesyolye rebyata_, Grigori Alexandrov, 1934)
- _Alexandre Nevski_ (Sergueï Eisenstein, 1938)
- _¡Que viva México!_ (Sergueï Eisenstein/Grigori Alexandrov, 1930/1979)
- _L'Ange bleue_ (_Der blaue Engel_, Josef von Sternberg, 1930)
- _Les Amants diaboliques_ (_Ossessione_, Luchino Visconti, 1943)
- _Paisà_ (Roberto Rossellini, 1946)
- _Riz amer_ (_Riso amaro_, Giuseppe De Santis, 1949)
- _Les Nuits de Cabiria_ (_Le notti di Cabiria_, Federico Fellini, 1957)
