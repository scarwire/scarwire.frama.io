---
# Page title
title: Partiel final

# Title for the menu link if you wish to use a shorter link title, otherwise remove this option.
# linktitle: 

# Page summary for search engines.
#summary: Blah, blah, blah...

# Date page published
date: 2022-04-20

# Academic page type (do not modify).
type: book
toc: false
draft: true

# Position of this page in the menu. Remove this option to sort alphabetically.
weight: 14

---

{{< figure src="../kong_face.jpg" caption="_King Kong_ (Merian Cooper/Enrest Schoedsack, 1933).<br/>Il ne fait pas vraiment peur, lui. Cette évaluation non plus !" >}}

Écrivez une dissertation argumentée d'environ 7000-8000 signes sur **un** des deux sujets suivants :

1) Avec leurs monstres surdimensionnés, _King Kong_ (Merian Cooper/Ernest Schoedsack, 1933) et _Godzilla_ (Ishiro Honda, 1954) peuvent tous deux être classés comme films fantastiques. Cependant, ces œuvres, y compris leurs monstres, témoignent de l'époque et de la société dans lesquelles elles ont été réalisées. Comment ? Que représentent ces créatures et la destruction qu'elles provoquent ? Quels sont leurs points communs et leurs différences clés ?

   (Pour bien répondre à cette question, il faudra avoir regardé _Godzilla_ aussi bien que _King Kong_, même si le premier ne figure pas parmi les films obligatoires du cours. La bonne nouvelle : [vous pouvez le regarder dès maintenant !](#liens-vers-les-films))

2) De quelle manière _Quand passent les cigognes_ (Mikhaïl Kalatozov, 1957) aborde-t-il la Seconde Guerre mondiale en particulier et l'expérience de la guerre en général ? Appuyez-vous sur une analyse des personnages, du récit, et/ou des séquences précises du film. Dans un second temps, examinez la façon dont cette perspective sur la guerre de 1939-1945 fait transparaître des enjeux politiques et culturels de l'Union soviétique de 1957.

   Une idée de conclusion, facultative : si vous le souhaitez, vous pouvez terminer par un paragraphe (pas plus) qui discute de la pertinence éventuelle de ce film par rapport à la guerre actuelle en Ukraine.

Votre dissertation doit comprendre une introduction, où vous annoncerez la problématique, ainsi qu'une brève conclusion. 

Soignez la présentation, l’organisation et l’orthographe de votre dissertation, qui ne doit pas comporter de fautes ou de coquilles. La longueur attendue pour le devoir, au format .odt, .docx ou .pdf, est de **4 pages maximum**. 

{{% callout note %}}

Ce **travail individuel** est à rendre par e-mail à <daniel.morgan@sorbonne-nouvelle.fr> le mardi 26 avril 2022 à 12h, dernier délai. Chaque heure de retard entraînera une pénalité d'un point.

{{% /callout %}}

{{% callout warning %}}

**Le plagiat d'Internet ou d'ouvrages de cinéma ne sera pas toléré.** Il s'agit d'un devoir personnel. Vous êtes libres (et encouragés !) de consulter les films et vos notes. Vous pouvez aussi regarder une encyclopédie et/ou des ouvrages de cinéma, mais ceci vous prendra du temps et n'est pas attendu. Cependant, toute citation ou paraphrase doit  obligatoirement être signalé par une note de bas de page. **Tout passage copié ou paraphrasé sans attribution entraînera la note de zéro sur vingt.**

{{% /callout %}}

## Liens vers les films

Vous êtes invités à consulter les films et à appuyer vos conclusions sur de courtes analyses de séquences et d'images. Si souhaitez voir ou revoir les films, des copies sont à votre disposition :

- _King Kong_ : [téléchargement](https://filesender.renater.fr/?s=download&token=1f735d9c-dfb1-42a9-9cec-ba2355d60160), [lien alternatif](https://uptobox.com/9lq6zc1cdts2/1933%20King%20Kong%20MULTi.1080p.HDLight.x264.AC3.mkv)
- _Godzilla_ : [téléchargement](https://filesender.renater.fr/?s=download&token=96b3ed4d-5a53-4f68-a67b-74d668e3a57f), [lien alternatif](https://uptobox.com/sjdc58a4my4a)
- _Quand passent les cigognes_ : [téléchargement](https://filesender.renater.fr/?s=download&token=f999712a-3d70-4501-93f2-b18766c715ba), [lien alternatif](https://uptobox.com/6uin4c3c0pxu/Kalatozov-1957-Quand%20passent%20les%20cigognes_multi_720.mp4)
