---
# Page title
title: Le cinéma italien, 1929-1959

# Title for the menu link if you wish to use a shorter link title, otherwise remove this option.
# linktitle: 

# Page summary for search engines.
#summary: Blah, blah, blah...

# Date page published
date: 2023-04-12

# Academic page type (do not modify).
type: book
toc: false
draft: true

# Position of this page in the menu. Remove this option to sort alphabetically.
weight: 14

---

{{< figure src="../diapos_hcc_it/diapo1.jpg" caption="Cinecittà" >}}

Cet exposé, qui vient remplacer le cours qui aurait eu lieu le matin du 12 avril 2023, vous présentera le cinéma italien classique. N'hésitez pas à m'écrire si vous avez des questions. En lecture complémentaire, je vous conseille fortement les deux premiers chapitres du livre le Laurence Schifano, [_Le Cinéma italien de 1945 à nos jours_](https://ipfs.io/ipfs/bafykbzacedkcovulidgymbtxmr4c4goccqfuyksu3en34eadft2zxe43zl3zw?filename=Schifano%2C%20Laurence%20-%20Le%20cin%C3%A9ma%20italien%20de%201945%20%C3%A0%20nos%20jours%20-%204e%20%C3%A9d-Armand%20Colin%20%282016%29.epub).

{{< figure src="../diapos_hcc_it/diapo2.jpg" caption="Diapo 2" >}}

Dans les derniers cours et ceux qui suivent, il est souvent question du rapport entre le cinéma et la politique. On vient de voir les rapports entre le cinéma allemand et la montée du nazisme au cours des années 1930. Ici on regardera l'Italie sous le fascisme, et prochainement on verra le cas du cinéma dans deux autres régimes totalitaires, le Japon impérial et l'Union soviétique, ainsi que les rapports entre cinéma et politique dans les États-Unis au début de la Guerre froide.

Pour l'Italie, on peut clairement diviser la période étudiée en deux moitiés : **l'avant-guerre** sous le fascisme, et **la période suivant la Libération**, avec la naissance de l'école néoréaliste et le cinéma de l'après-guerre.

{{< figure src="../diapos_hcc_it/diapo3.jpg" caption="Diapo 3" >}}

Dans les années 1920 l'industrie cinématographique italienne n'arrive pas à faire face à la concurrence des films américains. C'est une période de déclin de l'industrie, avec une baisse sensible du nombre de films produits.

- La figure exemplaire de cet immobilisme est **Stefano Pittaluga**, qui domine le cinéma italien en tant que directeur de la seule société verticalement intégrée, la **Cines**. 
- 1925-1929 : seulement une centaine de films produits en Italie. L'arrivée du son accélère le déclin, avec seulement 12 longs métrages produits en 1930 et 13 en 1931.

{{< figure src="../diapos_hcc_it/diapo4.jpg" caption="Diapo 4" >}}

En Italie, la fin des années 1920 représente le retour d’un cinéma de qualité :

- Les cinéastes **Alessandro Blasetti** et **Mario Camerini** émergent à la fin des années 1920 et travaillent pour de nouvelles sociétés de production.
- Selon la critique, ils renouvellent le cinéma italien en le libérant des modèles littéraires. Deux exemples clés : 

- _Sole_ (_Soleil_, Alessandro Blasetti, 1929) - acteurs non professionnells
- _Rotaie_ (_Rails_, Mario Camerini, 1930) - film muet sonorisé ensuite

Introduction du cinéma parlant en 1930. À la différence de la radio, ou plus tard la télévision, le cinéma parlant a la particularité de profiter de la **diversité linguistique** (plusieurs dialectes parlés à travers l'Italie) plutôt que d'homogéniser ou standardiser autour d'un seul dialecte.

{{< figure src="../diapos_hcc_it/diapo5.jpg" caption="Diapo 5" >}}

L'arrivée au pouvoir du dictateur fasciste Benito Mussolini aura des effets importants sur le cinéma italien, en deux temps distincts.

{{< figure src="../diapos_hcc_it/diapo6.jpg" caption="Diapo 6" >}}

Dans une **première période** (1922 jusqu'à 1935 environ), Mussolini doit faire face à une crise économique et intervient relativement peu dans la production culturelle et cinématographique. Il se contente de faire examiner tous les films sortis en Italie par un organisme de censure, ce qui a pour effet de limiter la circulation des films étrangers, ainsi que des films jugés pacifistes ou subversifs. Il établit des liens avec des sociétés de production et crée un système de financement de films italiens par la banque centrale.

Puis, dans un **deuxième temps**, à partir du milieu des années 1930, le gouvernement fasciste décide de centraliser et institutionnaliser la production cinématographique, en reconnaissant, comme les autres régimes totalitaires, son utilité dans une politique culturelle au service du pouvoir. Mussolini lui-même déclare : "Le cinéma est l'art le plus fort".

{{< figure src="../diapos_hcc_it/diapo7.jpg" caption="Diapo 7" >}}

- 1934 : Direction générale de la cinématographie, chargé de l'organisation de l'industrie cinématographique au niveau national, ainsi que la censure
  - Dirigé par Luigi Freddi, ancien chef de la propagande du parti de Mussolini. Celui-ci croyait que l'état devait récompenser l'industrie et non pas dicter son fonctionnement. Il était convaincu que le public italien n'accepterait pas des film fortement propagandistes, et qu'il valait mieux un "cinéma de distraction", semblable à Hollywood. De son point de vue, un public bien diverti était un public docile.
- 1935 : l’institut LUCE (L’Union cinématographique éducative, depuis 1924 le producteur de films d’actualités), passe à la production de films de fiction

{{< figure src="../diapos_hcc_it/diapo8.jpg" caption="Diapo 8" >}}

{{< figure src="../diapos_hcc_it/diapo9.jpg" caption="Diapo 9" >}}

La politique du gouvernement fasciste implique la création de nouvelles institutions promouvant le cinéma :

- La **Mostra de Venise**, premier festival de films du monde. Promotion de l'Italie en tant que pays moderne et cosmopolite, invitation de cinéastes et comédiens du monde entier ;
- Le **Centro sperimentale** (« Centre expérimental », école nationale du cinéma), toujours aujourd'hui l'école de cinéma la plus importante d'Italie ;
- Le **Ministère de la culture populaire** qui établit le financement central de toute l'industrie du spectacle ;
- **Cinecittà**, un ensemble de studios de cinéma parmi les plus grands en Europe, construit dans un quartier périphérique de Rome. Connu par son surnom "Hollywood sur Tibre" pour le nom du fleuve qui traverse la capitale italienne.

{{< figure src="../diapos_hcc_it/diapo10.jpg" caption="Diapo 10" >}}

Aidé par l'état, le cinéma italien a connu de bons résultats au box office et une croissance de la production, jusqu'à 120 film en 1942. Mais la quantité n'est pas forcément synonme de qualité. Sous un régime fasciste, dont le conservatisme cherchait à protéger l'Italie en tant que pays du roi, du pape et du Duce, le cinéma était strictement censuré et il était quasiment impossible de créer des films avec le moindre contenu subversif. Selon le spécialiste du cinéma italien Jean Gili, les films ne montraient qu'une "certaine irréalité sociale".

- Du côté du cinéma documentaire, les actualités LUCE donnent une apparance de légitimité et d'objectivité à des informations clairement sélectionnées et manipulées pour le compte du parti.
- Quant à la fiction, on voit dans ces films certains thèmes typiques du fascisme italien :
  - Exaltation du parti dans _Camicia nera_ (_Chemises noires_, Gioacchino Forzano, 1932), film commandé pour le 10e anniversaire du parti fasciste (appelé ainsi pour l'uniforme du parti)
  - Éloge de l’expansion colonialiste en Éthiopie dans _Il grande appello_ (_Le grand appel_, Mario Camerini, 1936)
  - La gloire des ancêtres romains dans le péplum _Scipione l’Africano_ (_Scipion l’Africain_, Carmine Gallone, 1937), la production la plus coûteuse et monumentale de l'époque fasciste, primé à Venise avec un prix qui était à l'époque nommé pour Mussolini lui-même. Histoire du général romain Scipion et de sa victoire contre les Carthaginiens dans le Deuxième Guerre punique.

Voici un extrait :

{{< vid_iframe "https://drive.google.com/file/d/1FrNyNhv6VMzBq3RAjcHarKeUrtI81bnr/preview" >}}

- On y suggère un parallèle entre la conquête de l'Afrique par la Rome antique et celle entreprise par l'Italie fasciste, notamment en Éthiopie.
- Adoption d'un genre cher à l'industrie cinématographique italienne, le péplum.
- Symbolisme : empire romain -> empire fasciste.
- Rhétorique guerrière : gloire, courage, victoires militaires.
- Exaltation d'une figure de grand chef fort, implacable. Echo de l'image de Mussolini dans toute l'iconographie fasciste de l'époque.

{{< figure src="../diapos_hcc_it/diapo11.jpg" caption="Diapo 11" >}}

Un autre produit caractéristique des années fascistes en Italie, ce sont les _telefoni bianchi_ ("téléphones blancs") : des divertissements légers, créés sur le modèle des comédies hollywoodiennes de Frank Capra ou encore certains films de René Clair. Éloignés de toute préoccupation sociale ou politique, ces films étaient aussi parfois appelés des "comédies hongroises" : certains étaient effectivement situés en Europe centrale, ce qui les permettaient d'évoquer le divorce ou l'adultère, ce qui aurait été censuré si le récit se déroulait en Italie.

On peut noter que ces films ont marqué les débuts de l'acteur et réalisateur Vittorio De Sica et l'écrivain et scénariste Cesare Zavattini, figures clés du néoréalisme d'après-guerre.

{{< figure src="../diapos_hcc_it/diapo12.jpg" caption="Diapo 12" >}}

Au beau milieu de ce cinéma très éloginé de la réalité quotidienne, il y a eu plusieurs tentatives au début des années 1940 de faire des films plus réalistes. La comédie populaire _Quatre pattes dans les nuages_ d'Alessandro Blasetti (1942), ou encore le mélodrame tragique _Les Enfants nous regardent_ (1944) de Vittorio De Sica, sont des exemples, mais la plus grande rupture arrive avec un film de Lucchino Visconti en 1943: _Les Amants diaboliques_ (_Ossessione_).

Cette adaptation du roman _Le Facteur sonne toujours deux fois_ de l'Américain James M. Cain est un film noir à l'italienne : c'est l'histoire d'un jeune vagabond qui rencontre une femme mariée. Ils commencent une liaison adultère qui mènera vers un assassinat. Le film est tourné non pas à Cinecittà, mais dans la vallée de la rivière Po, une région économiquement défavorisée. C'est le véritable contraire des décors et costumes luxueux et personnages insouciants des "téléphones blancs".

Voici le début du film :

{{< vid_iframe "https://drive.google.com/file/d/1fnWRuHKXDgdQ9qtxc5Rso1qdf3CzjFmE/preview" >}}

Le film est sorti en 1943, quelques mois avant l'arrivée progressive des troupes alliées par le sud et la libération de l'Italie. On est toujours sous le fascisme, donc on ne met pas sur le générique l'auteur du roman sur lequel le film est fondé, James M. Cain. Hollywood fait sa propre adaptation de ce roman en 1946.

_Ossessione_ annonce le début du néoréalisme, l'école du cinéma italien qui marquera l'après-guerre.

{{< figure src="../diapos_hcc_it/diapo14.jpg" caption="Diapo 14" >}}

{{< figure src="../diapos_hcc_it/diapo15.jpg" caption="Diapo 15" >}}

Le néoréalisme (une école ou un style, non pas un genre) est un cinéma **proche du documentaire** :

- Il peut y avoir une histoire, mais il faut avant tout un **témoignage** ;
- Les histoires de guerre dans ces films ne sont pas héroïques ;
- Problème : il y a déjà dans les années 1930 une tendance vers davantage de réalisme dans les autres cinémas européens. Donc, s'agit-il d'une véritable rupture ? (Oui, mais ce n'est pas un phénomène complètement isolé non plus. Exemple de _La Bataille du rail_ en France.)

{{< figure src="../diapos_hcc_it/diapo16.jpg" caption="Diapo 16" >}}

L'un des plus grands réalisateurs de l'école néoréaliste est **Roberto Rossellini**.

- Commence comme réalisateur en tournant des films de propagande militaire sous le fascisme.
- Il change de côté après la libération du pays et tourne une triolgie de films humanistes, antifascistes, qui le font surnommer "le père du néoréalisme".
- Dans _Rome, ville ouverte_, on voit tout l'éventail de personnages de la période où Rome était une ville ouverte, c'est à dire que les troupes fascistes se retiraient et les troupes alliés avançaient pour libérer la ville : soldats allemands, partisans communistes, prêtres et surtout des familles ordinaires. D'abord, Rossellini élabore un projet de documentaire sur l'assassinat d'un curé, mais cela devient enfin un film qui réinvente la fiction en montrant la vie des gens ordinaires pendant la guerre.
- Lancement de la carrière de la comédienne emblématique **Anna Magnani** et aussi de l'assistant réalisateur, **Federico Fellini**

Voici la séquence la plus connue, à la fin du film, où la protagoniste, Pina, jouée par Magnani, court après son mari capturé par les soldats allemands. 

{{< youtube CEXOTkBTayM >}}

Enfin, voici un extrait d'un autre film néoréaliste, _Umberto D._ (1952) de Vittorio De Sica. Ce film, très peu réussi au box-office, marque la fin du cycle néoréaliste. Pourtant, certains critiques, notamment en France, saluent la façon dont ce film va encore au-delà des films précédents en témoignant de la vie quotidienne, avec un niveau de détail inhabituel. Dans cette scène, une domestique -- déjà un personnage secondaire du film -- se réveille.

{{< vid_iframe "https://drive.google.com/file/d/1DNd1ZT_AgvNxsGBeqNAULntMnZt5uQR3/preview" >}}

Dans une scène précédente, on a vu que son copain, un soldat, l'a quittée. Au moment où le cadre se resserre autour d'elle, la bonne se rend compte qu'elle est probablement enceinte. Mais cela peut être fait de façon très efficace en quelques secondes ; pourquoi alors tous les détails, le chat qui marche sur le toit, l'allumette qui ne s'allume pas bien sur le mur ?

{{< figure src="../diapos_hcc_it/diapo18.jpg" caption="Diapo 18" >}}

{{< figure src="../diapos_hcc_it/diapo19.jpg" caption="Diapo 19" >}}

{{< figure src="../diapos_hcc_it/diapo20.jpg" caption="Diapo 20" >}}

Finalement, le début d'un dernier film, _La Strada_ (1954) de Federico Fellini. Fellini est l'ancien assistant de Rossellini, et on voit bien que son style s'inspire partiellement du néoréalisme.

{{< vid_iframe "https://drive.google.com/file/d/1tE4RQ_tmDHmJU0hQGXi1WhrEeYraJNlG/preview" >}}

{{< figure src="../diapos_hcc_it/diapo24.jpg" caption="Diapo 24" >}}

À votre avis, qu'est-ce que ce début de film a en commun avec le courant néoréaliste, et qu'est-ce qu'il y a de nouveau ? Comment Fellini -- un grand cinéaste des années 1960 et 1970, auteur de films comme _La Dolce Vita_ et _8 1/2_ -- construit-il son propre style ici, à l'aide des comédiens Giulietta Massina et Anthony Quinn ?
