---
# Page title
title: Spoken reductions in English
lang: en-US

# Title for the menu link if you wish to use a shorter link title, otherwise remove this option.
linktitle: Spoken reductions

# Page summary for search engines.
#summary: Blah, blah, blah...

# Date page published
date: 2021-06-14

# Academic page type (do not modify).
type: book
toc: true

# Position of this page in the menu. Remove this option to sort alphabetically.
weight: 5
---

{{< cnam_mediaserver v1261aba90fb1w5x9npk >}}

## Summary

The following is a list of frequent oral reductions in English.

### First category: expressions ending with "to"

- gonna = going to
- wanna = want to
- hafta = have to
- oughta = ought to
- gotta = got to

### Second category: expressions ending with "of"

- a lotta = a lot of
- outta = out of
- kinda = kind of
- sorta = sort of

### Third category: expressions ending with "have"

- coulda = could have
- woulda = would have
- shoulda = should have
- musta = must have
- mighta = might have

### A few more frequent reductions

- I dunno = I don't know
- d'ja = did you
- whaddaya = what do you/what are you

## Quiz

_For each question, click to listen to a short audio clip. Then choose the best response._

{{< quiz reductions_quiz.js >}}
