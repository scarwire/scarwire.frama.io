+++
widget = "blank"
headless = true  # This file represents a page section.
active = true
weight = 81
title = "Autres communications"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "2"
+++

« Criminelles ou victimes ? Femmes accusées et incarcérées dans le cinéma social français, 1948-1958 », colloque _Le spectacle du crime féminin sur la scène et dans le cinéma européens_, Université Rouen-Normandie, 16 novembre 2018.

« Negotiating images of delinquency: young offenders and reform schools in post-war French cinema », _Studies in French Cinema_ annual conference, King's College, Londres, 30 juin 2016.

« Censorship and the judicial films of André Cayatte », MAGIS Gorizia International Film Studies Spring School, Gorizia (Italie), 8 avril 2014.

« “Concerns over public order”: the reception of the films of André Cayatte, 1948-1955 », journée d’études _Radical Readings/Radical Texts_, Canterbury Christ Church University (Angleterre), 15 mars 2014.

« Cinéma dans les journaux, journalisme au cinéma ? », journée d’études _La Presse : une source essentielle pour la recherche cinématographique_, Université Sorbonne Nouvelle – Paris 3, 13 mars 2014.

« La peine de mort dans le cinéma et la télévision français », université d’été _Esthétisation des médias, médiatisation des arts_, Universidad Complutense, Madrid, 5 juin 2013.
