+++
widget = "blank"
headless = true  # This file represents a page section.
active = true
weight = 75
title = "Activités collectives de recherche, responsabilités administratives"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "2"
+++

Évaluation d’articles à double aveugle et gestion du site web (Open Journal Systems + WordPress) pour la revue à accès ouvert [Media Theory](http://mediatheoryjournal.org).

Organisateur et membre du comité scientifique du colloque **La mort des films**, Université Sorbonne Nouvelle – Paris 3, 6-7 mai 2015, portant sur la nature éphémère du support et de l’art cinématographiques. Rédaction en petite équipe d’un appel à communications, recherche de financements, évaluation des propositions en liaison avec le comité scientifique, organisation logistique de l’événement en lien avec un groupe de bénévoles. [Co-direction d’un numéro de revue](#publications) issu du colloque.

Organisateur de la journée d’études **La presse : une source essentielle pour la recherche cinématographique**, Université Sorbonne Nouvelle – Paris 3, 13 mars 2014.

2013 – 2017 : Membre fondateur, membre du conseil d’administration, webmaster, graphiste pour l'association de jeunes chercheurs Kinétraces.

2013 – 2019 : Représentant des usagers au conseil de l'IRCAV (EA 185, Institut de recherche sur le cinéma et l'audiovisuel, Université Sorbonne Nouvelle – Paris 3).
