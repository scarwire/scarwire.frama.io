+++
widget = "blank"
headless = true  # This file represents a page section.
active = true
weight = 40
title = "Compétences"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "2"
+++

## Langues

- **anglais** : langue maternelle
- **français** : bilingue
- **espagnol** : courant

## Informatique

- composition et édition de textes scientifiques en formats de texte brut :  
  LaTeX, Markdown, Pandoc, Zotero, styles CSL
- PAO : InDesign, Scribus
- retouche photo, conception graphique : GIMP,  Photoshop, Lightroom
- conception et entretien de sites web en HTML5/CSS3
- CMS : Joomla, WordPress, Open Journal Systems, LODEL
- gestion de sites statiques : Hugo
- programmation en Python :  web scraping et traitement XML-TEI (BeautifulSoup), expressions régulières, analyse et visualisation de données (pandas, numpy, scipy, matplotlib), traitement d’images (OpenCV)
- systèmes d’exploitation : Mac, Linux, Windows
- excellente connaissance de la ligne de commande, écriture de scripts Bash
- services auto-hébergés : administration de serveur LAMP, Nextcloud
- gestion de versions avec git
- bases de données : LibreOffice Base, MySQL/MariaDB
