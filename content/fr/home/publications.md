+++
widget = "blank"
headless = true  # This file represents a page section.
active = true
weight = 70
title = "Publications"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "2"
+++

# Direction de numéro de revue

_La Mort des films_, _Kinétraces Éditions_ n° 2, 2017. Co-dirigé avec Laurent Husson. <https://hal.archives-ouvertes.fr/hal-01962723>

# Articles publiés dans des revues scientifiques

[« Censure et consensus dans le cinéma de l’après-guerre : La genèse parallèle d’_Au royaume des cieux_ (Julien Duvivier, 1949) et _La Cage aux filles_ (Maurice Cloche, 1950) »]({{< relref "/censure-consensus" >}}), _French Cultural Studies_, 2022, DOI: [10.1177/09571558221120881](https://dx.doi.org/10.1177/09571558221120881).

<!-- « Du film au téléfilm à thèse : _L'affaire Seznec_, de Cayatte à Boisset », _CinémAction_, numéro sur le cinéma d'Yves Boisset (à paraître 2020). -->

« Relégué aux archives : _L’Affaire Seznec_, un film mort d’un cinéaste rejeté », _Kinétraces Éditions_ n° 2, 2017, p. 57-71. <https://hal-univ-paris3.archives-ouvertes.fr/hal-01478936>

« Cinéma dans les journaux, journalisme au cinéma ? _Razzia sur la chnouf_ et la représentation des stupéfiants dans la presse française des années cinquante », _Kinétraces Éditions_ n° 1, 2015, p. 116-128. <https://hal-univ-paris3.archives-ouvertes.fr/hal-01493859>

# Chapitres, articles de vulgarisation

« Ne jugez pas : André Cayatte et la scène judiciare », _Positif_ n° 704, octobre 2019, p. 71-72. <http://www.revue-positif.net/n704.html>

« La fiction fait du journalisme : la peine de mort dans le cinéma français, 1939–1981 », dans Collectif DAEM, _Esthétisation des médias et médiatisation des arts_, Paris, L'Harmattan, 2016, p. 267-274.
