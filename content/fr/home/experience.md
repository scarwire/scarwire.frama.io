+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Expériences professionnelles"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "01.2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Enseignant d'anglais"
  company = "Conservatoire national des arts et métiers - structure Communication en langues étrangères"
  company_url = "https://langues.cnam.fr/"
  location = ""
  date_start = "2020-09-01"
  date_end = ""
  description = """
  Cours d'anglais professionnel destinés à un public divers d'adultes en plusieurs formations scientifiques.

  """

[[experience]]
  title = "Associate lecturer (chargé d'enseignement)"
  company = "University of Kent - Paris School of Arts and Culture"
  company_url = "https://www.kent.ac.uk/paris/"
  location = ""
  date_start = "2019-09-01"
  date_end = "2020-01-31"
  description = """
  Cours de méthodologie de recherche en histoire du cinéma pour étudiants en master, dispensé en anglais.

  """

[[experience]]
  title = "Chargé de mission éditoriale"
  company = """
  Institut de recherche sur le cinéma et l'audiovisuel  
  Université Sorbonne Nouvelle – Paris 3
  """
  company_url = "http://www.univ-paris3.fr/ircav-institut-de-recherche-sur-le-cinema-et-l-audiovisuel-ea-185-3445.kjsp"
  location = ""
  date_start = "2019-04-01"
  date_end = "2019-06-30"
  description = """
  Harmonisation typographique et bibliographique d’articles
pour un numéro de la revue scientifique _Théorème_.
  
  Traduction du site web et de supports de communication
du laboratoire du français vers l'anglais.
  """

[[experience]]
  title = "ATER en anglais et humanités numériques"
  company = "Université de Versailles Saint-Quentin-en-Yvelines"
  company_url = "http://www.chcsc.uvsq.fr/"
  location = ""
  date_start = "2016-09-01"
  date_end = "2018-08-31"
  description = """
  Licence anglais : TD de cinéma classique hollywoodien et de thème (traduction du français vers l'anglais).
  
  Masters LLCER/Histoire/Culture et communication : CM et TD en humanités numériques. Ateliers de traitement de l’image, LaTeX, langages de balisage, introduction à Linux, méthodes quantitatives d’analyse du cinéma.
  """
  
[[experience]]
  title = "Chargé d'enseignement"
  company = "Université Sorbonne Nouvelle – Paris 3"
  company_url = "http://www.univ-paris3.fr/"
  location = ""
  date_start = "2014-01-01"
  date_end = "2016-05-31"
  description = """
  Cours de cinéma en anglais pour étudiants en licence cinéma et audiovisuel.
  
  + <em>Les classes ouvrières dans le cinéma britannique</em> (L2)
  + <em>La crise : Hollywood dans les années 1930</em> (L3)
  """
  
[[experience]]
  title = "Maître de langue en anglais"
  company = "Institut d'études politiques de Lille"
  company_url = "http://www.sciencespo-lille.eu/"
  location = ""
  date_start = "2013-09-01"
  date_end = "2015-08-31"
  description = """
  Conférences de méthode de langue et civilisation américaines (pour étudiants de première année) et britanniques (deuxième année).
  """

[[experience]]
  title = "Chargé d'enseignement en anglais"
  company = "Conservatoire National des Arts et Métiers, Paris"
  company_url = "http://langues.cnam.fr/le-centre-de-ressources-en-langues--613595.kjsp"
  location = ""
  date_start = "2011-03-01"
  date_end = "2016-06-30"
  description = ""

[[experience]]
  title = "Professeur d'anglais"
  company = "Académie de Paris"
  company_url = "https://www.ac-paris.fr/"
  location = ""
  date_start = "2012-09-01"
  date_end = "2013-06-30"
  description = """
  Professeur aux Lycées Jacques Decour (Paris 9e) et Émile Dubois (Paris 14e).
  """
  
[[experience]]
  title = "Lecteur d'anglais"
  company = "Université Paris Ouest – Nanterre La Défense"
  company_url = "https://www.parisnanterre.fr/"
  location = ""
  date_start = "2010-09-01"
  date_end = "2012-08-31"
  description = """
  Cours d’anglais aux niveaux licence et master pour des formations en sciences de l’ingénieur et métiers du livre.
  """

+++

Expérience totale d'environ **1960 heures équivalent TD** dans l'enseignement supérieur depuis 2010.
