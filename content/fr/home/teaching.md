+++
widget = "blank"
headless = true  # This file represents a page section.
active = true
weight = 90
title = "Enseignements 2024-2025"

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "2"
+++

# Conservatoire national des arts et métiers

Mes cours d'anglais au [Cnam Paris](https://langues.cnam.fr/) sont accessibles via l'[Espace numérique de formation](https://sts.lecnam.net/).
<!-- - [Anglais professionnel (ANG320/ANG330)](https://par.moodle.lecnam.net/course/view.php?id=7949)
- [Anglais pour le master en criminologie](https://par.moodle.lecnam.net/course/view.php?id=7949) -->

# Université Sorbonne Nouvelle -- Paris 3

- [Histoire du cinéma classique (V2MA001)](cours/paris3/hcc)
