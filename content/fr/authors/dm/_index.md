---
# Display name
title: Daniel Morgan

# Username (this should match the folder name)
authors:
- dm

# Is this the primary user of the site?
superuser: true

# Role/position
role: |
  Chercheur en études cinématographiques  
    
  Enseignant d'anglais, cinéma et humanités numériques

# Organizations/Affiliations
organizations:
- name: Université Sorbonne Nouvelle – Paris 3 (IRCAV)
  url: "http://www.univ-paris3.fr/ircav"
- name: Conservatoire national des arts et métiers (service Communication en langues étrangères)
  url: "http://langues.cnam.fr/" 

# Short bio (displayed in user profile at end of posts)
# bio: My research interests include distributed robotics, mobile computing and programmable matter.

# interests:
# - Artificial Intelligence
# - Computational Linguistics
# - Information Retrieval

qualifications:
  - né le 04.04.1984 à Boston (États-Unis)
  - nationalités américaine et française
  - qualifié aux fonctions de maître de conférences en section 18 (arts) du CNU

languages:
  - | 
      anglais : langue maternelle
  - | 
      français : bilingue
  - | 
      espagnol : courant

education:
  courses:
  - course: Doctorat en études cinématographiques et audiovisuelles
    institution: | 
      Université Sorbonne Nouvelle – Paris 3  
    year: 2018
  - course: Master 2 en cinéma anthropologique et documentaire
    institution: Université Paris Ouest – Nanterre La Défense
    year: 2010
  - course: Master 1 en études cinématographiques et audiovisuelles
    institution: Université Sorbonne Nouvelle – Paris 3
    year: 2009
  - course: B.A. en cinéma et médias
    institution: University of Chicago
    year: 2006

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
# - icon: twitter
#   icon_pack: fab
#   link: https://twitter.com/GeorgeCushen
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
# - icon: github
#   icon_pack: fab
#   link: https://github.com/gcushen
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---


Anglophone natif installé en France depuis 2007, j'ai une expérience solide et variée dans l'enseignement supérieur, la recherche et l'édition numérique. Spécialiste de l'histoire du cinéma français, j'ai soutenu ma [thèse de doctorat](http://theses.fr/2018USPCA124) en novembre 2018 : _Du crime de guerre au fait divers. La justice pénale, un enjeu politique dans le cinéma français, 1945-1958._ En matière de recherche, je m'intéresse aux liens entre le cinéma et la politique, la censure cinématographique, la propagande, l'histoire de la presse filmée, les études comparatives entre les cinémas français et américain et les méthodologies quantitatives en sciences humaines. 
