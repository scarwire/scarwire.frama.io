---
title: "Du cinéma dans les journaux, du journalisme au cinéma ? La censure de Razzia sur la chnouf et la représentation des stupéfiants dans la presse française des années 1950"
date: 2015-01-01
publishDate: 2019-11-20T11:54:51.587505Z
authors: ["Daniel Morgan"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "_Kinétraces éditions_ n° 1, 2015, p. 117-129"
url_pdf: "https://hal-univ-paris3.archives-ouvertes.fr/hal-01493859"
---

_Kinétraces éditions_ n° 1, 2015, p. 117-129
