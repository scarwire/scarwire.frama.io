---
title: "Relégué aux archives : _L’Affaire Seznec_, un film mort d’un cinéaste rejeté"
date: 2017-01-01
publishDate: 2019-11-20T11:54:51.586955Z
authors: ["Daniel Morgan"]
publication_types: ["2"]
abstract: ""
featured: false
publication: ""
url_pdf: "https://hal-univ-paris3.archives-ouvertes.fr/hal-01478936"
---

