#!/bin/bash

ipaddress=$(curl -s ipinfo.io/ip)

if [ "${ipaddress}" != "93.31.178.166" ] ; then
	mysite="danielmorgan.eu"
else
	mysite="cannibale.local"
fi

cd /home/dm/danielmorgan.eu
if hugo ; then
  echo "Syncing static site to server..."
  rsync -r --info=progress2 public/ "${mysite}:/var/www/danielmorgan.eu/public/"
fi
cd -
