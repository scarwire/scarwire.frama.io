// pos is position of where the user in the test or which question they're up to
var pos = 0, test, test_status, question, choice, choices, chA, chB, chC, correct = 0;
// this is a multidimensional array with 4 inner array elements with 5 elements inside them
var questions = [
  {
      question: '<audio controls=""><source src="../q1.mp3" type="audio/mpeg"></audio>',
      a: "I don't like working on the weekend, either!",
      b: "Either we do that, or we’ll need to ask the boss for extra time.",
      c: "The deadline is next Monday.",
      answer: "B"
    },
  {
      question: '<audio controls=""><source src="../q2.mp3" type="audio/mpeg"></audio>',
      a: "Since there are so many, maybe we should just start with the best three resumés.",
      b: "Since there are so few, we should just call them all in for interviews next week.",
      c: "I haven’t seen that candidate’s resumé yet.",
      answer: "A"
    },
  {
      question: '<audio controls=""><source src="../q3.mp3" type="audio/mpeg"></audio>',
      a: "Yes, the marketing team is working on a new project.",
      b: "The marketing team wants to reach new audiences, especially young people.",
      c: "We’ve been working really hard for the past month on targeting new audiences, especially young people.",
      answer: "C"
    },
  ];
// this get function is short for the getElementById function
function get(x){
  return document.getElementById(x);
}

// this function renders a question for display on the page
function renderQuestion(){
  test = get("test");
  if(pos >= questions.length){
    test.innerHTML = "<p>You got "+correct+" of "+questions.length+" questions correct</p><h2>Correct answers</h2><h3>Question 1</h3><audio controls=\"\"><source src=\"../q1.mp3\" type=\"audio/mpeg\"></audio><p>Q: There's a big deadline coming up. Do you think we're gonna hafta come in over the weekend to finish the project?<br /><strong>A: Either we do that, or we’ll need to ask the boss for extra time.</strong></p><h3>Question 2</h3><audio controls=\"\"><source src=\"../q2.mp3\" type=\"audio/mpeg\"></audio><p>Q: There are a lotta candidates for the new editorial position. Do you wanna see their resumés?<br /><strong>A: Since there are so many, maybe we should just start with the best three resumés.</strong></p><h3>Question 3</h3><audio controls=\"\"><source src=\"../q3.mp3\" type=\"audio/mpeg\"></audio><p>Q: I hear you've been working on a new project with the marketing team. Whaddaya been up to?<br /><strong>A: We’ve been working really hard for the past month on targeting new audiences, especially young people.</strong></p>";
    get("test_status").innerHTML = "Test completed";
    // resets the variable to allow users to restart the test
    pos = 0;
    correct = 0;
    // stops rest of renderQuestion function running when test is completed
    return false;
  }

  get("test_status").innerHTML = "Question "+(pos+1)+" of "+questions.length;

  question = questions[pos].question;
  chA = questions[pos].a;
  chB = questions[pos].b;
  chC = questions[pos].c;

  // display the question
  test.innerHTML = "<h3>"+question+"</h3>";

  // display the answer options
  // the += appends to the data we started on the line above
  test.innerHTML += "<label> <input type='radio' name='choices' value='A'> "+chA+"</label><br>";
  test.innerHTML += "<label> <input type='radio' name='choices' value='B'> "+chB+"</label><br>";
  test.innerHTML += "<label> <input type='radio' name='choices' value='C'> "+chC+"</label><br><br>";
  test.innerHTML += "<button class='btn btn-outline-primary px-3 py-2' onclick='checkAnswer()' style='margin: 0.5em;'>Submit Answer</button>";
}

function checkAnswer(){
  // use getElementsByName because we have an array which it will loop through
  choices = document.getElementsByName("choices");
  for(var i=0; i<choices.length; i++){
    if(choices[i].checked){
      choice = choices[i].value;
    }
  }
  // checks if answer matches the correct choice
  if(choice == questions[pos].answer){
    //each time there is a correct answer this value increases
    correct++;
  }
  // changes position of which character user is on
  pos++;
  // then the renderQuestion function runs again to go to next question
  renderQuestion();
}
// Add event listener to call renderQuestion on page load event
window.addEventListener("load", renderQuestion);
